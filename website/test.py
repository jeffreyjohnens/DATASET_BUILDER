import IPython
from IPython.display import display, Javascript, HTML
import uuid
import time
import json
#from google.colab import output
import numpy as np

import dataset_builder_2 as db

# can we have a database of different instrument sounds
note_height = 5
step_width = 20

# first demo should be a single bar regenerate tool
# and a selective track generate tool

def add_track(track_num):
  track_id = "track_{}".format(track_num)
  js = '''
  var x = document.createElement('div');
  x.style.marginBottom = "10px";
  x.style.width = "100%";
  x.style.height = "{track_height}px"; 
  x.style.background = "green";
  x.id = "{track_id}";
  document.querySelector('#test').append(x);
  '''
  display(Javascript(js.format(
    track_height=note_height*128,
    track_id=track_id
  )))

def add_note(track_num, height, pitch, start, end):
  track_id = "track_{}".format(track_num)
  js = '''
  var x = document.createElement('div');
  x.style.height = "5px";
  x.style.width = "{duration}px";
  x.style.left = "{start}px";
  x.style.top = "{pitch}px";
  x.style.position = "absolute";
  x.style.background = "red";
  document.querySelector('#{track_id}').append(x);
  '''
  display(Javascript(js.format(
    start=start*step_width,
    duration=(end-start)*step_width,
    pitch=pitch*note_height + height,
    track_id=track_id
  )))

def add_inst(track_num, note_list):
  js = '''
  var inst{number} = new Tone.PolySynth(4, Tone.Synth, {{
    "volume" : -8,
    "oscillator" : {{
      "partials" : [1, 2, 1],
    }},
    "portamento" : 0.05
  }}).toMaster();

  var part{number} = new Tone.Part(function(time, event){{
    inst.triggerAttackRelease(event.note, event.dur, time)
  }}, {note_list}).start("0m");

  part.loop = true;
  part.loopEnd = Tone.Ticks(3072).toNotation();
  '''
  display(Javascript(js.format(note_list=note_list, number=track_num)))

def build_viz(data, start_bar):
  # iterate over events and build notes ...
  for track_num in range(len(data["tracks"])):
    add_track(track_num)
    note_list = [] # for tonejs
    note_template = '''{{ 
      time : Tone.Ticks({start}).toNotation(), 
      note : Tone.Midi({pitch}).toNote(), 
      dur : Tone.Ticks({duration}).toNotation()
    }}'''
    for bar_num in range(start_bar, start_bar+4):
      time_offset = 48 * (bar_num - start_bar)
      bar = data["tracks"][track_num]["bars"][bar_num]
      if "events" in bar:
        onsets = {}
        for event_num in bar["events"]:
          event = data["events"][event_num]
          if event["velocity"]:
            onsets[event["pitch"]] = event
          elif event["pitch"] in onsets:
            pitch = event["pitch"]
            start = onsets[event["pitch"]]["qtime"] + time_offset
            end = event["qtime"] + time_offset
            onsets.pop(event["pitch"]) # remove from onsets
            add_note(track_num, pitch, start, end)
            note_list.append(
              note_template.format(
                pitch=pitch, start=start*4, duration=(end-start)*4))
          else:
            print("skipped note ..")
    
    note_list = '[' + ','.join(note_list) + ']'
    add_inst(track_num, note_list)

  js = '''
  //set the transport 
  Tone.Transport.bpm.value = 90;
  Tone.Transport.PPQ = 192;

  //bind the interface
  document.querySelector("tone-play-toggle").bind(Tone.Transport);
  '''
  display(Javascript(js))
    
def get_notes(data, track_num, start_bar, end_bar):
  # this ignore notes that hang over last bar
  notes = []
  onsets = {}
  for bar_num in range(start_bar, end_bar):
    bar_offset = 192 * (bar_num - start_bar)
    bar = data["tracks"][track_num]["bars"][bar_num]
    for event_num in bar.get("events", []):
      event = data["events"][event_num]
      print(event)
      if event["velocity"]:
        onsets[event["pitch"]] = event
      elif event["pitch"] in onsets:
        pitch = event["pitch"]
        start = onsets[event["pitch"]]["qtime"] + bar_offset
        end = event["qtime"] + bar_offset
        onsets.pop(event["pitch"]) # remove from onset
        notes.append({"pitch" : pitch, "start" : start, "end" : end})
  return notes

def build_viz(data, start_bar, end_bar):
  current_height = 0
  for track_num in range(len(data["tracks"])):
    notes = get_notes(data, track_num, start_bar, end_bar)

    """
    # map pitches on to compresed range
    unique_pitches = np.sort(np.unique([n["pitch"] for n in notes]))
    compressed_map = {p:i for i,p in enumerate(unique_pitches)}
    track_height = len(unique_pitches) * note_height

    add_track(track_height)
    for n in notes:
      add_note(track_num, current_height, compressed_map[n["pitch"]], n["start"], n["end"])
    current_height += track_height + 10 # gap
    """

path = '/Users/Jeff/SFU/PhD/DATASET_BUILDER/test_midi/MIDI Files/FFVII OST (GM Arrangements)/GM - 1-02 Opening ~ Bombing Mission.mid'
data = json.loads(db.parse_to_json(path))
build_viz(data, 0, 8)
exit()

with open("test.json", "w") as f:
  f.write(json.dumps(data, indent=4))

exit()

#build_viz(data, 4)
#exit()


note_height = 2
step_width = 20

tracks = [None,None]

# this is run each time the RUN button is clicked
def callback():
  output.clear()
  print("refreshed ...", time.time())
  build(callback, tracks)

def file_handler(*args, **kwargs):
  import re
  import base64
  data = re.search(r'base64,(.*)', args[0]).group(1)
  with open("test.mid", "wb") as f:
    f.write(base64.b64decode(data))
  data = json.loads(db.parse_to_json("test.mid"))
  build_viz(data, 4)

# this is rebuilt after run is called
# input list of tracks where each track is a list of notes
def build(callback, tracks):
  callback_id = 'button-' + str(uuid.uuid4())
  output.register_callback(callback_id, callback)

  # make a button that we can press to load a midi file
  html = '''
  <input type='file' onchange='onChooseFile(event, onFileLoad.bind(this, "contents"))' />
  <p id="contents"></p>
  '''

  js = '''
  var fr = new FileReader();
  
  function onFileLoad(elementId, event) {
    //document.getElementById(elementId).innerText = event.target.result;
    google.colab.kernel.invokeFunction('file_handler', [event.target.result], {});
  }

  function onChooseFile(event, onLoadFileHandler) {
    if (typeof window.FileReader !== 'function')
        throw ("The file API isn't supported on this browser.");
    let input = event.target;
    if (!input)
        throw ("The browser does not properly implement the event object");
    if (!input.files)
        throw ("This browser does not support the `files` property of the file input.");
    if (!input.files[0])
        return undefined;
    let file = input.files[0];
    //let fr = new FileReader();
    fr.onload = onLoadFileHandler;
    //fr.readAsArrayBuffer(file);
    //fr.readAsText(file);
    //fr.readAsBinaryString(file);
    fr.readAsDataURL( file );
  }
  '''
  output.register_callback("file_handler", file_handler)
  display(Javascript(js))
  display(HTML(html))
  

  return

  html = '''
  <style>
    .pr {
      padding: 0;
      background: gray;
      height: 700px;
      width: 100%;
    }
  </style>
  <button id='cbutton'>RUN</button>
  <button id='inter'>TEST</button>
  <div id="test" class="pr"></div>
  '''

  js = '''
  document.querySelector('#cbutton').onclick = function(e) {{
    google.colab.kernel.invokeFunction('{callback_id}', [], {{}});
  }};
  document.querySelector('#inter').onclick = function(e) {{
    var x = document.createElement('div');
    x.style.width = "100px";
    x.style.height = "100px";
    x.style.top = "50px";
    x.style.left = "50px"; 
    document.querySelector('#test').append(x);
  }};
  '''


  #display(HTML(html))
  #display(Javascript(js.format(callback_id=callback_id)))
  #for track_num, track in enumerate(tracks):
  #  add_track(track_num)
  #  for _ in range(10):
  #    add_note(track_num, np.random.randint(16), np.random.randint(128))
  

  # do playback with 192 ticks per quarter ...
  note_list = []
  note_template = '''{{ 
    time : Tone.Ticks({start}).toNotation(), 
    note : {pitch_list}, 
    dur : Tone.Ticks({duration}).toNotation()
  }}'''
  for _ in range(10):
    start = np.random.randint(192*16)
    duration = 192
    pitches = np.random.randint(48, high=84, size=(3,))
    pitch_list = '[' + ','.join(['Tone.Midi({}).toNote()'.format(p) for p in pitches]) + ']'
    note_list.append(note_template.format(pitch_list=pitch_list, start=start, duration=duration))
  note_list = '[' + ','.join(note_list) + ']'
  print(note_list)

  js = '''
  var piano = new Tone.PolySynth(4, Tone.Synth, {{
    "volume" : -8,
    "oscillator" : {{
      "partials" : [1, 2, 1],
    }},
    "portamento" : 0.05
  }}).toMaster();

  var pianoPart = new Tone.Part(function(time, event){{
    piano.triggerAttackRelease(event.note, event.dur, time)
  }}, {note_list}).start("0m");

  pianoPart.loop = true;
  pianoPart.loopEnd = Tone.Ticks(3072).toNotation();

  //set the transport 
  Tone.Transport.bpm.value = 90;
  Tone.Transport.PPQ = 192;

  //bind the interface
  document.querySelector("tone-play-toggle").bind(Tone.Transport);
  '''

  # these links seem to work for javascript packages ...
  html = '''
  <script src="https://unpkg.com/jquery"></script>
  <script src="https://unpkg.com/@webcomponents/webcomponentsjs@^2/webcomponents-bundle.js"></script>
	<script src="https://unpkg.com/tone"></script>
	<script src="https://unpkg.com/@tonejs/ui"></script>

  <tone-content>
    <tone-play-toggle></tone-play-toggle>
  </tone-content>
  '''

  display(HTML(html))
  display(Javascript(js.format(note_list=note_list)))

#build(callback, tracks)