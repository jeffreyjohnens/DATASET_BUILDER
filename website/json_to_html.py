import glob
import json
import numpy as np
import dataset_builder_2 as db

# a json stores the current info about a piece we build directly from that
# and make changes to it then call update non-destructively

midi_paths = glob.glob("/users/jeff/data/lmd_full/**/*.mid", recursive=True)
path = np.random.choice(midi_paths)

path = "/users/jeff/data/lmd_full/c/c8059638c792f38d359ad4325f5055fb.mid"


e = db.EncoderConfig()
e.do_track_shuffle = False # don't reshuffle

js = db.TrackEncoder().midi_to_json(path,e)
js = db.select_segment(js, 3) # select a random section with 3 tracks
js = json.loads(js)

# what is below is what is needed to visualize a json

def get_track_notes(js, track):
  # join the events together
  notes = []
  onsets = {}
  for bar_num,bar in enumerate(track.get("bars",[])):
    for event_id in bar.get("events",[]):
      event = js["events"][event_id]
      if event["velocity"] > 0:
        event["bar"] = bar_num
        onsets[event["pitch"]] = event
      elif event["pitch"] in onsets:
        notes.append({
          "start" : onsets[event["pitch"]]["qtime"],
          "end" : event["qtime"],
          "pitch" : event["pitch"],
          "bar" : onsets[event["pitch"]]["bar"]
        })
        onsets.pop(event["pitch"])
  return notes

def compress_pitches(notes):
  pitches = []
  for note in notes:
    pitches.append(note["pitch"])
  pitch_map = {p:i for i,p in enumerate(np.sort(np.unique(pitches)))}
  for note in notes:
    note["pitch"] = pitch_map[note["pitch"]]

def build_inst_selector(inst=None, track_num=0):
  html = """
  <div class="mdl-cell mdl-cell--1-col"> 
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">
        <input type="text" value="" class="mdl-textfield__input" id="sample6" readonly>
        <input type="hidden" value="" name="sample6">
        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
        <label for="sample6" class="mdl-textfield__label">Country</label>
        <ul for="sample6" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
            <li class="mdl-menu__item" data-val="BY" data-selected="true">Belarus</li>
            <li class="mdl-menu__item" data-val="BR">Brazil</li>
            <li class="mdl-menu__item" data-val="ES">Estonia</li>
            <li class="mdl-menu__item" data-val="FI">Finland</li>
            <li class="mdl-menu__item" data-val="FR">France</li>
            <li class="mdl-menu__item" data-val="DE">Germany</li>
            <li class="mdl-menu__item" data-val="PL">Poland</li>
            <li class="mdl-menu__item" data-val="RU">Russia</li>
        </ul>
    </div>
  </div>
  """.format(track_num=track_num)
  return html

def build_html_piano_roll(js):
  html = []
  for track_num, track in enumerate(js.get("tracks",[])):
    notes = get_track_notes(js,track)
    html.append('<div class="mdl-grid">')
    
    #html.append('<div class="mdl-cell mdl-cell--1-col">')
    #html.append('<button id="{id}button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">{button_text}</button>'.format(id='so', button_text='pressME'))
    #html.append('</div>')

    #if track_num == 0:
    #  html.append(build_inst_selector(track_num=track_num))

    for i in range(4):
      html.append('<div class="mdl-cell mdl-cell--2-col">')
      html.append('<svg width="100%" height="120px" style="background-color:grey" viewBox="0 0 100 100" preserveAspectRatio="none">')
      for note in notes:
        if note["bar"] == i:
          y = float(note["pitch"]) / 128 * 100
          x = float(note["start"]) / 48 * 100
          w = float(note["end"] - note["start"]) / 48 * 100
          h = 1. / 128 * 100
          html.append('<rect x="{}%" y="{}%" width="{}%" height="{}%" fill="purple" />'.format(x,y,w,h))
      html.append('</svg>')
      html.append('</div>')
    html.append('</div>')
  
  
  return "\n".join(html)


# use this helper to test in html
header = """<html>
<head>
<title>My Sample</title>

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
</head>


<body>
<div id="trackContainer"></div>
"""

footer = """</body>
</html>
"""

with open("auto.html", "w") as f:
  f.write(header)
  f.write(build_html_piano_roll(js))
  f.write(footer)

from subprocess import call
call("open auto.html", shell=True)
  

  