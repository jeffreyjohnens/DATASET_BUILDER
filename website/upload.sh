#!/bin/bash
eval `ssh-agent`
ssh-add
ssh jeffe@ftp.sfu.ca 'mkdir -p pub_html/onnx_checkpoints/track_gpt2'
rsync -a onnx_checkpoints/track_gpt2/61000 jeffe@ftp.sfu.ca:pub_html/onnx_checkpoints/track_gpt2 --progress
ssh jeffe@ftp.sfu.ca 'chmod -R 711 pub_html/onnx_checkpoints'
ssh jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/onnx_checkpoints'
kill $SSH_AGENT_PID