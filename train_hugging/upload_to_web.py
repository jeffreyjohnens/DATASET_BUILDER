from subprocess import call
import os

def upload_model(name, step):  
  id_rsa = "/home/jeffe/.ssh/sfuwebcedar_rsa"
  ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
  call("ssh -i {} jeffe@ftp.sfu.ca 'mkdir -p pub_html/{}'".format(id_rsa, ckpt_path), shell=True)
  call("rsync -e 'ssh -i {}' -a {} jeffe@ftp.sfu.ca:pub_html/{} --progress".format(id_rsa, ckpt_path,os.path.dirname(ckpt_path)), shell=True)
  call("ssh -i {} jeffe@ftp.sfu.ca 'chmod -R 711 pub_html/checkpoints'".format(id_rsa), shell=True)
  call("ssh -i {} jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/checkpoints'".format(id_rsa), shell=True)

# this runs from local computer
def upload_model_v2(name, step):
  ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
  call("ssh -i /Users/jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'mkdir -p pub_html/{}'".format(ckpt_path), shell=True)
  execute_command_on_cedar("rsync -e 'ssh -i /home/jeffe/.ssh/sfuwebcedar_rsa' -a /home/jeffe/project/jeffe/DATASET_BUILDER/train_hugging/{} jeffe@ftp.sfu.ca:pub_html/{} --progress".format(ckpt_path,os.path.dirname(ckpt_path)))
  call("ssh -i /Users/jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'chmod -R 711 pub_html/checkpoints; chmod -R 755 pub_html/checkpoints'", shell=True)

def execute_command_on_cedar(cmd):
  call('''ssh -i /Users/Jeff/.ssh/cedar_rsa -t jeffe@cedar.computecanada.ca "{}"'''.format(cmd), shell=True)

if __name__ == "__main__":

  # models are found at http://www.sfu.ca/~jeffe/checkpoints/
  # python upload_to_web.py --name "TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False" --step 130000
  # python upload_to_web.py --name TRACK_ENCODER_gpt2_version1_Jul_01_22_28_False --step 310000

  # python upload_to_web.py --name TRACK_ENCODER_gpt2_version3_Jul_06_19_58_False --step 260000

  # python upload_to_web.py --name 

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--name", default="", type=str)
  parser.add_argument("--step", default=-1, type=int)
  parser.add_argument("--list", action='store_true')
  args = parser.parse_args()

  ckpt_path = "checkpoints"
  if len(args.name):
    ckpt_path = "checkpoints/{}".format(args.name)
  if len(args.name) and args.step >= 0:
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(args.name,args.step)

  if args.list:
    execute_command_on_cedar("ls -lsht /home/jeffe/project/jeffe/DATASET_BUILDER/train_hugging/{}".format(ckpt_path))
  else:
    assert len(args.name)
    assert args.step >= 0

    #upload_model(args.name, args.step)
    upload_model_v2(args.name, args.step)