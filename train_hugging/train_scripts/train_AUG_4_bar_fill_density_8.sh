#!/bin/bash
#SBATCH --gres=gpu:v100l:4
#SBATCH --cpus-per-task=32
#SBATCH --exclusive
#SBATCH --mem=0
#SBATCH --time=2-12:00
#SBATCH --account=def-pasquier
#SBATCH --mail-user jeffreyjohnens@gmail.com
#SBATCH --mail-type ALL

module load gcc/7.3.0 cuda/10.0.130
source ~/HUGGING/bin/activate
cd /home/jeffe/project/jeffe/DATASET_BUILDER/train_hugging
python train_torch.py --overwrite 1 --model gpt2 --encoding TRACK_BAR_FILL_DENSITY_ENCODER --lr .001 --num_bars 8 --max_tracks 6

