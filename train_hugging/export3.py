import torch

# try to monkey patch embedding ...
def _no_grad_embedding_renorm_(weight, input, max_norm, norm_type):
    # type: (Tensor, Tensor, float, float) -> Tensor
    with torch.no_grad():
        torch.embedding_renorm_(weight, input, max_norm, norm_type)

def embedding(input, weight, padding_idx=None, max_norm=None, norm_type=2.,
              scale_grad_by_freq=False, sparse=False):
    # type: (Tensor, Tensor, Optional[int], Optional[float], float, bool, bool) -> Tensor
    r"""A simple lookup table that looks up embeddings in a fixed dictionary and size.
    This module is often used to retrieve word embeddings using indices.
    The input to the module is a list of indices, and the embedding matrix,
    and the output is the corresponding word embeddings.
    See :class:`torch.nn.Embedding` for more details.
    Args:
        input (LongTensor): Tensor containing indices into the embedding matrix
        weight (Tensor): The embedding matrix with number of rows equal to the maximum possible index + 1,
            and number of columns equal to the embedding size
        padding_idx (int, optional): If given, pads the output with the embedding vector at :attr:`padding_idx`
                                         (initialized to zeros) whenever it encounters the index.
        max_norm (float, optional): If given, each embedding vector with norm larger than :attr:`max_norm`
                                    is renormalized to have norm :attr:`max_norm`.
                                    Note: this will modify :attr:`weight` in-place.
        norm_type (float, optional): The p of the p-norm to compute for the :attr:`max_norm` option. Default ``2``.
        scale_grad_by_freq (boolean, optional): If given, this will scale gradients by the inverse of frequency of
                                                the words in the mini-batch. Default ``False``.
        sparse (bool, optional): If ``True``, gradient w.r.t. :attr:`weight` will be a sparse tensor. See Notes under
                                 :class:`torch.nn.Embedding` for more details regarding sparse gradients.
    Shape:
        - Input: LongTensor of arbitrary shape containing the indices to extract
        - Weight: Embedding matrix of floating point type with shape `(V, embedding_dim)`,
                            where V = maximum index + 1 and embedding_dim = the embedding size
        - Output: `(*, embedding_dim)`, where `*` is the input shape
    Examples::
        >>> # a batch of 2 samples of 4 indices each
        >>> input = torch.tensor([[1,2,4,5],[4,3,2,9]])
        >>> # an embedding matrix containing 10 tensors of size 3
        >>> embedding_matrix = torch.rand(10, 3)
        >>> F.embedding(input, embedding_matrix)
        tensor([[[ 0.8490,  0.9625,  0.6753],
                 [ 0.9666,  0.7761,  0.6108],
                 [ 0.6246,  0.9751,  0.3618],
                 [ 0.4161,  0.2419,  0.7383]],
                [[ 0.6246,  0.9751,  0.3618],
                 [ 0.0237,  0.7794,  0.0528],
                 [ 0.9666,  0.7761,  0.6108],
                 [ 0.3385,  0.8612,  0.1867]]])
        >>> # example with padding_idx
        >>> weights = torch.rand(10, 3)
        >>> weights[0, :].zero_()
        >>> embedding_matrix = weights
        >>> input = torch.tensor([[0,2,0,5]])
        >>> F.embedding(input, embedding_matrix, padding_idx=0)
        tensor([[[ 0.0000,  0.0000,  0.0000],
                 [ 0.5609,  0.5384,  0.8720],
                 [ 0.0000,  0.0000,  0.0000],
                 [ 0.6262,  0.2438,  0.7471]]])
    """
    input = input.to(torch.long)
    if padding_idx is not None:
        if padding_idx > 0:
            assert padding_idx < weight.size(0), 'Padding_idx must be within num_embeddings'
        elif padding_idx < 0:
            assert padding_idx >= -weight.size(0), 'Padding_idx must be within num_embeddings'
            padding_idx = weight.size(0) + padding_idx
    else:
        padding_idx = -1
    if max_norm is not None:
        # `embedding_renorm_` will call .contiguous() on input anyways, so we
        # call it here and take advantage of the improved locality in the
        # `embedding` call below too.
        input = input.contiguous()
        # XXX: equivalent to
        # with torch.no_grad():
        #   torch.nembedding_renorm_
        # remove once script supports set_grad_enabled
        _no_grad_embedding_renorm_(weight, input, max_norm, norm_type)
    return torch.embedding(weight, input, padding_idx, scale_grad_by_freq, sparse)

torch.nn.functional.embedding = embedding


from transformers import Trainer, TrainingArguments
from transformers import *
import transformers

from torch.onnx import export
import numpy as np
import os
from subprocess import call

# this seems to work ...

def build_shape_dict(name: str, tensor, is_input: bool, seq_len: int):
    if isinstance(tensor, (tuple, list)):
        return [build_shape_dict(name, t, is_input, seq_len) for t in tensor]

    else:
        # Let's assume batch is the first axis with only 1 element (~~ might not be always true ...)
        axes = {[axis for axis, numel in enumerate(tensor.shape) if numel == 1][0]: "batch"}
        if is_input:
            if len(tensor.shape) == 2:
                axes[1] = "sequence"
            else:
                raise ValueError("Unable to infer tensor axes ({})".format(len(tensor.shape)))
        else:
            seq_axes = [dim for dim, shape in enumerate(tensor.shape) if shape == seq_len]
            axes.update({dim: "sequence" for dim in seq_axes})

    print("Found {} {} with shape: {}".format("input" if is_input else "output", name, axes))
    return axes

def ensure_valid_input(model, tokens, input_names):
    """
    Ensure input are presented in the correct order, without any None
    Args:
        model: The model used to forward the input data
        tokens: BatchEncoding holding the input data
        input_names: The name of the inputs
    Returns: Tuple
    """
    print("Ensuring inputs are in correct order")

    model_args_name = model.forward.__code__.co_varnames
    model_args, ordered_input_names = [], []
    for arg_name in model_args_name[1:]:  # start at index 1 to skip "self" argument
        if arg_name in input_names:
            ordered_input_names.append(arg_name)
            model_args.append(tokens[arg_name])
        else:
            print("{} is not present in the generated input list.".format(arg_name))
            break

    print("Generated inputs order: {}".format(ordered_input_names))
    return ordered_input_names, tuple(model_args)

# convert to int64 inside model ...
class SafeGPT2LMHeadModel(GPT2LMHeadModel):
    def __init__(self, *args, **kwargs):
        super(SafeGPT2LMHeadModel,self).__init__(*args,**kwargs)
    def __call__(self, *args, **kwargs):
        print(kwargs)
        if "input_ids" in kwargs:
            kwargs["input_ids"] = kwargs["input_ids"].to(torch.long)
        return super(SafeGPT2LMHeadModel,self).__call__(*args,**kwargs)

name = "track_gpt2"
step = 61000

model_path = "checkpoints/{}/checkpoint-{}".format(name,step)
model = GPT2LMHeadModel.from_pretrained(model_path)
#model = SafeGPT2LMHeadModel.from_pretrained(model_path)

seq_len = 2
tokens = {"input_ids" : torch.from_numpy(np.zeros((1,seq_len), dtype=np.int)).to(torch.int)}
outputs = model(**tokens)

outputs_flat = []
for output in outputs:
  if isinstance(output, (tuple, list)):
    outputs_flat.extend(output)
  else:
    outputs_flat.append(output)

input_names = ["input_ids"]
output_names = ["output_{}".format(i) for i in range(len(outputs_flat))]
dynamic_axes = {k: build_shape_dict(k, v, False, seq_len) for k, v in zip(output_names, outputs_flat)}

ordered_input_names, model_args = ensure_valid_input(model, tokens, input_names)

output_path = "onnx_checkpoints/{}/{}/model.onnx".format(name,step)
os.makedirs(os.path.dirname(output_path), exist_ok=True)

#model_args = tuple([x.to(torch.int) for x in model_args])

export(
  model,
  model_args,
  f=output_path,
  input_names=ordered_input_names,
  output_names=output_names,
  dynamic_axes=dynamic_axes,
  do_constant_folding=True,
  enable_onnx_checker=True,
  opset_version=11,
  verbose=True)

# create a keras from the onnx
import onnx
from onnx2keras import onnx_to_keras
onnx_model = onnx.load(output_path)

k_model = onnx_to_keras(
    onnx_model=onnx_model, input_names=input_names,
    input_shapes=[(32,1)], name_policy='short',
    verbose=True, change_ordering=False)

"""
import onnx
model = onnx.load(output_path)
for node in model.graph.node:
    print(node.input, node.output)
"""

"""
import onnx
from onnx_tf.backend import prepare

onnx_model = onnx.load(output_path)  # load onnx model
tf_rep = prepare(onnx_model)  # prepare tf representation
tf_rep.export_graph("")  # export the model
"""