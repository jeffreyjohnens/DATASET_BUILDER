from transformers import Trainer, TrainingArguments
from transformers import *
import tensorflow as tf
import os

# tensorflowjs_converter --input_format tf_saved_model tf_checkpoints/track_gpt2/61000 js_checkpoints/track_gpt2/61000

name = "track_gpt2"
step = 61000

model = TFGPT2LMHeadModel.from_pretrained("checkpoints/{}/checkpoint-{}".format(name,step), from_pt=True)
model._set_inputs(tf.TensorSpec([1,2], tf.int32))
print("NUM PARAMETERS : {:,}".format(model.num_parameters()))

model_folder = "tf_checkpoints/{}/{}".format(name,step)
model_path = os.path.join(model_folder, "model")
os.makedirs(model_folder, exist_ok=True)

#import tensorflowjs as tfjs
#tfjs.converters.save_keras_model(model, 'tfjs_artifacts')
#exit()

#inputs = tf.keras.Input(shape=(None,), dtype=tf.int32)
#outputs = model.transformer(inputs)
#kmodel = tf.keras.Model(inputs, outputs)

#kmodel = tf.keras.Sequential([model.transformer])
#tf.keras.models.save_model(kmodel, model_path, save_format="h5")

# then call 
#tensorflowjs_converter --input_format=keras tf_checkpoints/track_gpt2/61000/model.h5 js_checkpoints/track_gpt2/61000

#model.save_weights(model_path, save_format="tf")
tf.saved_model.save(model, model_folder)

o_model = tf.saved_model.load(model_folder)
print(dir(o_model))
print(dir(o_model.transformer))

# then call
#tensorflowjs_converter --input_format=tf_saved_model tf_checkpoints/track_gpt2/61000 js_checkpoints/track_gpt2/61000

#python3 /usr/local/lib/python3.6/site-packages/tensorflowjs/converters/converter.py --input_format=tf_saved_model tf_checkpoints/track_gpt2/61000 js_checkpoints/track_gpt2/61000