import os
from subprocess import call
import tensorflow as tf
from transformers import *
import numpy as np
from dataset_builder_2 import Jagged, ENCODER_TYPE, getEncoderSize


#dataset_path = "../dataset_builder_2/test.arr"
dataset_path = "/home/jeffe/scratch/data_track.arr" 

save_ckpt_every = 1000
batch_size = 32
max_seq_len = 1024
encoder_mode = ENCODER_TYPE.TRACK_ENCODER
vocab_size = getEncoderSize(encoder_mode)

def train_inputs(*args):
  jag = Jagged(dataset_path)
  while True:
    batch, mask = jag.read_batch(batch_size, 0, encoder_mode)
    batch = np.array(batch)[:,:max_seq_len+1]
    mask = np.array(mask)[:,:max_seq_len+1]
    yield {"input_ids":batch[:,:-1], "attention_mask":mask[:,:-1]}, batch[:,1:]

def train_eval_inputs(*args):
  jag = Jagged(dataset_path)
  while True:
    batch, mask = jag.read_batch(batch_size, 1, encoder_mode)
    batch = np.array(batch)[:,:max_seq_len+1]
    mask = np.array(mask)[:,:max_seq_len+1]
    yield {"input_ids":batch[:,:-1], "attention_mask":mask[:,:-1]}, batch[:,1:]

train_dataset = tf.data.Dataset.from_generator(
  train_inputs, 
  ({"input_ids" : tf.int64, "attention_mask" : tf.int64}, tf.int64),
  ({"input_ids" : tf.TensorShape([None,max_seq_len]), "attention_mask" : tf.TensorShape([None,max_seq_len])}, tf.TensorShape([None,max_seq_len]))
)

valid_dataset = tf.data.Dataset.from_generator(
  train_eval_inputs, 
  ({"input_ids" : tf.int64, "attention_mask" : tf.int64}, tf.int64),
  ({"input_ids" : tf.TensorShape([None,max_seq_len]), "attention_mask" : tf.TensorShape([None,max_seq_len])}, tf.TensorShape([None,max_seq_len]))
)

#other models
"""
config = XLNetConfig(
  vocab_size=vocab_size, 
  n_layer=1, 
  n_head=1
)
model = TFXLNetLMHeadModel(config)
"""

# incompatible shapes errors for both the following models
config = GPT2Config(
  vocab_size=vocab_size, 
  n_layer=6,
  n_head=6,
  n_embd=300,
  n_positions=max_seq_len,
  n_ctx=max_seq_len)
model = TFGPT2LMHeadModel(config)


"""
config = TransfoXLConfig(
  vocab_size=vocab_size,
  cutoffs=[vocab_size],
  n_layer=1,
  n_head=1,
  mem_len=max_seq_len,
  clamp_len=max_seq_len,
  attn_type=0
)
model = TFTransfoXLLMHeadModel(config)
"""

optimizer = tf.keras.optimizers.Adam(learning_rate=3e-5, epsilon=1e-08, clipnorm=1.0)
loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
metric = tf.keras.metrics.SparseCategoricalAccuracy('accuracy')

model.compile(optimizer=optimizer, loss=[loss], metrics=[metric])

log_dir = "../checkpoints/hf_gpt2"
call("rm -rf " + log_dir, shell=True)

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
checkpoint_callback = tf.keras.callbacks.ModelCheckpoint("../checkpoints/hf_gpt2/model_{epoch}", save_best_only=False, save_freq=save_ckpt_every, verbose=1)

model.fit(train_dataset, validation_data=valid_dataset, epochs=20000, steps_per_epoch=10, callbacks=[tensorboard_callback, checkpoint_callback], validation_steps=1)