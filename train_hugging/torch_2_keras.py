from transformers import Trainer, TrainingArguments
from transformers import *
import tensorflow as tf
import os

name = "track_gpt2"
step = 61000
model_path = "checkpoints/{}/checkpoint-{}".format(name,step)
model = GPT2LMHeadModel.from_pretrained(model_path)

import torch
from pytorch2keras.converter import pytorch_to_keras

x = torch.zeros((1,2), dtype=torch.long)
k_model = pytorch_to_keras(model, x, [(None,None)], verbose=True)