import os
import tensorflow as tf
from tensorflow.python.keras.backend import set_session
from tensorflow.python.keras.models import load_model

dest_folder = "tf_checkpoints/track_gpt2/61000_old"
os.makedirs(dest_folder, exist_ok=True)

sess = tf.Session()
graph = tf.get_default_graph()

# IMPORTANT: models have to be loaded AFTER SETTING THE SESSION for keras! 
# Otherwise, their weights will be unavailable in the threads after the session there has been set
set_session(sess)
model = load_model("tf_checkpoints/track_gpt2/61000")

print(model.summary())

#tf.keras.models.save_model(model, "test.h5")

#tf.saved_model.load(sess, ["serve"], "tf_checkpoints/track_gpt2/61000")
#tf.saved_model.save(sess, dest_folder)


exit()



#with tf.Session() as sess:
  #model = tf.saved_model.load(sess, ["serve"], "tf_checkpoints/track_gpt2/61000")

model = tf.saved_model.load_v2("tf_checkpoints/track_gpt2/61000")
tf.saved_model.save(model, dest_folder)

