import os
import glob
import numpy as np
import torch
import time
import json
import copy
from subprocess import call
from transformers import *
import dataset_builder_2 as db

model_dict = {
  "gpt2" : GPT2LMHeadModel
}

# make some globals for control
GENERAL_MIDI_MAP = {
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "non_drum" : list(range(128))
}

def load_model(model_name,name,step):
  # load a model
  # download a model from compute canada (cedar) if necessary
  ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
  model_folder = os.path.dirname(ckpt_path)
  current_folder = os.path.dirname(os.path.abspath(__file__))
  dst_folder = os.path.join(current_folder, model_folder)
  os.makedirs(dst_folder, exist_ok=True)
  if not os.path.exists(ckpt_path):
    print("downloading {} [step={}] ...".format(name,step))
    call("rsync -a -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/{}/checkpoint-{} {} --progress".format(name,step,dst_folder), shell=True)
  return model_dict[model_name].from_pretrained(ckpt_path, from_tf=False)

def load_model_from_web(model_name,name,step):
  # don't need optimizer
  file_list = ["config.json", "pytorch_model.bin", "scheduler.pt", "training_args.bin"]
  ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
  current_folder = os.path.dirname(os.path.abspath(__file__))
  dst_folder = os.path.join(current_folder, ckpt_path)
  os.makedirs(dst_folder, exist_ok=True)
  for file in file_list:
    path = os.path.join(dst_folder,file)
    if not os.path.exists(path):
      call("wget http://www.sfu.ca/~jeffe/{}/{} -O {}".format(ckpt_path,file,path), shell=True)
  return model_dict[model_name].from_pretrained(ckpt_path, from_tf=False)
  
load_model_from_web("gpt2", "TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False", 130000)
exit()


def make_inf_mask(mask):
  mask[mask==0] = -float("Inf")
  mask[mask==1] = 0
  return mask

def generate_w_injections(model, input_ids=None, injects=None, temperature=.9):
  import torch.nn.functional as F
  assert injects is not None
  batch_size = len(injects)

  gen_start_time = time.time()

  if input_ids is None:
    input_ids = torch.zeros((batch_size,1), dtype=torch.long)
  else:
    input_ids = torch.from_numpy(input_ids).to(torch.long)
  # must be 2d

  past = None
  finished = np.zeros((batch_size,), dtype=np.bool)
  use_cache = model.config.use_cache
  vocab_size = model.config.vocab_size
  
  while np.any(finished==False):

    raw_input = input_ids
    if past:
      raw_input = input_ids[:,-1].unsqueeze(-1)

    outputs = model(input_ids=raw_input, past=past, use_cache=use_cache)
    if model._use_cache(outputs, use_cache):
      past = outputs[1]
    
    logits = outputs[0][:,-1,:]
    if temperature != 1.0:
      logits = logits / temperature
    
    last_tokens = raw_input[:,-1].detach().numpy()
    masks = torch.zeros((batch_size, vocab_size), dtype=torch.float)
    for i,(last_token,inject) in enumerate(zip(last_tokens,injects)):
      trigger, mask = inject[0]
      if last_token == trigger:
        if mask is not None:
          assert mask.dtype == np.bool
          masks[i][~mask] = -float("Inf") # set avoided tokens to smallest value
        if len(inject) == 1:
          finished[i] = True
        else:
          inject.pop(0)
        
        #print(np.where(masks==0))
    
    if np.any(finished==False):
      logits = masks + logits
      
      probs = F.softmax(logits, dim=-1)
      next_token = torch.multinomial(probs, num_samples=1).squeeze(1)
      next_token[finished] = 0 # if the sequence has ended
      input_ids = torch.cat([input_ids, next_token.unsqueeze(-1)], dim=-1)
    
    if input_ids.shape[1] >= 2048:
      finished[:] = True

  print(time.time() - gen_start_time)
  input_ids = input_ids.detach().numpy()

  # remove the padding at end of sequences
  output = []
  for seq in input_ids:
    idx = np.where(seq==0)[0]
    if len(idx) > 1:
      output.append( seq[:idx[1]] )
    else:
      output.append( seq )
  return output 

# bars is a list of (track,bar) tuples indicating which
# bars to resample we do this gibbs sampling style
def replace_bars(path, fill_bars, batch_size, result_directory, temperature):
  
  model = load_model(
    "gpt2", "TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False", 130000)

  enc = db.TrackOneBarFillEncoder()
  FILL_START = enc.rep.encode({db.TOKEN_TYPE.FILL_IN : 1})
  FILL_END = enc.rep.encode({db.TOKEN_TYPE.FILL_IN : 2})

  jstr = db.encode_to_json(path, 12)

  for i,(track,bar) in enumerate(fill_bars):
    tokens = np.array(enc.fill_bar(jstr, track, bar, 526))
    print(tokens[np.where(tokens >= model.config.vocab_size)])
    print(tokens[np.where(tokens < 0)])
    #print(tokens)
    #print(json.dumps(jstr,indent=4))

    if (i==0):
      print(enc.rep.pretty(tokens))
      db.decode("quick_test/fill_orig.mid", tokens, db.TRACK_ONE_BAR_FILL_ENCODER)
    else:
      print(enc.rep.pretty(tokens[np.where(tokens==FILL_START)[0][0]:]))

    prompt_end = np.where(tokens==FILL_START)[0][0]
    prompt = tokens[:prompt_end+1]
    input_ids = np.tile(prompt[None,:], (batch_size,1))
    
    control = [(FILL_END, None)]
    controls = [copy.deepcopy(control) for _ in range(batch_size)]

    gen = generate_w_injections(
      model, input_ids=input_ids, injects=controls, temperature=temperature)

    #print([len(g) for g in gen])
    #print(gen[0])
    #print(json.dumps(jstr,indent=4))

    jstr = db.decode_to_json(gen[0], db.ENCODER_TYPE.TRACK_ONE_BAR_FILL_ENCODER)

  db.decode("quick_test/fill.mid", gen[0], db.TRACK_ONE_BAR_FILL_ENCODER)


# tracks is a list of lists indicating which midi instruments to pick
# [[0, 1 4], None, [60], 129]
# 128 indicates drum track
def run_generation(jstr, cond_tracks, tracks, batch_size, temperature):

  model = load_model(
    "gpt2", "TRACK_ENCODER_gpt2_version1_Jun_25_10_21_False", 130000)
  
  # make the injects
  vocab_size = model.config.vocab_size
  enc = db.TrackEncoder()

  input_ids = None
  if jstr is not None:
    tokens = enc.encode_from_json(jstr, cond_tracks)
    input_ids = np.array(tokens)[None,:]

  PIECE_START = enc.rep.encode({db.TOKEN_TYPE.PIECE_START : 0})
  NORMAL_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 0})
  DRUM_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 1})
  BAR_START = enc.rep.encode({db.TOKEN_TYPE.BAR : 0})
  TRACK_END = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})
  ANY_TOKEN_MASK = None
  
  # impose various controls on the generated sequence
  control = []
  for track_num, track in enumerate(tracks):
    if track is None:
      track = list(range(128))
    if not isinstance(track,list):
      track = [track]
    if isinstance(track[0],str):
      if track[0] in GENERAL_MIDI_MAP:
        track = GENERAL_MIDI_MAP[track[0]]
      else:
        print("WARNING : UNKNOWN GENERAL MIDI CATEGORY")
    
    is_drum = (128 in track)
    
    mask = np.zeros((vocab_size,), dtype=np.bool)
    if is_drum:
      mask[DRUM_TRACK] = True
    else:
      mask[NORMAL_TRACK] = True 

    if track_num == 0 and input_ids is None:
      control.append((PIECE_START, mask))
    else:
      control.append((TRACK_END, mask))

    mask = np.zeros((vocab_size,), dtype=np.bool)
    if not is_drum:
      for allowed_inst in track:
        tok = enc.rep.encode({db.TOKEN_TYPE.INSTRUMENT : allowed_inst})
        mask[tok] = True
      for _ in range(4):
        control.append((BAR_START, mask))
    
    control.append((TRACK_END, ANY_TOKEN_MASK))
  
  #print([(t,np.where(m)[0][:10]) for t,m in control])

  controls = [copy.deepcopy(control) for _ in range(batch_size)]
  tokens = generate_w_injections(
    model, input_ids=input_ids, injects=controls, temperature=temperature)

  return db.tokens_to_json(tokens[0], db.ENCODER_TYPE.TRACK_ENCODER)



def infer_instruments(jstr):
  x = json.loads(jstr)
  track2inst = {}
  for track_num,track in enumerate(x["tracks"]):
    for bar in track.get("bars", []):
      for event in bar.get("events", []):
        inst = x["events"][event]["instrument"]
        track2inst[track_num] = (track["isDrum"], inst)
  return track2inst

#replace_bars(path, [(0,2)], 2, "quick_test", .95)

paths = glob.glob("/Users/jeff/DATA/lmd_full/**/*.mid", recursive=True)

path = np.random.choice(paths)
enc = db.TrackEncoder()
print( enc.rep.pretty(db.midi_to_tokens(path, 12, db.ENCODER_TYPE.TRACK_ENCODER, 0, 6123)) )

exit()


# is there a way to override the tempo so that they all have same tempo ?
# a function to resample n tracks
n_tracks = 3
jstr = ""
while len(jstr) == 0:
  path = np.random.choice(paths)
  jstr = db.midi_to_json(path, 12, n_tracks)

db.json_to_midi("quick_test/json_orig.mid", jstr)
track2inst = infer_instruments(jstr)
track_order = np.arange(n_tracks)

start_jstr = copy.deepcopy(jstr)

for k in range(5):
  jstr = start_jstr
  for i in range(n_tracks):
    flip_order = track_order[1:] # always remove the first track
    jstr = db.partial_copy_json(jstr, flip_order, 0, -1)
    inst = 128 if track2inst[i][0] else track2inst[i][1]
    jstr = run_generation(jstr, len(flip_order), [inst], 1, .95)
    db.json_to_midi("quick_test/json_step_{}_{}.mid".format(k,i), jstr)

# a function to sample n tracks

exit()


fill_model = load_model(
    "gpt2", "TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False", 130000)

# for each model predict a voice set and measure loglik on pitch specifically

norm_losses = []
fill_losses = []

from tqdm import tqdm

for _ in tqdm(range(500)):

  try:
    seed = np.random.randint(2**20)
    path = np.random.choice(paths)
    norm_tokens = np.array(db.encode_to_tokens(
      path, 12, db.ENCODER_TYPE.TRACK_ENCODER, 0, seed))

    fill_tokens = np.array(db.encode_to_tokens(
      path, 12, db.ENCODER_TYPE.TRACK_ONE_BAR_FILL_ENCODER, 0, seed))

    nenc = db.TrackEncoder()
    fenc = db.TrackOneBarFillEncoder()

    tts = [db.TOKEN_TYPE.PITCH, db.TOKEN_TYPE.TIME_DELTA]
    norm_mask = np.array([any([nenc.rep.is_token_type(token,tt) for tt in tts]) for token in norm_tokens])

    fill_mask = np.array([any([fenc.rep.is_token_type(token,tt) for tt in tts]) for token in fill_tokens])

    from torch.nn.functional import nll_loss

    if len(norm_tokens):

      raw_inputs = np.array(norm_tokens)[None,:]
      inputs = torch.from_numpy(raw_inputs).to(torch.long)
      logits = model(inputs)[0].data.numpy()[0]

      logits = logits[norm_mask]
      labels = norm_tokens[norm_mask]

      norm_loss = nll_loss(
        torch.from_numpy(logits), torch.from_numpy(labels).to(torch.long))

      raw_inputs = np.array(fill_tokens)[None,:]
      inputs = torch.from_numpy(raw_inputs).to(torch.long)
      logits = fill_model(inputs)[0].data.numpy()[0]

      logits = logits[fill_mask]
      labels = fill_tokens[fill_mask]

      fill_loss = nll_loss(
        torch.from_numpy(logits), torch.from_numpy(labels).to(torch.long))
      
      fill_losses.append(fill_loss.detach().numpy())
      norm_losses.append(norm_loss.detach().numpy())
  
  except:
    pass

print(len(fill_losses), len(norm_losses))
print(np.mean(fill_losses))
print(np.mean(norm_losses))

from matplotlib import pyplot as plt

plt.hist(fill_losses, color='red', alpha=.5)
plt.hist(norm_losses, color='blue', alpha=.5)
plt.show()


