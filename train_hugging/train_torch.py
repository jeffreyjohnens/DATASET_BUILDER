# monkey patching so that padding works on loss
import torch
import torch.nn.functional as F
def dummy_forward(self, input, target):
  return F.cross_entropy(input, target, weight=self.weight,
    ignore_index=0, reduction=self.reduction)

import dataset_builder_2 as db
def dummy_forward(self, input, target):
  print("CALLED .....")
  if GLOBAL_FILL_LOSS_SWITCH:
    FILL_TOKEN = db.TrackOneBarFillEncoder().rep.encode({db.TOKEN_TYPE.FILL_IN : 1})
    target = target.view(batch_size//args.ngpu,-1) # for 4 gpu setup ...
    target = torch.where(
      torch.cumprod(target!=FILL_TOKEN,1).to(torch.bool) | (target==pad_value), 
      torch.ones_like(target) * pad_value, 
      target)
    target = torch.flatten(target)
  reduced_loss = F.cross_entropy(input, target, weight=self.weight,
    ignore_index=pad_value, reduction=self.reduction)
  return reduced_loss

# fill loss forward is another way
#torch.nn.CrossEntropyLoss.forward = dummy_forward

from transformers import Trainer, TrainingArguments
from transformers import *

import os
import sys
import json
import time
import datetime
import numpy as np
import dataset_builder_2 as db
from dataset_builder_2 import Jagged, ENCODER_TYPE, getEncoderSize

# [[recipe for testing]]
# salloc --time=1:0:0 --ntasks-per-node=32 --mem=0 --gres=gpu:v100l:4 --account=def-pasquier

# salloc --time=1:0:0 --ntasks-per-node=32 --mem=0 --gres=gpu:v100l:1 --account=def-pasquier

# fuser -v /dev/nvidia*
# kill -9 23689

# comment out line 464 in modeling_gpt2.py
# attention_mask = attention_mask.to(dtype=next(self.parameters()).dtype)  # fp16 compatibility

# make the script a bit cleaner for training models.

# python train_torch.py --overwrite 1 --model xlm --encoding TRACK_ENCODER
# python train_torch.py --overwrite 1 --model gpt2 --encoding TRACK_ENCODER --lr .001 --accum_steps 32 --no_eval

# salloc --time=1:0:0 --mem=0 --gres=gpu:p100:4 --account=def-pasquier
# salloc --time=1:0:0 --mem=32000M --gres=gpu:p100:1 --account=def-pasquier
# salloc --time=1:0:0 --mem=32000M --gres=gpu:v1001:1 --account=def-pasquier

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--overwrite", type=int, default=1)
  parser.add_argument("--model", type=str, default="gpt2")
  parser.add_argument("--label", type=str, default="version3")
  parser.add_argument("--dry", type=int, default=0)
  parser.add_argument("--step", type=int, default=0)
  parser.add_argument("--fill_loss", default=False, action='store_true')
  parser.add_argument("--encoding", type=str, required=True)
  parser.add_argument("--name", type=str, default="")
  parser.add_argument("--lr", type=float, default=1e-4)
  parser.add_argument("--ngpu", type=int, default=4)
  parser.add_argument("--accum_steps", type=int, default=1)
  parser.add_argument("--max_seq_len", type=int, default=2048)
  parser.add_argument("--no_eval", action="store_true")
  parser.add_argument("--num_bars", type=int, default=4)
  parser.add_argument("--max_tracks", type=int, default=12)
  parser.add_argument("--batch_size", type=int, default=32)
  parser.add_argument("--num_layers", type=int, default=6)
  args = parser.parse_args()

  np.random.seed(int(time.time()))

  date_str = datetime.datetime.now().strftime('%b_%d_%H_%M')
  encoder_mode = db.getEncoderType(args.encoding)
  assert encoder_mode is not db.ENCODER_TYPE.NO_ENCODER
  vocab_size = getEncoderSize(encoder_mode)

  name = "_".join([args.encoding, args.model, args.label, date_str, str(args.fill_loss), "num_bars", str(args.num_bars), str(args.max_tracks)])
  if len(args.name):
    name = args.name
  
  print("MODEL NAME : " + name)
  print("VOCAB SIZE : " + str(vocab_size))
  print("ARGS : " + json.dumps(vars(args),indent=4))

  GLOBAL_FILL_LOSS_SWITCH = args.fill_loss

  if args.dry:
    args.overwrite = 0
  
  accum_steps = args.accum_steps
  max_seq_len = args.max_seq_len
  batch_size = args.batch_size
  pad_value = -100
  is_transformer_xl = "xl" in args.model

  if sys.platform == "darwin":
    dataset_path = "../dataset_builder_2/test_NUM_BARS=4.arr"
  else:
    # version five increases the number of bars needed to be a valid track
    dataset_path = "/home/jeffe/scratch/data_oct_NUM_BARS=4_OPZ_False.arr"

  logging_dir = "logs/{}".format(name)
  output_dir = "checkpoints/{}".format(name)

  if args.model == "gpt2":
    config = GPT2Config(
      vocab_size=vocab_size,
      n_positions=max_seq_len,
      n_ctx=max_seq_len,
      n_layer=args.num_layers,
      n_head=8,
      n_embd=512,
    )
    model_cls = GPT2LMHeadModel
  elif args.model == "gpt2big":
    config = GPT2Config(
      vocab_size=vocab_size,
      n_positions=max_seq_len,
      n_ctx=max_seq_len,
      n_layer=12,
      n_head=12,
      n_embd=768
    )
    model_cls = GPT2LMHeadModel
  elif args.model == "reformer":
    max_seq_len = 4096
    config = ReformerConfig(
      vocab_size=vocab_size,
      is_decoder=True,
    )
    model_cls = ReformerModelWithLMHead
  elif args.model == "gpt2_tiny":
    max_seq_len = 256
    config = GPT2Config(
      vocab_size=vocab_size,
      n_positions=max_seq_len,
      n_ctx=max_seq_len,
      n_layer=2,
      n_head=2,
      n_embd=128,
    )
    model_cls = GPT2LMHeadModel
  elif args.model == "xl":
    pad_value = 0
    config = TransfoXLConfig(
      vocab_size=vocab_size,
      cutoffs=[],
      n_layer=6,
      n_head=8,
      tgt_len=max_seq_len,
      mem_len=max_seq_len,
      clamp_len=max_seq_len*2,
      d_model=512,
      d_embed=512,
      d_inner=1600,
    )
    model_cls = TransfoXLLMHeadModel
  elif args.model == "xl_tiny":
    
    config = TransfoXLConfig(
      vocab_size=vocab_size,
      cutoffs=[],
      n_layer=1,
      n_head=8,
      tgt_len=max_seq_len,
      mem_len=max_seq_len,
      clamp_len=max_seq_len*2,
      d_model=128,
      d_embed=128,
      d_inner=128,
    )
    model_cls = TransfoXLLMHeadModel
  elif args.model == "xlm":
    config = XLMConfig(
      vocab_size=vocab_size,
      emb_dim=1024,
      n_layers=6,
      n_heads=8,
      causal=True, # need this for autoregressive langauge modelling
      use_lang_emb=False,
      max_position_embeddings=max_seq_len,
      bos_token=0,
      eos_index=-1,
      pad_index=0,
      unk_index=-1,
      mask_index=-1,
    )
    model_cls = XLMWithLMHeadModel
  else:
    raise NotImplementedError

  if args.step == 0:
    ckpt_path = None
    model = model_cls(config)
  else:
    ckpt_path = os.path.join(output_dir, "checkpoint-{}".format(args.step))
    model = model_cls.from_pretrained(ckpt_path)

  print("NUM PARAMETERS : {:,}".format(model.num_parameters()))

  class monkeyPatchDatasetBase:
    def __init__(self,is_training=False):
      self.is_training = is_training
      self.batch_size = batch_size
      self.split_id = 0
      self.batches_per_epoch = 1000
      self.dataset = list(range(self.batches_per_epoch)) # number of examples ??
      self.dataloader = Jagged(dataset_path)
      self.dataloader.set_num_bars(args.num_bars)
      self.dataloader.set_max_tracks(args.max_tracks)
      self.dataloader.set_max_seq_len(max_seq_len)
      seed = np.random.randint(2**20)
      self.dataloader.set_seed(seed)
      self.tc = db.TrainConfig()
      self.tc.num_bars = args.num_bars
      self.tc.max_tracks = args.max_tracks
      self.tc.opz = False

    def _get_batch(self):
      if is_transformer_xl:
        batch, is_continued = self.dataloader.read_batch_w_continue(
          self.batch_size, self.split_id, encoder_mode, max_seq_len, self.is_training==False)
        inputs = np.array(batch)
        outputs = {
          "input_ids" : torch.from_numpy(inputs),
          "labels" : torch.from_numpy(inputs),
          "is_continued" : torch.from_numpy(np.array(is_continued)[None,:,None])
        }
        if not self.is_training:
          outputs.pop("is_continued")
        return outputs
      else:
        batch, mask = self.dataloader.read_batch(
          self.batch_size, self.split_id, encoder_mode, self.tc)
        batch = np.array(batch)
        mask = np.array(mask)
        #batch = np.zeros((self.batch_size,args.max_seq_len)).astype(np.int64)
        #mask = np.ones((self.batch_size,args.max_seq_len)).astype(np.int64)
        labels = np.copy(batch)
        labels += (1-mask) * pad_value # set masked tokens to pad_value
        print(batch.shape, max_seq_len)
        return {
          "input_ids" : torch.from_numpy(np.array(batch)), #[:,:max_seq_len]), 
          "attention_mask" : torch.from_numpy(np.array(mask)), #[:,:max_seq_len]),
          "labels" : torch.from_numpy(labels) #[:,:max_seq_len])
        }

    def __iter__(self):
      self.current = 0
      return self

    def __next__(self):
      self.current += 1
      if self.current <= self.batches_per_epoch:
        return self._get_batch()
      raise StopIteration
    
    def __len__(self):
      return self.batches_per_epoch

  class TrainDataset(monkeyPatchDatasetBase):
    def __init__(self):
      super().__init__(is_training=True)
      self.split_id = 0

  class ValidDataset(monkeyPatchDatasetBase):
    def __init__(self):
      super().__init__()
      self.split_id = 1
      self.batches_per_epoch = 1

  class TestDataset(monkeyPatchDatasetBase):
    def __init__(self):
      super().__init__()
      self.split_id = 2

  Trainer.get_train_dataloader = lambda *args,**kwargs: TrainDataset()
  Trainer.get_eval_dataloader = lambda *args,**kwargs: ValidDataset()
  Trainer.get_test_dataloader = lambda *args,**kwargs: TestDataset()

  
  def custom_training_step(self, model, inputs, optimizer):

    model.train()
    for k, v in inputs.items():
      if isinstance(v, torch.Tensor):
        inputs[k] = v.to(self.args.device)

    # if not continued zero memory
    if hasattr(self, "_past"):
      inputs["mems"] = []
      for mem in self._past:
        shape = mem.shape
        masked_mem = mem.view(-1,batch_size,shape[-1]) * inputs["is_continued"]
        inputs["mems"].append( masked_mem.view(shape) )     
    inputs.pop("is_continued")

    # run through model 
    outputs = model(**inputs)
    #print(outputs[0].shape)
    
    # infer mask and calculate mean loss
    loss = outputs[0]
    loss = loss.contiguous()
    mask = (inputs["labels"][:,1:]!=0).contiguous()
    loss = (loss * mask).sum() / mask.sum()

    #print(loss)

    # keep track of mems
    self._past = outputs[2]

    # this is already handled above
    #if self.args.n_gpu > 1:
    #    loss = loss.mean()  # mean() to average on multi-gpu parallel training
    if self.args.gradient_accumulation_steps > 1:
      loss = loss / self.args.gradient_accumulation_steps

    if self.args.fp16:
      with amp.scale_loss(loss, optimizer) as scaled_loss:
        scaled_loss.backward()
    else:
      loss.backward()

    return loss.item()

  

  # load schedule and optimizer ...
  eval_dur_train = True
  if args.no_eval:
    eval_dur_train = False

  training_args = TrainingArguments(
    logging_dir=logging_dir,
    output_dir=output_dir,
    overwrite_output_dir=bool(args.overwrite),
    num_train_epochs=500*accum_steps, # 500000 total steps
    logging_steps=100, # log every 100 steps
    save_steps=5000,
    save_total_limit=None,
    learning_rate=args.lr,
    gradient_accumulation_steps=accum_steps,
    per_device_train_batch_size=batch_size//args.ngpu//accum_steps,
    per_device_eval_batch_size=batch_size//args.ngpu//accum_steps,
    evaluate_during_training=eval_dur_train,
    #past_index=2 if is_transformer_xl else -1
  )

  trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=None,
    train_dataset=None,
    prediction_loss_only=True,
  )

  # monkey patch my own training step function
  if is_transformer_xl:
    trainer._training_step = custom_training_step.__get__(trainer, Trainer)

  trainer.train(ckpt_path)
