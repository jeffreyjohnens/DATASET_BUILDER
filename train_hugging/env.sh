module load python/3.6
virtualenv --no-download ~/HUGGING
source ~/HUGGING/bin/activate
pip install --no-index --upgrade pip
pip install transformers
pip install tensorflow_gpu
pip install torch