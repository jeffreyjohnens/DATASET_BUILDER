import os
import glob
from subprocess import call

root = "checkpoints"

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--force", action="store_true")
  args = parser.parse_args()

  for run_folder in glob.glob(root + "/*"):
    for ckpt_folder in glob.glob(run_folder + "/*"):
      step = int(ckpt_folder.split("-")[-1])
      if step < 100000:
        cmd = "rm -rf " + ckpt_folder
        if args.force:
          pass
          call(cmd, shell=True)
        else:
          print(cmd)