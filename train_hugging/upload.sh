#!/bin/bash
ssh jeffe@ftp.sfu.ca 'mkdir -p pub_html/js_checkpoints/track_gpt2'
rsync -a js_checkpoints/track_gpt2/61000 jeffe@ftp.sfu.ca:pub_html/js_checkpoints/track_gpt2 --progress
ssh jeffe@ftp.sfu.ca 'chmod -R 711 pub_html/js_checkpoints'
ssh jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/js_checkpoints'