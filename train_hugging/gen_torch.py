from transformers import Trainer, TrainingArguments
from transformers import *

import os
import random
from glob import glob
import sys
import torch
import numpy as np
import dataset_builder_2 as db

# rsync -a jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/track_gpt2/checkpoint-61000 /Users/jeff/CODE/DATASET_BUILDER/train_hugging/checkpoints/track_gpt2/checkpoint-61000

def argsort(seq):
  return sorted(range(len(seq)), key=seq.__getitem__)

def download_model(name,step):
  from subprocess import call
  # download a model from compute canada (cedar)
  ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
  model_folder = os.path.dirname(ckpt_path)
  dst_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), model_folder)
  os.makedirs(dst_folder, exist_ok=True)
  if not os.path.exists(ckpt_path):
    call("rsync -a -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/{}/checkpoint-{} {} --progress".format(name,step,dst_folder), shell=True)
  return ckpt_path

def sample_until(model, input_ids, end_token, temperature=.95):
  input_ids = torch.from_numpy(np.array(input_ids)[None,:]).to(torch.long)
  output = model.generate(
    input_ids, do_sample=True, max_length=len(input_ids) + 512, top_k=0, temperature=temperature, eos_token_id=end_token)
  return output.detach().numpy().flatten()

def split_to_tracks(path,mode):
  mode = db.ENCODER_TYPE.TRACK_ENCODER
  enc = db.TrackEncoder()
  END_TOKEN = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})
  tokens = np.array(db.encode_to_tokens(path, 12, mode))
  if len(tokens) == 0:
    return []
  tracks = np.split(tokens, np.where(tokens==END_TOKEN)[0])
  tracks = [np.concatenate([track[1:], [END_TOKEN]]) for track in tracks]
  return [track for track in tracks if len(track) > 10] # no empty tracks

def generate_w_injections(model, input_ids, use_cache=False):

  past = None
  finished = False
  input_ids = torch.from_numpy(input_ids).to(torch.long)
  
  while not finished:

    if past:
      raw_input = input_ids[:,-1].unsqueeze(-1)

    outputs = model(input_ids=raw_input, past=past, use_cache=use_cache)
    if model._use_cache(outputs, use_cache):
      past = outputs[1]
    
    logits = outputs[0]

midi_paths = glob("../test_midi/**/*.mid", recursive=True)
random.shuffle(midi_paths)

name = "TRACK_ENCODER_gpt2_version1_Jun_23_17_52_False"
step = 145000
mode = db.ENCODER_TYPE.TRACK_ENCODER
enc = db.TrackEncoder()

#name = "track_gpt_loss_fix"
#step = 165000
#mode = db.ENCODER_TYPE.TRACK_ENCODER
#enc = db.TrackEncoder()

model_path = download_model(name,step)
result_directory = "samples/{}/{}".format(name,step)

model = GPT2LMHeadModel.from_pretrained(model_path, from_tf=False)
print("NUM PARAMETERS : {:,}".format(model.num_parameters()))

track_token = enc.rep.encode({db.TOKEN_TYPE.TRACK : 0})
end_token = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})

"""
# bar replacement from a sample
for path in midi_paths:
  mode = db.ENCODER_TYPE.ORDERED_TRACK_BAR_ENCODER
  enc = db.OrderedTrackBarEncoder()
  END_BAR_TOKEN = enc.rep.encode({db.TOKEN_TYPE.BAR_END : 0})
  FILL_TOKEN = enc.rep.encode({db.TOKEN_TYPE.FILL_IN : 0})
  # can we pass args into the encoding ..
  # so that we can control ordering of bars
  # and where the fill in point is ... ?
  # provide the split point ...
  tokens = np.array(db.encode_to_tokens(path, 12, mode))
  if len(tokens) > 0:

    db.decode("temp_ORIG.mid", tokens, mode)

    ii = enc.rep.where(tokens, db.TOKEN_TYPE.FILL_IN)[0]

    prompt = list(tokens[:ii])
    after = tokens[ii+1:]

    tracks = enc.rep.where_values(after, db.TOKEN_TYPE.TRACK)
    bars = enc.rep.where_values(after, db.TOKEN_TYPE.BAR)

    for track,bar in zip(tracks,bars):
      track = enc.rep.encode({db.TOKEN_TYPE.TRACK : track})
      bar = enc.rep.encode({db.TOKEN_TYPE.BAR : bar})
      cur_prompt = prompt + [FILL_TOKEN, track, bar]
      enc.rep.show(cur_prompt[-10:])
      prompt = list(sample_until(model, cur_prompt, END_BAR_TOKEN))
      print("added {} tokens".format(len(prompt) - len(cur_prompt)))
      prompt.remove(FILL_TOKEN)

      # tracks must be re-ordered ...
      bar_idx = np.array(enc.rep.where(prompt, db.BAR_END))
      token_bars = np.split(prompt[1:], bar_idx)
      cur_tracks = enc.rep.where_values(prompt, db.TOKEN_TYPE.TRACK)
      cur_bars = enc.rep.where_values(prompt, db.TOKEN_TYPE.BAR)
      order = argsort(list(zip(cur_tracks, cur_bars)))
      
      # make sure to add piece start at the front ...
      prompt = list(np.hstack([0] + [token_bars[i] for i in order if i < len(token_bars)]))
      
    db.decode("temp.mid", prompt, mode)
    exit()

exit()
"""

"""
# do a variety of sampling methods as well ...
temperature = .95
num_beams = 1
n_prompt_tracks = 3

folder = "combination_test/temp={}_beams={}_nprompt={}".format(temperature,num_beams,n_prompt_tracks)
os.makedirs(folder, exist_ok=True)


for midi_path in midi_paths:
  bname = os.path.splitext(os.path.basename(midi_path))[0]
  tracks = split_to_tracks(midi_path)
  if len(tracks) > 0:
    for i in range(10):
      tracks = split_to_tracks(midi_path)
      while len(tracks) == 0:
        tracks = split_to_tracks(midi_path)
      prompt = np.hstack(tracks[:n_prompt_tracks]) # up to first two tracks

      input_ids = np.hstack([[0],prompt])[None,:]
      input_ids = torch.from_numpy(input_ids).to(torch.long)
      output = model.generate(
        input_ids, do_sample=True, max_length=len(prompt) + 512, top_k=0, temperature=temperature, num_beams=num_beams)
      output = output.detach().numpy().flatten()
      # limit to max one extra track
      n_tracks = np.sum(prompt == end_token)
      n_gen_tracks = np.sum(output == end_token)
      for j in range(min(4,n_gen_tracks - n_tracks)):
        end = np.where(output == end_token)[0][n_tracks+j]

        gen_path = os.path.join(folder,bname+"_{}_GEN_{}_TRACK.mid".format(i,j+1))
        db.decode(gen_path, output[:end+1], mode)

      
      prompt_path = os.path.join(folder,bname+"_{}_PROMPT.mid".format(i))
      db.decode(prompt_path, prompt, mode)

exit()
"""

# standard from scratch sampling below
import time
start = time.time()

input_ids = torch.from_numpy(np.zeros((1,1), dtype=np.int64))
output = model.generate(input_ids, do_sample=True, max_length=1024, top_k=0, temperature=.9)
output = output.detach().numpy()

print(time.time() - start)

if len(result_directory):
  os.makedirs(result_directory, exist_ok=True)
for i,seq in enumerate(output):
  midi_path = os.path.join(result_directory, "{}.mid".format(i))
  #db.decode(midi_path, seq, mode)
  try:
    seq = seq[:np.where(seq==end_token)[0][4]]
  except:
    print("prunned tracks", i)
    pass

  db.decode(midi_path, seq, mode)
    
