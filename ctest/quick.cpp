#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <tuple>

using namespace std;

int main() {

  srand(time(NULL));

  vector<pair<int,int>> x;
  for (int i=0; i<8; i++) {
    for (int j=0; j<4; j++) {
      x.push_back(make_pair(i,j));
    }
  }

  random_shuffle(x.begin(), x.end());

  int split = rand() % x.size();
  sort(x.begin(), x.begin() + split);
  sort(x.begin() + split, x.end());

  int i = 0;
  int pos = 0;
  vector<pair<int,int>> y(32, make_pair(-1,-1));
  for (int i=0; i<split; i++) {
    y[get<0>(x[i])*4 + get<1>(x[i])] = x[i];
  }

  cout << "SPLIT : " << split << endl;
  for (int i=0; i<(int)y.size(); i++) {
    cout << get<0>(y[i]) << " " << get<1>(y[i]) << endl;
  }

}