from tensor2tensor.models import transformer, mtf_transformer

transformer_hp = transformer.transformer_base()
mtf_transformer_hp = mtf_transformer.mtf_transformer_base()

replica = {
  "d_kv" : 384,
  "d_model" : 384,

}

for k,v in mtf_transformer_hp.__dict__.items():
  if k not in transformer_hp.__dict__:
    v = replica.get(k,v)
    transformer_hp.add_hparam(k,v)
    print("adding {}={}".format(k,v))

