#!/bin/bash
rm dataset_builder_2.zip

# overwrite the version.h
TIMESTAMP=`date +%Y-%m-%d_%H-%M-%S`
printf "#include <string>\rstd::string version() { return \"$TIMESTAMP\"; }" > dataset_builder_2/src/dataset_builder_2/version.h

zip -r dataset_builder_2.zip dataset_builder_2/ -x "dataset_builder_2/*.arr" -x "dataset_builder_2/dist/*" -x "dataset_builder_2/build/*" -x "dataset_builder_2/dataset_builder_2.egg-info/*" -x "dataset_builder_2/__pycache__/*" -x "dataset_builder_2/STAT_note_density.npz"

ssh -i /users/jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'mkdir -p pub_html/dataset_builder_2'
rsync -e 'ssh -i /users/jeff/.ssh/sfuweb_rsa' dataset_builder_2.zip jeffe@ftp.sfu.ca:pub_html/dataset_builder_2/dataset_builder_2.zip --progress
ssh -i /users/jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'chmod -R 711 pub_html/dataset_builder_2'
ssh -i /users/jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/dataset_builder_2'

# http://www.sfu.ca/~jeffe/dataset_builder_2/dataset_builder_2.zip