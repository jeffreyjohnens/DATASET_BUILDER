bar_view = [
  "NOTE_ON=60",
  "TIME_DELTA=2",
  "NOTE_OFF=60",
  "NOTE_ON=64",
  "NOTE_ON=67",
  "TIME_DELTA=4",
  "NOTE_OFF=64",
  "TIME_DELTA=4",
  "NOTE_OFF=67",
]

voice_view = [
  "INST=30",
  "DENSITY=5",
  "BAR_START",
  "<BAR>",
  "BAR_END",
  "BAR_START",
  "<BAR>",
  "BAR_END",
  "BAR_START",
  "<BAR>",
  "BAR_END",
  "BAR_START",
  "<BAR>",
  "BAR_END",
]

piece_view = [
  "PIECE_START",
  "TRACK_START",
  "<TRACK>",
  "TRACK_END",
  "TRACK_START",
  "<TRACK>",
  "TRACK_END",
  "TRACK_START",
  "<TRACK>",
  "TRACK_END",
]

fill_view = [
  "PIECE_START",
  "TRACK_START",
  "INST=30",
  "DENSITY=5",
  "BAR_START",
  "FILL_IN",
  "BAR_END",
  "...",
  "TRACK_END",
  "FILL_START",
  "<BAR>",
  "FILL_END",
  "FILL_START",
  "<BAR>",
  "FILL_END"
]

def make_node(x,y,content):
  content = content.replace("_","\\_")
  alpha = 10
  if content.startswith("<") or content.startswith("."):
    alpha = 30
  return """\\node[draw=none,fill=gray!{alpha},anchor=north west,minimum width=8cm,minimum height=1.5cm] at ({x},{y}) {{\\myBody{{{content}}}}};""".format(x=x,y=y,content=content,alpha=alpha)

for i,token in enumerate(bar_view):
  print(make_node(0,-2*i,token))

for i,token in enumerate(voice_view):
  print(make_node(10.5,-2*i,token))

for i,token in enumerate(piece_view):
  print(make_node(21,-2*i,token))

for i,token in enumerate(fill_view):
  print(make_node(40,-2*i,token))
