# take a json file and create a piano roll pdf
# 4 bar examples

import json
import cairo
import os

def get_notes(track):
  notes = []
  onsets = {}
  for bar in track.get("bars",[]):
    for event_id in bar.get("events",[]):
      event = midi_json["events"][event_id]
      if event["velocity"] > 0:
        onsets[event["pitch"]] = event
      elif event["pitch"] in onsets:
        notes.append({
          "start" : onsets[event["pitch"]]["time"],
          "end" : event["time"],
          "pitch" : event["pitch"],
        })
        onsets.pop(event["pitch"])
  for _,v in onsets.items():
    notes.append({
      "start" : v["time"],
      "end" : 192,
      "pitch" : v["pitch"],
    })
  return notes

def draw_rect(cr, x, y, w, h, color):
  color = list(color)
  if isinstance(color[0],int):
    color = [float(c)/256 for c in color]
  if len(color) == 3:
    color.append(256)
  cr.set_source_rgba(*color)
  cr.rectangle(x, y, w, h)
  cr.fill()

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('--input', required=True, type=str)
  parser.add_argument('--ntracks', type=int, default=2048)
  parser.add_argument('--bars', nargs="+", type=int)
  args = parser.parse_args()

  bars = {}
  if args.bars is not None:
    for t,b in zip(args.bars[::2],args.bars[1::2]):
      bars[(t,b)] = True

  with open(args.input, "r") as f:
    midi_json = json.load(f)

  nh = 1
  nw = 2
  margin = 10
  pad = 5

  max_pitch = 80
  min_pitch = 20
  pitch_range = max_pitch - min_pitch

  bar_width = 48*nw
  roll_width = 192*nw
  track_height = pitch_range*nh

  num_tracks = min(args.ntracks,len(midi_json["tracks"]))

  output = os.path.splitext(args.input)[0] + ".pdf"
  pdf = cairo.PDFSurface(output, roll_width + 2*margin, num_tracks*track_height + 2*margin + (num_tracks-1)*pad)
  cr = cairo.Context(pdf)

  # get the notes first
  for track_num,track in enumerate(midi_json["tracks"][:num_tracks]):

    # draw the background for a track
    draw_rect(cr, margin, margin + track_num*track_height + track_num*pad, roll_width, track_height, (.8,.8,.8))

    # draw highlighted bars if there are any
    for bar_num in range(4):
      if (track_num,bar_num) in bars:
        draw_rect(cr, margin + bar_num*bar_width, margin + track_num*track_height + track_num*pad, bar_width, track_height, (66,133,244,128))

    notes = get_notes(track)
    for note in notes:
      if note["pitch"] > min_pitch and note["pitch"] <= max_pitch:
        pitch = pitch_range - (note["pitch"] - min_pitch)
        draw_rect(
          cr, 
          note["start"]*nw + margin, 
          pitch*nh + track_num*track_height + track_num*pad + margin, 
          (note["end"]-note["start"])*nw, 
          nh, 
          (219,68,55))
  
  
      
  