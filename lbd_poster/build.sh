python3 roll_pdf.py --input ex1.json --ntracks 2
python3 roll_pdf.py --input ex2.json --ntracks 3 --bars 2 0 2 1 2 2 2 3
python3 roll_pdf.py --input ex3.json --ntracks 3
python3 roll_pdf.py --input ex4.json --ntracks 3 --bars 0 0 0 3 1 1 1 2 2 1
#python3 roll_pdf.py --input exlow.json --ntracks 1 --bars 0 0 0 1 0 2 0 3
#python3 roll_pdf.py --input exmed.json --ntracks 1 --bars 0 0 0 1 0 2 0 3
#python3 roll_pdf.py --input exhigh.json --ntracks 1 --bars 0 0 0 1 0 2 0 3
python3 roll_pdf.py --input exlow.json --ntracks 1
python3 roll_pdf.py --input exmed.json --ntracks 1
python3 roll_pdf.py --input exhigh.json --ntracks 1
python3 roll_pdf.py --input exit1.json --ntracks 1 
python3 roll_pdf.py --input exit2.json --ntracks 1 --bars 0 0 0 3
python3 roll_pdf.py --input exit3.json --ntracks 1 --bars 0 1 0 2
python3 roll_pdf.py --input exit4.json --ntracks 1 --bars 0 0