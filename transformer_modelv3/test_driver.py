import os
from model import train, generate

os.makedirs("demo", exist_ok=True)

status = '{"tracks":[{"track_id":0,"inst":"Acoustic Grand Piano","density":1,"density_disabled":true,"resample":false,"highlighted_bars":[false,false,false,false]},{"track_id":1,"inst":"Acoustic Grand Piano","density":1,"density_disabled":true,"resample":false,"highlighted_bars":[false,false,false,false]},{"track_id":2,"inst":"drum_0","density":1,"density_disabled":true,"resample":false,"highlighted_bars":[false,false,false,false]},{"track_id":3,"inst":"guitar","density":1,"density_disabled":true,"resample":true,"highlighted_bars":[true,true,true,true]}],"tempo":"240","nbars":"4"}'

#train("demo", "demo/test_model.json", 0)
generate("demo", 2, .9, "demo/test_model.json", "demo/0.mid", None, 'checkpoints', status)