import os
import json
import copy
import dataset_builder_2 as db
from subprocess import call
from midi2audio import FluidSynth

def swap_extension(path, ext):
  return os.path.splitext(path)[0] + ext

def load_midi(path):
  encoder = db.TrackEncoder()
  #encoder = db.TrackBarFillDensityEncoder()
  ec = db.EncoderConfig()
  ec.do_track_shuffle = False
  ec.force_four_four = True
  #ec.force_valid = True
  return json.loads(encoder.midi_to_json(path,ec))

def json_to_midi(x, path):
  encoder = db.TrackEncoder()
  ec = db.EncoderConfig()
  ec.do_track_shuffle = False
  ec.force_four_four = True
  encoder.json_to_midi(json.dumps(x),path,ec)

def load_json(path):
  with open(path, "r") as f:
    return json.load(f)

def dump_json(x, path):
  with open(path, "w") as f:
    return json.dump(x,f)

def load_json_or_midi(path):
  if not os.path.exists(path):
    return load_midi(swap_extension(path, ".mid"))
  return load_json(path)

def json_to_wav(x, path):
  json_to_midi(x, swap_extension(path, ".mid"))
  fs = FluidSynth("local_notebook/font.sf2")
  fs.midi_to_audio(swap_extension(path, ".mid"), swap_extension(path, ".wav"))
  return swap_extension(path, ".wav")

def upload_folder(path):
  id_rsa = "/Users/Jeff/.ssh/sfuweb_rsa"
  call("ssh -i {} jeffe@ftp.sfu.ca 'mkdir -p pub_html/{}'".format(id_rsa, path), shell=True)
  call("rsync -e 'ssh -i {}' -a {} jeffe@ftp.sfu.ca:pub_html/{} --progress".format(id_rsa, path, os.path.dirname(path)), shell=True)
  call("ssh -i {} jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/{}'".format(id_rsa,os.path.basename(path)), shell=True)

#midi_json = load_json("gen/stem_acous.json")
#dump_json( json.loads(db.prune_tracks(json.dumps(midi_json), [0,1,2,4,5])), "gen/stem_acous.json")
#exit()

# manipulate the velocity levels per track
# first squash them all to one value then set to one of ten levels [0,9]
def mix_tracks_in_json(midi_json, levels):
  AUDIO_LEVELS = [12,24,36,48,60,72,84,96,108,120]
  for track_num, track in enumerate(midi_json.get("tracks",[])):
    for bar in track.get("bars",[]):
      for event_index in bar.get("events",[]):
        event = midi_json["events"][event_index]
        if event["velocity"] > 0:
          event["velocity"] = AUDIO_LEVELS[levels[track_num]]


sfu_prefix = "https://www.sfu.ca/~jeffe/"
folder_name = "examples"

template = load_json("template.json")
result = copy.deepcopy(template)

#call("rm -rf " + folder_name, shell=True)
#call("mkdir " + folder_name, shell=True)

sample_count = 1
letter_count = 0
prev_id = None
root_id = None
last_sample_id = None
for i,section in enumerate(result):
  for i,example in enumerate(section["examples"]):
    json_path = example["src"]
    basepath = os.path.basename(json_path)
    path = os.path.join(folder_name, basepath)
    
    midi_json = load_json_or_midi( json_path )

    # remix json
    if example.get("mixer",None) is not None:
      mix_tracks_in_json(midi_json, example["mixer"])

    # set tempo
    if example.get("tempo",None) is not None:
      midi_json["tempo"] = example["tempo"]
    
    # this section makes sure that the sample ids and refs are correct
    # update the sample id
    if last_sample_id is not None and last_sample_id != example["sample_id"]:
      sample_count += 1
      letter_count = 0
    last_sample_id = example["sample_id"]
    example["sample_id"] = str(sample_count) + chr(97+letter_count)
    
    # update the caption so refs to sample id are accurate
    if prev_id is not None:
      example["caption"] = example["caption"].replace("$PREV$", prev_id)
      example["caption"] = example["caption"].replace("$ROOT$", root_id)
    prev_id = example["sample_id"]
    if letter_count == 0:
      root_id = example["sample_id"]
    letter_count += 1

    example["json"] = midi_json

    # make the json file
    dump_json(midi_json, swap_extension(path, ".json"))

    audio_path = swap_extension(path, ".wav")

    if not os.path.exists(swap_extension(audio_path, ".wav")):
      json_to_wav(midi_json, path)
    
    if not os.path.exists(swap_extension(audio_path, ".mp3")):
      call("ffmpeg -i {input} -codec:a libmp3lame -qscale:a 2 {output}".format(input=audio_path, output=swap_extension(audio_path, ".mp3")), shell=True)
    
    # add path to json
    example["audio"] = os.path.join(sfu_prefix, swap_extension(audio_path, ".mp3"))

# total is the live one ...
dump_json(result, os.path.join(folder_name, "total.json"))

# add the pdf to the folder as well
call("cp TRANSFORMER_PAPER.pdf examples/MMM.pdf", shell=True)
upload_folder(folder_name)

