# turn a midi file into a bunch of stems
import os
import json
import glob
import random
import dataset_builder_2 as db
from subprocess import call

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)

call("rm -rf stems", shell=True)
call("mkdir stems", shell=True)

num_tracks = 4
stems = []
while len(stems) < 100:
  try:
    path = random.choice(paths)
    encoder = db.TrackEncoder()
    e = db.EncoderConfig()
    e.num_bars = 8
    x = encoder.midi_to_json(path,e)
    x = db.select_segment(x, 6, -1)
    midi_json = json.loads(x)
    if len(midi_json.get("tracks",[])) >= num_tracks:
      e.default_tempo = json.loads(x)["tempo"]
      encoder.json_to_midi(x, "stems/stem_{}.mid".format(len(stems)), e)
      stems.append( json.loads(x) )
  except:
    pass

with open("get_stems.json", "w") as f:
  json.dump(stems,f)