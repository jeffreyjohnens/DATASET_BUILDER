
import json
import dataset_builder_2 as db

def get_current_midi():
  with open("current_midi.json", "r") as f:
    return json.load(f)

def mix_tracks_in_json(midi_json, levels=None):
  AUDIO_LEVELS = [12,24,36,48,60,72,84,96,108,120]
  for track_num, track in enumerate(midi_json.get("tracks",[])):
    for bar in track.get("bars",[]):
      for event_index in bar.get("events",[]):
        event = midi_json["events"][event_index]
        if event["velocity"] > 0:
          audio_level = AUDIO_LEVELS[8]
          if levels is not None:
            audio_level = AUDIO_LEVELS[levels[track_num]]
          event["velocity"] = audio_level

def validate_midi_json(midi_json):
  bar_lengths = []
  for track in midi_json.get("tracks",[]):
    bars = track.get("bars",[])
    bar_lengths.append( len(bars) )
    print(len(bars), [bar.get("events",[]) for bar in bars])
    for bar in bars:
      for event_index in bar.get("events",[]):
        assert event_index < len(midi_json.get("events",[]))
  print(bar_lengths)
      


def play_callback(status):
  midi_json = get_current_midi()
  tracks = list(range(len(midi_json.get("tracks",[]))))

  #validate_midi_json(midi_json)
  #exit()

  encoder = db.TrackEncoder()
  e = db.EncoderConfig()
  e.do_track_shuffle = False
  
  midi_json["tempo"] = 120
  mix_tracks_in_json(midi_json, )
  raw = json.dumps(midi_json)

  print("haweruhb")
  raw = db.prune_tracks(raw, tracks)

  print("here")

  encoder.json_to_midi(raw, "current.mid", e)
  exit()
  FluidSynth("font.sf2").midi_to_audio('current.mid', 'current.wav')
  
  # set the src and play
  sound = open("current.wav", "rb").read()
  sound_encoded = base64.b64encode(sound).decode('ascii')
  script = '''<script type="text/javascript">
  var audio = document.querySelector("#beep");
  audio.src = "data:audio/wav;base64,{raw_audio}";
  audio.play();
  </script>'''.format(raw_audio=sound_encoded)
  display(HTML(script))

play_callback({})