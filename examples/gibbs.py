import os
import json
import glob
import random
import dataset_builder_2 as db
from subprocess import call

from tqdm import tqdm

import re
import base64
import copy
import time
import torch
import numpy as np
from transformers import *

GENERAL_MIDI_MAP_BASIC = {
  "any" : list(range(129)),
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [128]
}

ANY_DRUM = 256
GENERAL_MIDI_MAP = {
  "any" : list(range(128)) + [ANY_DRUM],
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [ANY_DRUM],
  "Acoustic Grand Piano" : [0],
  "Bright Acoustic Piano" : [1],
  "Electric Grand Piano" : [2],
  "Honky-tonk Piano" : [3],
  "Electric Piano 1" : [4],
  "Electric Piano 2" : [5],
  "Harpsichord" : [6],
  "Clavi" : [7],
  "Celesta" : [8],
  "Glockenspiel" : [9],
  "Music Box" : [10],
  "Vibraphone" : [11],
  "Marimba" : [12],
  "Xylophone" : [13],
  "Tubular Bells" : [14],
  "Dulcimer" : [15],
  "Drawbar Organ" : [16],
  "Percussive Organ" : [17],
  "Rock Organ" : [18],
  "Church Organ" : [19],
  "Reed Organ" : [20],
  "Accordion" : [21],
  "Harmonica" : [22],
  "Tango Accordion" : [23],
  "Acoustic Guitar (nylon)" : [24],
  "Acoustic Guitar (steel)" : [25],
  "Electric Guitar (jazz)" : [26],
  "Electric Guitar (clean)" : [27],
  "Electric Guitar (muted)" : [28],
  "Overdriven Guitar" : [29],
  "Distortion Guitar" : [30],
  "Guitar Harmonics" : [31],
  "Acoustic Bass" : [32],
  "Electric Bass (finger)" : [33],
  "Electric Bass (pick)" : [34],
  "Fretless Bass" : [35],
  "Slap Bass 1" : [36],
  "Slap Bass 2" : [37],
  "Synth Bass 1" : [38],
  "Synth Bass 2" : [39],
  "Violin" : [40],
  "Viola" : [41],
  "Cello" : [42],
  "Contrabass" : [43],
  "Tremolo Strings" : [44],
  "Pizzicato Strings" : [45],
  "Orchestral Harp" : [46],
  "Timpani" : [47],
  "String Ensemble 1" : [48],
  "String Ensemble 2" : [49],
  "Synth Strings 1" : [50],
  "Synth Strings 2" : [51],
  "Choir Aahs" : [52],
  "Voice Oohs" : [53],
  "Synth Voice" : [54],
  "Orchestra Hit" : [55],
  "Trumpet" : [56],
  "Trombone" : [57],
  "Tuba" : [58],
  "Muted Trumpet" : [59],
  "French Horn" : [60],
  "Brass Section" : [61],
  "Synth Brass 1" : [62],
  "Synth Brass 2" : [63],
  "Soprano Sax" : [64],
  "Alto Sax" : [65],
  "Tenor Sax" : [66],
  "Baritone Sax" : [67],
  "Oboe" : [68],
  "English Horn" : [69],
  "Bassoon" : [70],
  "Clarinet" : [71],
  "Piccolo" : [72],
  "Flute" : [73],
  "Recorder" : [74],
  "Pan Flute" : [75],
  "Blown bottle" : [76],
  "Shakuhachi" : [77],
  "Whistle" : [78],
  "Ocarina" : [79],
  "Lead 1 (square)" : [80],
  "Lead 2 (sawtooth)" : [81],
  "Lead 3 (calliope)" : [82],
  "Lead 4 (chiff)" : [83],
  "Lead 5 (charang)" : [84],
  "Lead 6 (voice)" : [85],
  "Lead 7 (fifths)" : [86],
  "Lead 8 (bass + lead)" : [87],
  "Pad 1 (new age)" : [88],
  "Pad 2 (warm)" : [89],
  "Pad 3 (polysynth)" : [90],
  "Pad 4 (choir)" : [91],
  "Pad 5 (bowed)" : [92],
  "Pad 6 (metallic)" : [93],
  "Pad 7 (halo)" : [94],
  "Pad 8 (sweep)" : [95],
  "FX 1 (rain)" : [96],
  "FX 2 (soundtrack)" : [97],
  "FX 3 (crystal)" : [98],
  "FX 4 (atmosphere)" : [99],
  "FX 5 (brightness)" : [100],
  "FX 6 (goblins)" : [101],
  "FX 7 (echoes)" : [102],
  "FX 8 (sci-fi)" : [103],
  "Sitar" : [104],
  "Banjo" : [105],
  "Shamisen" : [106],
  "Koto" : [107],
  "Kalimba" : [108],
  "Bag pipe" : [109],
  "Fiddle" : [110],
  "Shanai" : [111],
  "Tinkle Bell" : [112],
  "Agogo" : [113],
  "Steel Drums" : [114],
  "Woodblock" : [115],
  "Taiko Drum" : [116],
  "Melodic Tom" : [117],
  "Synth Drum" : [118],
  "Reverse Cymbal" : [119],
  "Guitar Fret Noise" : [120],
  "Breath Noise" : [121],
  "Seashore" : [122],
  "Bird Tweet" : [123],
  "Telephone Ring" : [124],
  "Helicopter" : [125],
  "Applause" : [126],
  "Gunshot" : [127],

  "drum_0" : [128],
  "drum_1" : [129],
  "drum_2" : [130],
  "drum_3" : [131],
  "drum_4" : [132],
  "drum_5" : [133],
  "drum_6" : [134],
  "drum_7" : [135],
  "drum_8" : [136],
  "drum_9" : [137],
  "drum_10" : [138],
  "drum_11" : [139],
  "drum_12" : [140],
  "drum_13" : [141],
  "drum_14" : [142],
  "drum_15" : [143],
  "drum_16" : [144],
  "drum_17" : [145],
  "drum_18" : [146],
  "drum_19" : [147],
  "drum_20" : [148],
  "drum_21" : [149],
  "drum_22" : [150],
  "drum_23" : [151],
  "drum_24" : [152],
  "drum_25" : [153],
  "drum_26" : [154],
  "drum_27" : [155],
  "drum_28" : [156],
  "drum_29" : [157],
  "drum_30" : [158],
  "drum_31" : [159],
  "drum_32" : [160],
  "drum_33" : [161],
  "drum_34" : [162],
  "drum_35" : [163],
  "drum_36" : [164],
  "drum_37" : [165],
  "drum_38" : [166],
  "drum_39" : [167],
  "drum_40" : [168],
  "drum_41" : [169],
  "drum_42" : [170],
  "drum_43" : [171],
  "drum_44" : [172],
  "drum_45" : [173],
  "drum_46" : [174],
  "drum_47" : [175],
  "drum_48" : [176],
  "drum_49" : [177],
  "drum_50" : [178],
  "drum_51" : [179],
  "drum_52" : [180],
  "drum_53" : [181],
  "drum_54" : [182],
  "drum_55" : [183],
  "drum_56" : [184],
  "drum_57" : [185],
  "drum_58" : [186],
  "drum_59" : [187],
  "drum_60" : [188],
  "drum_61" : [189],
  "drum_62" : [190],
  "drum_63" : [191],
  "drum_64" : [192],
  "drum_65" : [193],
  "drum_66" : [194],
  "drum_67" : [195],
  "drum_68" : [196],
  "drum_69" : [197],
  "drum_70" : [198],
  "drum_71" : [199],
  "drum_72" : [200],
  "drum_73" : [201],
  "drum_74" : [202],
  "drum_75" : [203],
  "drum_76" : [204],
  "drum_77" : [205],
  "drum_78" : [206],
  "drum_79" : [207],
  "drum_80" : [208],
  "drum_81" : [209],
  "drum_82" : [210],
  "drum_83" : [211],
  "drum_84" : [212],
  "drum_85" : [213],
  "drum_86" : [214],
  "drum_87" : [215],
  "drum_88" : [216],
  "drum_89" : [217],
  "drum_90" : [218],
  "drum_91" : [219],
  "drum_92" : [220],
  "drum_93" : [221],
  "drum_94" : [222],
  "drum_95" : [223],
  "drum_96" : [224],
  "drum_97" : [225],
  "drum_98" : [226],
  "drum_99" : [227],
  "drum_100" : [228],
  "drum_101" : [229],
  "drum_102" : [230],
  "drum_103" : [231],
  "drum_104" : [232],
  "drum_105" : [233],
  "drum_106" : [234],
  "drum_107" : [235],
  "drum_108" : [236],
  "drum_109" : [237],
  "drum_110" : [238],
  "drum_111" : [239],
  "drum_112" : [240],
  "drum_113" : [241],
  "drum_114" : [242],
  "drum_115" : [243],
  "drum_116" : [244],
  "drum_117" : [245],
  "drum_118" : [246],
  "drum_119" : [247],
  "drum_120" : [248],
  "drum_121" : [249],
  "drum_122" : [250],
  "drum_123" : [251],
  "drum_124" : [252],
  "drum_125" : [253],
  "drum_126" : [254],
  "drum_127" : [255],
}

REVERSE_INSTRUMENT_TRACK = {}
for k,v in GENERAL_MIDI_MAP.items():
  if len(v) == 1:
    for vv in v:
      REVERSE_INSTRUMENT_TRACK[vv] = k

GENRE_TAG_MAP = {
  "Disabled" : -1,
  "Blues" : 0,
  "Country" : 1,
  "Electronic" : 2,
  "Folk" : 3,
  "Jazz" : 4,
  "Latin" : 5,
  "Metal" : 6,
  "New Age" : 7,
  "Pop" : 8,
  "Punk" : 9,
  "Rap" : 10,
  "Reggae" : 11,
  "RnB" : 12,
  "Rock" : 13,
  "World" : 14,
  "none" : 15
}

string2tokentype = {
  "PIECE_START": db.TOKEN_TYPE.PIECE_START,
  "NOTE_ONSET": db.TOKEN_TYPE.NOTE_ONSET,
  "NOTE_OFFSET": db.TOKEN_TYPE.NOTE_OFFSET,
  "PITCH": db.TOKEN_TYPE.PITCH,
  "NON_PITCH": db.TOKEN_TYPE.NON_PITCH,
  "VELOCITY": db.TOKEN_TYPE.VELOCITY,
  "TIME_DELTA": db.TOKEN_TYPE.TIME_DELTA,
  "INSTRUMENT": db.TOKEN_TYPE.INSTRUMENT,
  "BAR": db.TOKEN_TYPE.BAR,
  "BAR_END": db.TOKEN_TYPE.BAR_END,
  "TRACK": db.TOKEN_TYPE.TRACK,
  "TRACK_END": db.TOKEN_TYPE.TRACK_END,
  "DRUM_TRACK": db.TOKEN_TYPE.DRUM_TRACK,
  "FILL_IN": db.TOKEN_TYPE.FILL_IN,
  "HEADER": db.TOKEN_TYPE.HEADER,
  "VELOCITY_LEVEL": db.TOKEN_TYPE.VELOCITY_LEVEL,
  "GENRE": db.TOKEN_TYPE.GENRE,
  "DENSITY_LEVEL" : db.TOKEN_TYPE.DENSITY_LEVEL,
}

class Control:
  def __init__(self):
    self.controls = []
  def force_list(self, x):
    from collections import Iterable
    if not isinstance(x, Iterable):
      return [x]
    return x
  def encode(self, encoder, values):
    if values is None:
      return None
    values = {string2tokentype[k]:self.force_list(v) for k,v in values.items()}
    return np.array(encoder.rep.encode_to_one_hot(values)).astype(np.bool)
  def add(self, trigger, mask):
    self.controls.append((trigger,mask))
  def build(self, encoder):
    output = []
    for trigger,mask in self.controls:
      output.append((self.encode(encoder,trigger), self.encode(encoder,mask)))
    return output

def write_debug(msg):
  pass

#===============================================================================
#===============================================================================

class sampler:

  def __init__(self, ckpt_path=None, name=None, step=None, web=True, force=False):
    if ckpt_path is None:
      if web:
        ckpt_path = self.download_model_from_web(name, step, force=force)
      else:
        ckpt_path = self.download_model(name, step)
    # infer the model cls from the filename
    mstr = re.findall("(gpt2|xl)", ckpt_path)[0]
    if mstr == "gpt2":
      self.model = GPT2LMHeadModel.from_pretrained(ckpt_path, from_tf=False)
    elif mstr == "xl":
      self.model = TransfoXLLMHeadModel.from_pretrained(ckpt_path, from_tf=False)
    else:
      raise NotImplementedError
    # infer encoder from ckpt_path
    estr = re.findall("(TRACK_DENSITY_VELOCITY_ENCODER|TRACK_DENSITY_ENCODER|TRACK_BAR_FILL_DENSITY_VELOCITY_ENCODER|TRACK_BAR_FILL_DENSITY_ENCODER|TRACK_BAR_FILL_SIXTEEN_ENCODER|TRACK_DENSITY_ENCODER|TRACK_GENRE_ENCODER|TRACK_ENCODER|TRACK_BAR_FILL_ENCODER|TRACK_MONO_POLY_DENSITY_ENCODER)", ckpt_path)[0]
    self.encoder = db.getEncoder(db.getEncoderType(estr))

  def download_model_from_web(self,name,step,force=False):
    file_list = ["config.json", "pytorch_model.bin", "scheduler.pt", "training_args.bin"] # don't need optimizer
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    os.makedirs(ckpt_path, exist_ok=True)
    for file in file_list:
      path = os.path.join(ckpt_path,file)
      if force or not os.path.exists(path):
        call("wget http://www.sfu.ca/~jeffe/{}/{} -O {}".format(ckpt_path,file,path), shell=True)
    return ckpt_path

  def download_model(self, name, step):
    # download a model from compute canada (cedar)
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    model_dir = os.path.dirname(ckpt_path)
    dest_dir = os.path.join(current_dir, model_dir)
    os.makedirs(dest_dir, exist_ok=True)
    if not os.path.exists(ckpt_path):
      call("rsync -a -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/{}/checkpoint-{} {} --progress".format(name,step,dest_dir), shell=True)
    return ckpt_path

  def generate(self, input_ids=None, injects=None, temperature=.9, batch_size=2, verbose=True, global_mask=None):
    import torch.nn.functional as F

    #print("=" * 20)
    
    injects = injects.build(self.encoder) # we only use encoder here
    injects = [copy.deepcopy(injects) for _ in range(batch_size)]
    
    gen_start_time = time.time()

    if input_ids is None:
      input_ids = torch.zeros((batch_size,1), dtype=torch.long)
    else:
      input_ids = torch.from_numpy(input_ids).to(torch.long)
    # must be 2d

    past = None
    finished = np.zeros((batch_size,), dtype=np.bool)
    use_cache = self.model.config.use_cache
    vocab_size = self.model.config.vocab_size
    
    while np.any(finished==False):

      #raw_input = input_ids
      raw_input = input_ids[:,-2048:] # don't exceed window
      if past is not None and input_ids.shape[1] >= 2048:
        past = [p[:,:,:,-2047:,:] for p in past]
      inputs = self.model.prepare_inputs_for_generation(raw_input, past, use_cache=use_cache)

      outputs = self.model(**inputs)
      if self.model._use_cache(outputs, use_cache):
        past = outputs[1]
      
      logits = outputs[0][:,-1,:]
      if temperature != 1.0:
        logits = logits / temperature
      
      last_tokens = raw_input[:,-1].detach().numpy()
      masks = torch.zeros((batch_size, vocab_size), dtype=torch.float)
      if injects is not None:
        for i,(last_token,inject) in enumerate(zip(last_tokens,injects)):
          trigger, mask = inject[0]
          if trigger[last_token]:
            if mask is not None:
              assert mask.dtype == np.bool
              masks[i][~mask] = -float("Inf") # set avoided tokens to smallest value
            if len(inject) == 1:
              finished[i] = True
            else:
              inject.pop(0)
      
      if np.any(finished==False):
        if global_mask is not None:
          logits = masks + global_mask + logits
        else:
          logits = masks + logits
        
        probs = F.softmax(logits, dim=-1)
        next_token = torch.multinomial(probs, num_samples=1).squeeze(1)
        next_token[finished] = 0 # if the sequence has ended
        input_ids = torch.cat([input_ids, next_token.unsqueeze(-1)], dim=-1)

        pretty_token = self.encoder.rep.pretty([next_token])[0]
        #if "TRACK" in pretty_token or "BAR" in pretty_token:
        #  print(pretty_token, input_ids.shape[1])
        if verbose:
          write_debug(repr(pretty_token) + " " + str(input_ids.shape[1]))
      
      #if input_ids.shape[1] >= 512:
      #  finished[:] = True

    #print(time.time() - gen_start_time)
    input_ids = input_ids.detach().numpy()

    # remove the padding at end of sequences
    output = []
    for seq in input_ids:
      idx = np.where(seq==0)[0]
      if len(idx) > 1:
        output.append( seq[:idx[1]] )
      else:
        output.append( seq )
    return output 

  def generate_bars(self, bars, midi_json, batch_size=1, temperature=.9, nbars=None):
    
    e = db.EncoderConfig()
    e.multi_fill = set(bars)
    e.do_track_shuffle = False
    e.force_valid = True
    if nbars is not None:
      e.num_bars = nbars

    FILL_START = self.encoder.rep.encode({db.TOKEN_TYPE.FILL_IN : 1})

    # prompt is only up to (and including) the first fill start token
    prompt = np.array(self.encoder.json_to_tokens(json.dumps(midi_json), e))
    prompt = prompt[:np.where(prompt==FILL_START)[0][0] + 1][None,:]

    control = Control()
    for _ in range(len(bars)):
      control.add({"FILL_IN" : 2}, None)

    return self.generate(input_ids=prompt, temperature=temperature, injects=control, batch_size=batch_size)

  def generate_tracks(self, tracks, midi_json=None, batch_size=2, temperature=1., genre_tags_raw=None, mono_poly=None, density=None, global_mask=None, nbars=None):

    # temporary helper for no fill
    #c = Control()
    #m = c.encode(self.encoder, {"FILL_IN":-1})
    #global_mask = np.zeros_like(m).astype(np.float32)
    #global_mask[m] = -float("Inf")

    # check that genre tags are valid
    genre_tags = None
    if genre_tags_raw is not None:
      genre_tags = []
      for tag in genre_tags_raw:
        if tag in GENRE_TAG_MAP:
          genre_tags.append(GENRE_TAG_MAP[tag])
        else:
          print("WARNING : UNKNOWN GENRE TAG")
          genre_tags.append(GENRE_TAG_MAP["none"])

    control = Control()

    for track_num, track in enumerate(tracks):

      # convert the track strings to integers
      if isinstance(track,str):
        if track in GENERAL_MIDI_MAP:
          track = GENERAL_MIDI_MAP[track]
        else:
          print("WARNING : UNKNOWN INSTRUMENT TYPE")
          track = GENERAL_MIDI_MAP["any"]
      assert isinstance(track,list)

      track_type = 0 # normal track
      if any([t>=128 for t in track]):
        if len(track) == 1:
          track_type = 1 # drum track
        else:
          track_type = -1 # both tracks
      elif mono_poly is not None:
        if mono_poly[track_num] == "poly":
          track_type = 2
    
      # if we are generating from scratch
      if track_num == 0 and midi_json is None:
        if genre_tags is not None:
          control.add({"PIECE_START" : 0}, {"GENRE" : genre_tags[0]})
          control.add({"GENRE" : -1}, {"GENRE" : genre_tags[1]})
          control.add({"GENRE" : -1}, {"TRACK" : track_type})
        else:
          control.add({"PIECE_START" : 0}, {"TRACK" : track_type})
      else:
        control.add({"TRACK_END" : 0}, {"TRACK" : track_type})
      
      # make instrument
      #if ANY_DRUM not in track:
      control.add({"TRACK" : -1}, {"INSTRUMENT" : list(set([t%128 for t in track]))})
      
      # control density
      if density is not None and density[track_num] is not None:
        control.add({"INSTRUMENT" : -1}, {"DENSITY_LEVEL" : density[track_num]})

      # control bar number (seems to get crazy with more than 4 bars)
      for i in range(nbars-1):
        control.add({"BAR_END" : 0}, None)
      control.add({"BAR_END" : 0}, {"TRACK_END" : 0})
      
    control.add({"TRACK_END" : 0}, None)

    #for _ in control.controls:
    #  print(_)
  
    # make the prompt from the encoder here
    # so that calling code doesn't have to know what encoder to use ...
    ec = db.EncoderConfig()
    ec.do_track_shuffle = False
    ec.force_valid = True
    ec.force_four_four = True
    ec.segment_idx = 0 # always use the first one only
    if nbars is not None:
      ec.num_bars = nbars
    if genre_tags_raw is not None:
      ec.genre_tags = genre_tags_raw # pass the strings in here

    prompt = None
    if midi_json is not None:
      prompt = self.encoder.json_to_tokens(json.dumps(midi_json),ec)
      prompt = np.array(prompt)[None,:]

    #print([(np.where(t),np.where(m)) for t,m in control.build(self.encoder)])
    return self.generate(input_ids=prompt, temperature=temperature, injects=control, batch_size=batch_size, global_mask=None)

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)

def get_events(midi_json, track_num):
  events = []
  for bar in midi_json["tracks"][track_num].get("bars",[]):
    for event_id in bar.get("events",[]):
      event = midi_json["events"][event_id]
      events.append( (event["time"], event["pitch"]) )
  return set(events)

def jaccard(a, b):
  return float(len(a.intersection(b))) / min(len(a),len(b))

def random_stem():
  num_tracks = 4
  stem = None
  while stem is None:
    try:
      path = random.choice(paths)
      encoder = db.TrackEncoder()
      e = db.EncoderConfig()
      e.num_bars = 8
      x = encoder.midi_to_json(path,e)
      x = db.select_segment(x, 6, -1)
      midi_json = json.loads(x)
      if len(midi_json.get("tracks",[])) >= num_tracks:
        # need to remove duplicate tracks !!!
        no_duplicates = True
        ntracks = len(midi_json["tracks"])
        event_lists = [get_events(midi_json,i) for i in range(ntracks)]
        for i in range(ntracks):
          for j in range(i+1,ntracks):
            if jaccard(event_lists[i], event_lists[j]) >= .5:
              no_duplicates = False
        
        if no_duplicates:
          stem = json.loads(x)
    except:
      pass
  return stem

def mix_tracks_in_json(midi_json, levels=None):
  AUDIO_LEVELS = [12,24,36,48,60,72,84,96,108,120]
  for track_num, track in enumerate(midi_json.get("tracks",[])):
    for bar in track.get("bars",[]):
      for event_index in bar.get("events",[]):
        event = midi_json["events"][event_index]
        if event["velocity"] > 0:
          audio_level = AUDIO_LEVELS[8]
          if levels is not None:
            audio_level = AUDIO_LEVELS[levels[track_num]]
          event["velocity"] = audio_level

# we should export to json not to midi.
# fix what we have already done
"""
def midi_to_json(path):
  ec = db.EncoderConfig()
  ec.do_track_shuffle = False
  ec.num_bars = 8
  ec.force_valid = True
  ec.force_four_four = True
  ec.segment_idx = 0
  encoder = db.TrackBarFillDensityEncoder()
  midi_json = json.loads(encoder.midi_to_json(path,ec))
  midi_json = json.loads(db.prune_tracks_and_bars(json.dumps(midi_json), [], 8))
  with open(os.path.splitext(path)[0] + ".json", "w") as f:
    json.dump(midi_json, f)

for i in range(10):
  midi_to_json("gen/8b_gibbs_orig_{}.mid".format(i))
  midi_to_json("gen/8b_gibbs_gen_{}.mid".format(i))
"""

def save_json(midi_json, path):
  with open(path, "w") as f:
    json.dump(midi_json, f)



p = .25
steps = 5

#model = sampler(ckpt_path="local_notebook/checkpoints/TRACK_BAR_FILL_DENSITY_ENCODER_gpt2_version3_Aug_13_13_24_False_num_bars_8_6/checkpoint-175000")

model = sampler(ckpt_path="local_notebook/checkpoints/TRACK_DENSITY_ENCODER_gpt2_version3_Aug_13_13_31_False_num_bars_8_6/checkpoint-240000")


for i in tqdm(range(20,30)):

  midi_json = random_stem()

  ec = db.EncoderConfig()
  ec.do_track_shuffle = False
  ec.num_bars = 8
  ec.force_valid = True

  

  tempo = midi_json["tempo"]
  mix_tracks_in_json(midi_json)
  #model.encoder.json_to_midi(json.dumps(midi_json), "gen/8b_gibbs_orig_{}.mid".format(i), ec)
  save_json(midi_json, "gen/8b_gibbs_{}_orig.json".format(i))

  # do a random track resample /w bar infilling?
  ntracks = len(midi_json["tracks"])
  track_nums = list(range(ntracks))
  random.shuffle(track_nums)
  for track_num in track_nums[:2]:
    tracks = [REVERSE_INSTRUMENT_TRACK[midi_json["tracks"][track_num]["instrument"] + 128*midi_json["tracks"][track_num]["isDrum"]]]
    cond_tracks = [t for t in range(len(track_nums)) if t != track_num]
    cond_json = json.loads(db.prune_tracks_and_bars(json.dumps(midi_json), cond_tracks, 8))
    tokens = model.generate_tracks(tracks, cond_json, temperature=.975, nbars=8, batch_size=1)[0]
    #bars = [(track,j) for j in range(8)]
    #tokens = model.generate_bars(bars, midi_json, temperature=.975, nbars=8)[0]
    midi_json = json.loads(model.encoder.tokens_to_json(tokens,ec))

    # re-order the tracks
    order = list(range(ntracks-1))
    order.insert(track_num, ntracks-1)
    midi_json["tracks"] = [midi_json["tracks"][o] for o in order]

  #for _ in range(steps):
  #  bars = list(zip(*np.where(np.random.rand(len(midi_json["tracks"]),8) < p)))
  #  tokens = model.generate_bars(bars, midi_json, temperature=.975, nbars=8)[0]
  #  midi_json = json.loads(model.encoder.tokens_to_json(tokens,ec))

  midi_json["tempo"] = tempo
  mix_tracks_in_json(midi_json)
  #model.encoder.json_to_midi(json.dumps(midi_json), "gen/8b_gibbs_gen_{}.mid".format(i), ec)
  save_json(midi_json, "gen/8b_gibbs_{}_gen.json".format(i))