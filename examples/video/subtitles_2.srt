1
00:00:00,000 --> 00:00:05,000
The multi-track music machine can be used to generate musical material in a variety of ways.

2
00:00:05,000 --> 00:00:09,000
To demonstrate, we will start with a drum track.

3
00:00:26,000 --> 00:00:32,000
We can generate additional tracks conditioned on the drum track, while specifying the instrument for each track.

4
00:00:32,000 --> 00:00:38,000
Here, we generate a piano track and a bass track conditioned on the drum track.

5
00:00:57,000 --> 00:01:00,000
We can also resample any subset of the bars in the piece.

6
00:01:00,000 --> 00:01:03,000
Here, we will resample the 3rd bar in each of the three tracks.

7
00:01:23,000 --> 00:01:28,000
Now we will generate a jazz guitar track conditioned on the three current tracks.

8
00:01:48,000 --> 00:01:52,000
Although the new track has potential, some of the bars could use some work.

9
00:01:52,000 --> 00:01:55,000
Lets resample the 2nd 3rd and 8th bars.

10
00:02:15,000 --> 00:02:19,000
We can specify the note-density for a generated track.

11
00:02:19,000 --> 00:02:22,000
This allows us to generate a drum track with a higher note-density.





