# fix the video jsons

# list of which ones to use for the video

# [do] click add midi to load drum loop
# LOAD 0. start with a drum loop
# [do] add a new track with Acoustic Grand Piano
# [do] add a new track with Acoustic Bass
# LOAD 1. condition the generation of two tracks on drum loop. specify that they are piano and bass
# [do] highlight the 3rd bar in all three tracks
# LOAD 2. there is something we don't quite like about bar 2 lets resample
# [do] add a new track with type Jazz Guitar
# LOAD 3. generate a new track with jazz guitar
# [do] highlight bars 2,3 and 8 in the jazz guitar track
# LOAD 7. fix bars 2,3, and 8 in the jazz guitar track
# [do] enable density on drum track and set to 6
# 8. generate a more dense drum part 

# make each json into a wav for the video

import os
import json
import copy
import dataset_builder_2 as db
from subprocess import call
from midi2audio import FluidSynth

with open("8b_video_1.json", "r") as f:
  midi_json = json.load(f)

midi_json = json.loads(db.prune_tracks(json.dumps(midi_json), [0]))
with open("8b_video_0.json", "w") as f:
  json.dump(midi_json, f)

def swap_extension(path, ext):
  return os.path.splitext(path)[0] + ext

def json_to_midi(x, path):
  encoder = db.TrackEncoder()
  ec = db.EncoderConfig()
  ec.do_track_shuffle = False
  ec.force_four_four = True
  encoder.json_to_midi(json.dumps(x),path,ec)

def json_to_wav(x, path):
  json_to_midi(x, swap_extension(path, ".mid"))
  fs = FluidSynth("../local_notebook/font.sf2")
  wav_path = swap_extension(path, ".wav")
  fs.midi_to_audio(swap_extension(path, ".mid"), wav_path)
  # normalize the audio as well
  call("ffmpeg-normalize -f {} -o {} --normalization-type ebu".format(wav_path, swap_extension(path, "_NORM.wav")), shell=True)
  return wav_path

def mix_tracks_in_json(midi_json, levels):
  AUDIO_LEVELS = [12,24,36,48,60,72,84,96,108,120]
  for track_num, track in enumerate(midi_json.get("tracks",[])):
    for bar in track.get("bars",[]):
      for event_index in bar.get("events",[]):
        event = midi_json["events"][event_index]
        if event["velocity"] > 0:
          event["velocity"] = AUDIO_LEVELS[levels[track_num]]

for i in [0,1,2,3,7,8]:
  path = "8b_video_{}.json".format(i)
  with open(path, "r") as f:
    midi_json = json.load(f)
  mix_tracks_in_json(midi_json, [8,7,9,6])
  json_to_wav(midi_json, path)


exit()

for i in range(4,9):
  path = "8b_video_{}.json".format(i)
  with open(path,"r") as f:
    midi_json = json.load(f)
  with open(path.split(".")[0] + "_BACKUP.json", "w") as f:
    json.dump(midi_json, f)
  midi_json["tracks"] = [midi_json["tracks"][i] for i in [3,0,1,2]]
  with open(path,"w") as f:
    json.dump(midi_json, f)