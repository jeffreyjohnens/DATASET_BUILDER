# switch db.prune to accept list of bars

import os
import json
import dataset_builder_2 as db

for path in os.listdir("gen"):
  path = os.path.join("gen", path)
  if path.endswith(".json"):

    print(path)
  
    encoder = db.TrackEncoder()
    e = db.EncoderConfig()
    e.num_bars = 4
    e.do_track_shuffle = False
    
    with open(path,"r") as f:
      x = json.dumps(json.load(f))

    encoder.json_to_midi(x,os.path.splitext(path)[0] + ".mid",e)