# go through all the files in demo_stems and make a json
# need to deal with duplicate tracks
import os
import json
import dataset_builder_2 as db
from subprocess import call

# rsync /Users/Jeff/MMM_VIDEO_v3.mp4 jeffe@ftp.sfu.ca:pub_html/examples/video.mp4
# ssh jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/examples/video.mp4'
def upload_file(src_path, dest_path):
  id_rsa = "/Users/Jeff/.ssh/sfuweb_rsa"
  call("rsync -e 'ssh -i {}' -a {} jeffe@ftp.sfu.ca:pub_html/{} --progress".format(id_rsa, src_path, dest_path), shell=True)

demo_json = []

track_prune_map = {
  "demo_stems/stem_16.mid" : [0,1,3,4,5],
  "demo_stems/stem_a.mid" : [0,2,3,4,5],
  "demo_stems/stem_22.mid" : [0,1,3,4],
  "demo_stems/stem_33.mid" : [0,2,3,4,5],
  "demo_stems/stem_27.mid" : [0,2,3],
  "demo_stems/stem_19.mid" : [0,1,2,3,5],

  "demo_stems_8/stem_21.mid" : [0,1,3],
  "demo_stems_8/stem_23.mid" : [0,1,2,4],
  "demo_stems_8/stem_31.mid" : [0,1,3],
  "demo_stems_8/stem_39.mid" : [0,1,3,4,5],
  "demo_stems_8/stem_60.mid" : [0,1,2,5],
}

for folder,nbars in zip(["demo_stems_8", "demo_stems"],[8,4]):
  for path in sorted(os.listdir(folder)):
    if path.endswith(".mid"):
      path = os.path.join(folder, path)
      
      encoder = db.TrackEncoder()
      ec = db.EncoderConfig()
      ec.force_valid = True
      ec.force_four_four = True
      ec.do_track_shuffle = False
      ec.num_bars = nbars
      jstr = encoder.midi_to_json(path,ec)
      tracks = track_prune_map.get(path,[])
      jstr = db.prune_tracks_and_bars(jstr, tracks, nbars)
      midi_json = json.loads(jstr)
      if not "tempo" in midi_json:
        midi_json["tempo"] = 120
      demo_json.append( midi_json )

with open("demo_stems.json", "w") as f:
  json.dump(demo_json, f)

# write this file to the examples folder
upload_file("demo_stems.json", "examples/demo_stems.json")
upload_file("demo.html", "examples/demo.html")
upload_file("dataset_builder_2-0.0.1-cp36-cp36m-linux_x86_64.whl", "examples/dataset_builder_2-0.0.1-cp36-cp36m-linux_x86_64.whl")

# ssh -i /Users/Jeff/.ssh/sfuweb_rsa jeffe@ftp.sfu.ca 'chmod -R 755 pub_html/checkpoints'