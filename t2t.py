# also train a model using this version

import json
import numpy as np

#import tensorflow.compat.v1 as tf

import tensorflow as tf
import memory_saving_gradients
# monkey patch tf.gradients to point to our custom version, with automatic checkpoint selection
tf.__dict__["gradients"] = memory_saving_gradients.gradients_speed


from tensor2tensor.data_generators import problem
from tensor2tensor.utils import registry
from tensor2tensor.layers import modalities as t2t_modalities
from tensor2tensor.models import transformer, mtf_transformer
from tensor2tensor.bin import t2t_trainer

from subprocess import call
call("rm -rf train", shell=True) # remove old checkpoints

import dataset_builder

vocab_size = 512 #44644
batch_size = 128
maxlen = 2048

def gen():
  j = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    x = np.array(j.read_batch(batch_size, maxlen, 0))
    yield {"targets" : np.copy(x)}

def gen_eval():
  j = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    x = np.array(j.read_batch(batch_size, maxlen, 2))
    yield {"targets" : x.reshape(batch_size, maxlen, 1, 1)}


# these are test versions 
def gen():
  while True:
    yield {"targets" : np.random.randint(vocab_size, size=(batch_size,maxlen))}

def gen_eval():
  while True:
    yield {"targets" : np.random.randint(vocab_size, size=(batch_size,maxlen,1,1))}


@registry.register_problem('my_problem')
class MyProblem(problem.Problem):

  # take mode into consideration ...
  def make_estimator_input_fn(self,
                              mode,
                              hparams,
                              data_dir=None,
                              force_repeat=False,
                              prevent_repeat=False,
                              dataset_kwargs=None):

    @tf.function
    def estimator_input_fn(*args, **kwargs):
      return tf.data.Dataset.from_generator( 
        gen, 
        {"targets" : tf.int64}, 
        {"targets" : tf.TensorShape([batch_size,maxlen])})

    @tf.function
    def estimator_eval_fn(*args, **kwargs):
      return tf.data.Dataset.from_generator(
        gen_eval,
        {"targets" : tf.int64},
        {"targets" : tf.TensorShape([batch_size,maxlen,1,1])})

    if mode == tf.estimator.ModeKeys.EVAL:
      return estimator_eval_fn
    return estimator_input_fn
  
  def hparams(self, defaults, model_hparams):
    del model_hparams   # unused
    defaults.modality = {'targets': t2t_modalities.ModalityType.SYMBOL}
    defaults.vocab_size = {'targets': vocab_size} # vocab size

  @property
  def splits(self):
    return None

  @property
  def min_hop_size_seconds(self):
    return 0.0

  @property
  def max_hop_size_seconds(self):
    return 0.0

  @property
  def add_eos_symbol(self):
    return False

  @property
  def stretch_factors(self):
    # Stretch by -5%, -2.5%, 0%, 2.5%, and 5%.
    return [0.95, 0.975, 1.0, 1.025, 1.05]

  @property
  def transpose_amounts(self):
    # Transpose no more than a minor third.
    return [-3, -2, -1, 0, 1, 2, 3]

  @property
  def random_crop_in_train(self):
    return True

  @property
  def split_in_eval(self):
    return True

def update_transformer_hparams_for_music(hparams):
  """Updates hparams for symbolic music modeling problems."""
  hparams.shared_embedding_and_softmax_weights = False
  hparams.symbol_modality_num_shards = 1
  hparams.label_smoothing = 0.0

  hparams.layer_prepostprocess_dropout = 0.1
  hparams.attention_dropout = 0.1
  hparams.relu_dropout = 0.1

  hparams.max_length = maxlen
  hparams.batch_size = 2048

  hparams.sampling_method = "random"
  hparams.summarize_vars = True

  # For local attention.
  hparams.add_hparam("block_length", 512)

  # For decoding.
  hparams.add_hparam("decode_output_dir", "/tmp/")

  # For multi-embedding aggregation in melody & performance autoencoder
  hparams.add_hparam("aggregation", "sum")


def update_truncate_length(hparams, length):
  hparams.max_target_seq_length = length
  hparams.min_length = length
  # If using relative attention, set max relative distance to be half
  # the max training length.
  hparams.max_relative_position = int(length / 2.0)


def update_dropout(hparams, dropout):
  """Updates dropout rate."""
  hparams.layer_prepostprocess_dropout = dropout
  hparams.attention_dropout = dropout
  hparams.relu_dropout = dropout


def update_tiny(hparams):
  hparams.hidden_size = 256
  hparams.attention_key_channels = 512
  hparams.filter_size = 2048

  hparams.batch_size = 9000
  # did not actually adjust number of warmup steps
  # hparams.warmup = 10000
  hparams.learning_rate = 0.1
  return hparams


def update_small(hparams):
  hparams.hidden_size = 384
  hparams.attention_key_channels = 512
  hparams.filter_size = 1024


def update_small_lr(hparams):
  hparams.learning_rate = 0.1
  hparams.hidden_size = 384
  hparams.attention_key_channels = 512
  hparams.filter_size = 1024


def update_medium(hparams):
  hparams.learning_rate = 0.1
  hparams.hidden_size = 512
  hparams.attention_key_channels = 512
  hparams.filter_size = 1024

@registry.register_hparams
def fuckery():
  """Hparams for LM with relative attention, tiny transformer."""
  # hparams = transformer.transformer_base()
  hparams = transformer.transformer_tiny()
  update_transformer_hparams_for_music(hparams)
  update_truncate_length(hparams, 512)
  update_dropout(hparams, 0.15)
  hparams.self_attention_type = "dot_product_relative_v2"
  # Need to specify num_hidden_layers
  hparams.attention_key_channels = 512
  hparams.num_hidden_layers = 2
  hparams.batch_size = batch_size
  return hparams

@registry.register_hparams
def normal():
  """Hparams for LM with relative attention."""
  #hparams = mtf_transformer.mtf_transformer_base()
  #hparams.mesh_shape = "batch:1;model:1"
  #hparams.transformer_type = "decoder"
  #hparams.encoder_layers = ["att","drd"]
  #hparams.decoder_layers = ["att","drd"] * 8

  hparams = transformer.transformer_base()
  update_transformer_hparams_for_music(hparams)
  update_truncate_length(hparams, maxlen)
  update_medium(hparams) #update_small(hparams)
  update_dropout(hparams, 0.20)
  hparams.self_attention_type = "dot_product_relative_v2"
  hparams.num_hidden_layers = 8
  hparams.batch_size = batch_size
  return hparams

from tensor2tensor.utils.trainer_lib import create_run_config, create_experiment
from tensor2tensor.utils.trainer_lib import create_hparams

RUN_CONFIG = create_run_config(
  model_dir="train",
  model_name="transformer",
  save_checkpoints_steps=2, #100
  log_step_count_steps=1,
  num_gpus=4, #4,
)

tensorflow_exp_fn = create_experiment(
  run_config=RUN_CONFIG,
  hparams=normal(),
  model_name="transformer", #mtf_transformer
  problem_name="my_problem",
  train_steps=100000, 
  eval_steps=1,
  min_eval_frequency=1,
  data_dir=".",
  schedule="train",
)

#for k,v in tensorflow_exp_fn.estimator.params.items():
#  print(k, v.shape)
#exit()
#print(tf.trainable_variables())
#exit()

#tensorflow_exp_fn.train_and_evaluate()
tensorflow_exp_fn.train()