import os
import glob
import json
import numpy as np
from tqdm import tqdm
from multiprocessing import Pool

import dataset_builder_2 as db

# v4 has information about polyphony
# v5 requires 3 non_empty_bars per track
# v6 includes genre information

# bar major vs track major representation

# build a generic dataset with no exclusion of certain pieces ...

def worker(args):
	path,sid,genre,tcjson = args
	tc = db.TrainConfig()
	tc.from_json(tcjson)
	if tc.opz:
		encoder = db.TeTrackDensityEncoder()
	else:
		encoder = db.TrackEncoder()
	try:
		return sid, encoder.midi_to_json_bytes(path,tc)
	except Exception as e:
		print(e)
		return None,None

if __name__ == "__main__":

	# [[ recipe ]]
	# git pull origin master
	# cd dataset_builder_2
	# source build.sh

	# salloc --time=1:0:0 --ntasks-per-node=48 --mem=0 --account=def-pasquier
	# source ~/HUGGING/bin/activate
	# python build_dataset.py --data_dir /home/jeffe/project/jeffe/lmd_full --output /home/jeffe/scratch/data_trackv7.arr --num_bars 4
	# // remove the core.* files afterwards ...

	# python3 build_dataset.py --data_dir /Users/Jeff/DATA/lmd_full --output test.arr --max_size 5000 --num_bars 16

	# python3 build_dataset.py --data_dir /Users/Jeff/DATA/lmd_full --output /Users/Jeff/DATA/lmd.arr --num_bars 4

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("--data_dir", type=str, required=True)
	parser.add_argument("--output", type=str, required=True)
	parser.add_argument("--num_bars", type=int, default=4)
	parser.add_argument("--opz", type=bool, default=False)
	parser.add_argument("--n_threads", type=int, default=8)
	parser.add_argument("--max_size", type=int, default=-1)
	args = parser.parse_args()

	import os
	os.system("taskset -p 0xffff %d" % os.getpid())

	# multi thread approach takes about 2 minutes
	pool = Pool(args.n_threads)
	output = os.path.splitext(args.output)[0]
	output += "_NUM_BARS={}_OPZ_{}.arr".format(args.num_bars,args.opz)
	jag = db.Jagged(output)
	paths = list(glob.glob(args.data_dir + "/**/*.mid", recursive=True))
	
	import random
	import time
	random.seed(int(time.time()))

	tc = db.TrainConfig()
	tc.num_bars = args.num_bars
	tc.opz = args.opz
	tc = tc.to_json()


	# load genre_data
	#with open("genre_data.json", "r") as f:
	#	genre_data = json.load(f)
	
	# split dataset 
	sids = np.random.choice([0,1,2], p=[.8,.1,.1], size=(len(paths),))
	#genres = [genre_data[os.path.basename(p)] for p in paths]
	genres = [None for p in paths]
	tcs = [tc for _ in paths]
	inputs = list(zip(paths,sids,genres,tcs))
	random.shuffle(inputs)

	if args.max_size > 0:
		inputs = inputs[:args.max_size]

	total_count = 0
	success_count = 0
	pool = Pool(8)
	progress_bar = tqdm(pool.imap_unordered(worker, inputs), total=len(paths))
	for sid,b in progress_bar:
		if b is not None and len(b):
			jag.append(b,sid)
			success_count += 1
		total_count += 1
		status_str = "{}/{}".format(success_count,total_count)
		progress_bar.set_description(status_str)
	jag.close()