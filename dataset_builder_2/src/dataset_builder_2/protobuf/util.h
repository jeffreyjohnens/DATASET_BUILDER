#pragma once

#include <google/protobuf/util/json_util.h>

#include <vector>
#include "midi.pb.h"
#include "../enum/density.h"
#include "../enum/constants.h"
#include "../enum/te.h"
#include "../enum/encoder_config.h"

template<typename T>
vector<T> arange(T start, T stop, T step = 1) {
  vector<T> values;
  for (T value = start; value < stop; value += step)
    values.push_back(value);
  return values;
}

template<typename T>
vector<T> arange(T stop) {
  return arange(0, stop, 1);
}

struct RNG {
  int operator() (int n) {
    return std::rand() / (1.0 + RAND_MAX) * n;
  }
};


// select tracks for te rep

// ========================================================================
// MAX POLYPHONY

vector<midi::Note> track_events_to_notes(midi::Piece *p, int track_num) {
  midi::Event e;
  map<int,midi::Event> onsets;
  vector<midi::Note> notes;
  for (auto bar : p->tracks(track_num).bars()) {
    for (auto event_id : bar.events()) {
      e = p->events(event_id);
      if (e.velocity() > 0) {
        onsets[e.pitch()] = e;
      }
      else {
        auto it = onsets.find(e.pitch());
        if (it != onsets.end()) {
          midi::Event onset = it->second;
          midi::Note note;
          note.set_start( onset.time() );
          note.set_qstart( onset.qtime() );
          note.set_end( e.time() );
          note.set_qend( e.qtime() );
          note.set_velocity( onset.velocity() );
          note.set_pitch( onset.pitch() );
          note.set_instrument( onset.instrument() );
          note.set_track( onset.track() );
          note.set_bar( onset.bar() );
          note.set_is_drum( onset.is_drum() );
          notes.push_back(note);
          
          onsets.erase(it); // remove note
        }
      }
    }
  }
  return notes;
}

bool notes_overlap(midi::Note *a, midi::Note *b) {
  return (a->start() >= b->start()) && (a->start() < b->end());
}

int max_polyphony(vector<midi::Note> &notes) {
  int max_poly = 0;
  vector<int> overlap_counts(notes.size(), 0);
  for (int i=0; i<notes.size(); i++) {
    for (int j=0; j<notes.size(); j++) {
      if ((i!=j) && notes_overlap(&notes[i],&notes[j])) {
        overlap_counts[i]++;
        max_poly = max(max_poly, overlap_counts[i]);
      }
    }
  }
  return max_poly;
}

void update_max_polyphony(midi::Piece *p) {
  for (int i=0; i<p->tracks_size(); i++) {
    vector<midi::Note> notes = track_events_to_notes(p, i);
    p->mutable_tracks(i)->set_max_polyphony( max_polyphony(notes) );
  }
}


// ========================================================================
// NOTE DENSITY

void update_note_density(midi::Piece *x) {

  int track_num = 0;
  int num_notes, bar_num;
  for (const auto track : x->tracks()) {

    // calculate average notes per bar
    num_notes = 0;
    int bar_num = 0;
    set<int> valid_bars;
    for (const auto bar : track.bars()) {
      for (const auto event_index : bar.events()) {
        if (x->events(event_index).velocity()) {
          valid_bars.insert(bar_num);
          num_notes++;
        }
      }
      bar_num++;
    }
    int av_notes = round((double)num_notes / valid_bars.size());

    // calculate the density bin
    int qindex = track.instrument();
    if (track.is_drum()) {
      qindex = 128;
    }
    int bin = 0;
    while (av_notes > DENSITY_QUANTILES[qindex][bin]) { 
      bin++;
    }

    // update protobuf
    x->mutable_tracks(track_num)->set_note_density_v2(bin);
    track_num++;
  }
}

// ========================================================================
// EMPTY BARS

void update_has_notes(midi::Piece *x) {
  int track_num = 0;
  for (const auto track : x->tracks()) {
    int bar_num = 0;
    for (const auto bar : track.bars()) {
      bool has_notes = false;
      for (const auto event_index : bar.events()) {
        if (x->events(event_index).velocity()>0) {
          has_notes = true;
          break;
        }
      }
      x->mutable_tracks(track_num)->mutable_bars(bar_num)->set_has_notes(has_notes);
      bar_num++;
    }
    track_num++;
  }
}

int get_num_bars(midi::Piece *x) {
  if (x->tracks_size() == 0) {
    return 0;
  }
  set<int> lengths;
  for (const auto track : x->tracks()) {
    lengths.insert( track.bars_size() );
  }
  if (lengths.size() > 1) {
    throw std::runtime_error("Each track must have the same number of bars!");
  }
  return *lengths.begin();
}



void prune_tracks_dev2(midi::Piece *x, vector<int> tracks, vector<int> bars) {

  if (x->tracks_size() == 0) {
    return;
  }

  midi::Piece tmp(*x);

  int num_bars = get_num_bars(x);
  bool remove_bars = bars.size() > 0;
  if (remove_bars) {
    x->Clear();
  }
  else {
    x->clear_tracks();
  }

  vector<int> tracks_to_keep;
  for (const auto track_num : tracks) {
    if ((track_num >= 0) && (track_num < tmp.tracks_size())) {
      tracks_to_keep.push_back(track_num);
      //cout << "KEEP TRACK : " << track_num << endl;
    }
  }

  vector<int> bars_to_keep;
  for (const auto bar_num : bars) {
    if ((bar_num >= 0) && (bar_num < num_bars)) {
      bars_to_keep.push_back(bar_num);
      //cout << "KEEP BAR : " << bar_num << endl;
    }
  }

  for (const auto track_num : tracks_to_keep) {
    const midi::Track track = tmp.tracks(track_num);
    midi::Track *t = x->add_tracks();
    t->CopyFrom( track );
    if (remove_bars) {
      t->clear_bars();
      for (const auto bar_num : bars_to_keep) {
        const midi::Bar bar = track.bars(bar_num);
        midi::Bar *b  = t->add_bars();
        b->CopyFrom( bar );
        b->clear_events();
        for (const auto event_index : bar.events()) {
          b->add_events( x->events_size() );
          midi::Event *e = x->add_events();
          e->CopyFrom( tmp.events(event_index) );
        }
      }
    }
  }

  if (x->events_size() == 0) {
    throw std::runtime_error("NO EVENTS COPIED");
  }
  //cout << "NUM_EVENTS : " << x->events_size() << endl;
}

void shuffle_tracks_dev(midi::Piece *x) {
  vector<int> tracks = arange(0,x->tracks_size(),1);
  random_shuffle(tracks.begin(), tracks.end(), RNG());
  prune_tracks_dev2(x, tracks, {});
}

// ========================================================================
// RANDOM SEGMENT SELECTION FOR TRAINING
// 
// 1. we select an index of a random segment


void update_valid_segments(midi::Piece *x, int seglen, int min_tracks, bool opz) {
  update_has_notes(x);
  x->clear_valid_segments();
  x->clear_valid_tracks();

  if (x->tracks_size() < min_tracks) { return; } // no valid tracks

  int min_non_empty_bars = round(seglen * .75);
  int num_bars = get_num_bars(x);
  
  for (int start=0; start<num_bars-seglen+1; start++) {
    
    // check that all time sigs are supported
    bool supported_ts = true;
    bool is_four_four = true;
    for (int k=0; k<seglen; k++) {
      int beat_length = x->tracks(0).bars(start+k).beat_length();
      supported_ts &= (time_sig_map.find(beat_length) != time_sig_map.end());
      is_four_four &= (beat_length == 4);
    }

    // check which tracks are valid
    midi::ValidTrack vtracks;
    set<int> used_track_types;
    for (int track_num=0; track_num<x->tracks_size(); track_num++) {
      int non_empty_bars = 0;
      for (int k=0; k<seglen; k++) {
        if (x->tracks(track_num).bars(start+k).has_notes()) {
          non_empty_bars++;
        }
      }
      if (non_empty_bars >= min_non_empty_bars) {
        vtracks.add_tracks( track_num );
        used_track_types.insert( x->tracks(track_num).type() );
      }
    }

    // check if there are enough tracks
    bool enough_tracks = vtracks.tracks_size() >= min_tracks;
    if (opz) {
      enough_tracks &= used_track_types.size() >= min_tracks;
    }

    if (enough_tracks && is_four_four) {
      midi::ValidTrack *v = x->add_valid_tracks_v2();
      v->CopyFrom(vtracks);
      x->add_valid_segments(start);
    }
  }
}

void select_random_segment(midi::Piece *x, int num_bars, int min_tracks, int max_tracks, bool opz) {
  update_valid_segments(x, num_bars, min_tracks, opz);
  
  if (x->valid_segments_size() == 0) {
    throw std::runtime_error("NO VALID SEGMENTS");
  }

  int index = rand() % x->valid_segments_size();
  int start = x->valid_segments(index);
  vector<int> valid_tracks;
  for (const auto track_num : x->valid_tracks_v2(index).tracks()) {
    valid_tracks.push_back(track_num);
  }
  random_shuffle(valid_tracks.begin(), valid_tracks.end(), RNG());
  vector<int> bars = arange(start,start+num_bars,1);
  
  if (opz) {
    // filter out duplicate OPZ tracks
    vector<int> pruned_tracks;
    vector<int> used(NUM_TRACK_TYPES,0);
    for (const auto track_num : valid_tracks) {
      int track_type = x->tracks(track_num).type();
      if ((track_type >= 0) && (track_type < NUM_TRACK_TYPES)) {
        if ((used[track_type] == 0) && (track_type <= OPZ_CHORD_TRACK)) {
          pruned_tracks.push_back( track_num );
        }
        used[track_type] = 1;
      }
    }
    valid_tracks = pruned_tracks;
  }
  else {
    // limit the tracks
    int ntracks = min((int)valid_tracks.size(), max_tracks);
    valid_tracks.resize(ntracks);
  }

  prune_tracks_dev2(x, valid_tracks, bars);
}



// other helpers for training ...

tuple<int,int> get_pitch_extents(midi::Piece *x) {
  int min_pitch = INT_MAX;
  int max_pitch = 0;
  for (const auto track : x->tracks()) {
    if (!is_drum_track(track.type())) {
      for (const auto bar : track.bars()) {
        for (const auto event_index : bar.events()) {
          int pitch = x->events(event_index).pitch();
          min_pitch = min(pitch, min_pitch);
          max_pitch = max(pitch, max_pitch);
        }
      }
    }
  }
  return make_pair(min_pitch, max_pitch);
}

set<tuple<int,int>> make_bar_mask(midi::Piece *x, float proportion) {
  int num_tracks = x->tracks_size();
  int num_bars = get_num_bars(x);
  int n_fill = rand() % (int)round(num_tracks * num_bars * proportion);
  vector<tuple<int,int>> choices;
  for (int track_num=0; track_num<num_tracks; track_num++) {
    for (int bar_num=0; bar_num<num_bars; bar_num++) {
      choices.push_back(make_pair(track_num,bar_num));
    }
  }
  set<tuple<int,int>> mask;
  random_shuffle(choices.begin(), choices.end(), RNG());
  for (int i=0; i<n_fill; i++) {
    mask.insert(choices[i]);
  }
  return mask;
}

// wrap these for acccesibility in python
// ===================================================================
// ===================================================================
// ===================================================================
// ===================================================================
// ===================================================================

midi::Piece string_to_piece(string json_string) {
  midi::Piece x;
  google::protobuf::util::JsonStringToMessage(json_string.c_str(), &x);
  return x;
}

string piece_to_string(midi::Piece x) {
  string json_string;
  google::protobuf::util::MessageToJsonString(x, &json_string);
  return json_string;
}

string select_random_segment_py(string json_string, int num_bars, int min_tracks, int max_tracks, bool opz) {
  midi::Piece x = string_to_piece(json_string);
  select_random_segment(&x, num_bars, min_tracks, max_tracks, opz);
  return piece_to_string(x);
}

// summary of a 

