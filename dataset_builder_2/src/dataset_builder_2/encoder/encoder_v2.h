#pragma once

#include "encoder_base.h"
#include "util.h"
#include "../enum/te.h"
#include "../protobuf/util.h"
#include "../protobuf/validate.h"


class TrackInterleavedEncoder : public ENCODER {
public:
  TrackInterleavedEncoder() {    
    rep = new REPRESENTATION({
      {PIECE_START, TOKEN_DOMAIN(1)},
      {BAR, TOKEN_DOMAIN(1)},
      {BAR_END, TOKEN_DOMAIN(1)},
      {INSTRUMENT, TOKEN_DOMAIN(256)},
      {NOTE_OFFSET, TOKEN_DOMAIN(128)},
      {NOTE_ONSET, TOKEN_DOMAIN(128)},
      {TIME_DELTA, TOKEN_DOMAIN(48)},
      });
    config = get_encoder_config();
  }
  ~TrackInterleavedEncoder() {
    delete rep;
    delete config;
  }
  EncoderConfig *get_encoder_config() {
    EncoderConfig *e = new EncoderConfig();
    e->te = false;
    e->force_instrument = true;
    e->min_tracks = 1; // not sure this is used anywhere
    e->resolution = 12;
    return e;
  }
  vector<int> encode(midi::Piece *p) {
    return to_interleaved_performance(p, rep, config);
  }
  void decode(vector<int> &tokens, midi::Piece *p) {
    return decode_track_dev(tokens, p, rep, config);
  }
};

class TrackEncoder : public ENCODER {
public:
  TrackEncoder() {    
    rep = new REPRESENTATION({
      {PIECE_START, TOKEN_DOMAIN(1)},
      {BAR, TOKEN_DOMAIN(1)},
      {BAR_END, TOKEN_DOMAIN(1)},
      {TRACK, TOKEN_DOMAIN({
        STANDARD_TRACK,
        STANDARD_DRUM_TRACK,   
      })},
      {TRACK_END, TOKEN_DOMAIN(1)},
      {INSTRUMENT, TOKEN_DOMAIN(128)},
      {NOTE_OFFSET, TOKEN_DOMAIN(128)},
      {NOTE_ONSET, TOKEN_DOMAIN(128)},
      {TIME_DELTA, TOKEN_DOMAIN(48)},
      });
    config = get_encoder_config();
  }
  ~TrackEncoder() {
    delete rep;
    delete config;
  }
  EncoderConfig *get_encoder_config() {
    EncoderConfig *e = new EncoderConfig();
    e->force_instrument = true;
    e->min_tracks = 1; // not sure this is used anywhere
    e->resolution = 12;
    return e;
  }
  vector<int> encode(midi::Piece *p) {
    return to_performance_w_tracks_dev(p, rep, config);
  }
  void decode(vector<int> &tokens, midi::Piece *p) {
    return decode_track_dev(tokens, p, rep, config);
  }
};