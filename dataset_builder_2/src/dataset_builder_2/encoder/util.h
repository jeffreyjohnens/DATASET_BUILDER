#pragma once

#include <google/protobuf/util/json_util.h>

#include <vector>
#include <map>
#include <tuple>
#include <array>
#include <utility>

#include "../protobuf/midi.pb.h"
#include "../enum/token_types.h"
#include "../enum/constants.h"
#include "../enum/encoder_config.h"

// development versions

vector<int> to_performance_dev(midi::Bar *bar, midi::Piece *p, REPRESENTATION *rep, int transpose, bool is_drum, EncoderConfig *ec) {
  vector<int> tokens;
  int current_step = 0;
  int current_velocity = -1;
  int current_instrument = -1;
  int N_TIME_TOKENS = rep->get_domain_size(TIME_DELTA);
  bool added_instrument = false;
  for (const auto i : bar->events()) {
    midi::Event event = p->events(i);
    if ((!is_drum) || (event.velocity()>0) || (ec->use_drum_offsets)) {
      //int qvel = velocity_maps[rep->velocity_map_name][event.velocity()];
      int qvel = event.velocity() > 0;

      if (event.time() > current_step) {
        while (event.time() > current_step + N_TIME_TOKENS) {
          tokens.push_back( rep->encode(TIME_DELTA, N_TIME_TOKENS-1) );
          current_step += N_TIME_TOKENS;
        }
        if (event.time() > current_step) {
          tokens.push_back( rep->encode(
            TIME_DELTA, event.time()-current_step-1) );
        }
        current_step = event.time();
      }
      else if (event.time() < current_step) {
        throw std::runtime_error("Events are not sorted!");
      }
      // if the rep contains velocity levels
      if (ec->use_velocity_levels) {
        if ((qvel > 0) && (qvel != current_velocity)) {
          tokens.push_back( rep->encode(VELOCITY_LEVEL, qvel) );
          current_velocity = qvel;
        }
        qvel = min(1,qvel); // flatten down to binary for note
      }
      if (qvel==0) {
        tokens.push_back( rep->encode(NOTE_OFFSET, event.pitch() + transpose) );
      }
      else {
        tokens.push_back( rep->encode(NOTE_ONSET, event.pitch() + transpose) );
      }
    }
  }
  return tokens;
}

vector<int> to_interleaved_performance_inner(vector<midi::Event> &events, REPRESENTATION *rep, EncoderConfig *ec) {
  vector<int> tokens;
  int current_step = 0;
  int current_velocity = -1;
  int current_instrument = -1;
  int N_TIME_TOKENS = rep->get_domain_size(TIME_DELTA);
  bool added_instrument = false;
  for (const auto event : events) {
    bool is_drum = is_drum_track(event.track_type());
    int current_transpose = ec->transpose * (!is_drum);
    if ((!is_drum) || (event.velocity()>0) || (ec->use_drum_offsets)) {
      int qvel = event.velocity() > 0;
      if (event.time() > current_step) {
        while (event.time() > current_step + N_TIME_TOKENS) {
          tokens.push_back( rep->encode(TIME_DELTA, N_TIME_TOKENS-1) );
          current_step += N_TIME_TOKENS;
        }
        if (event.time() > current_step) {
          tokens.push_back( rep->encode(
            TIME_DELTA, event.time()-current_step-1) );
        }
        current_step = event.time();
      }
      else if (event.time() < current_step) {
        throw std::runtime_error("Events are not sorted!");
      }
      if (event.instrument() != current_instrument) {
        tokens.push_back( rep->encode(INSTRUMENT, event.instrument()) );
        current_instrument = event.instrument();
      }
      if (ec->use_velocity_levels) {
        if ((qvel > 0) && (qvel != current_velocity)) {
          tokens.push_back( rep->encode(VELOCITY_LEVEL, qvel) );
          current_velocity = qvel;
        }
        qvel = min(1,qvel); // flatten down to binary for note
      }
      if (qvel==0) {
        tokens.push_back( 
          rep->encode(NOTE_OFFSET, event.pitch() + current_transpose) );
      }
      else {
        tokens.push_back( 
          rep->encode(NOTE_ONSET, event.pitch() + current_transpose) );
      }
    }
  }
  return tokens;
}


bool sort_events(const midi::Event a, const midi::Event b) { 
  if (a.time() != b.time()) {
    return a.time() < b.time();
  }
  if (min(a.velocity(),1) != min(b.velocity(),1)) {
    return min(a.velocity(),1) < min(b.velocity(),1);
  }
  if (a.instrument() != b.instrument()) {
    return a.instrument() < b.instrument();
  }
  return a.pitch() < b.pitch();
}

vector<int> to_interleaved_performance(midi::Piece *p, REPRESENTATION *rep, EncoderConfig *e) {

  vector<int> tokens;

  tokens.push_back( rep->encode(PIECE_START, 0) );

  int num_bars = get_num_bars(p);
  for (int bar_num=0; bar_num<num_bars; bar_num++) {
    vector<midi::Event> events;
    for (int track_num=0; track_num<p->tracks_size(); track_num++) {
      int track_type = p->tracks(track_num).type();
      bool is_drum = is_drum_track(track_type); 
      for (const auto index : p->tracks(track_num).bars(bar_num).events()) {
        midi::Event e;
        e.CopyFrom( p->events(index) );
        e.set_instrument( p->tracks(track_num).instrument() + 128*is_drum );
        e.set_track_type( track_type );
        events.push_back( e );
      }
    }

    // sort the events by time, onset/offset, track_type, instrument, pitch
    sort(events.begin(), events.end(), sort_events);

    // make bar
    tokens.push_back( rep->encode(BAR, 0) );
    vector<int> bar_tokens = to_interleaved_performance_inner(events, rep, e);
    tokens.insert(tokens.end(), bar_tokens.begin(), bar_tokens.end());
    tokens.push_back( rep->encode(BAR_END, 0) );
  }
  return tokens;
}

vector<int> to_performance_w_tracks_dev(midi::Piece *p, REPRESENTATION *rep, EncoderConfig *e) {

  vector<int> tokens;
  int cur_transpose;
  tokens.push_back( rep->encode(PIECE_START, 0) );


  for (int track_num=0; track_num<p->tracks_size(); track_num++) {

    midi::Track track = p->tracks(track_num);

    bool is_drum = is_drum_track( track.type() );
    //cout << "TRACK : " << track_num << " " << is_drum << " <-- " << track.type() << endl;
    cur_transpose = e->transpose;
    if (is_drum) {
      cur_transpose = 0;
    }

    tokens.push_back( rep->encode(TRACK, track.type()) );
    tokens.push_back( rep->encode(INSTRUMENT,track.instrument()) );
    
    if (e->mark_density) {
      tokens.push_back( rep->encode(DENSITY_LEVEL, track.note_density_v2()) );
    }

    for (int bar_num=0; bar_num<track.bars_size(); bar_num++) {
      midi::Bar bar = track.bars(bar_num);
      tokens.push_back( rep->encode(BAR, 0) );
      if (e->mark_time_sigs) {
        // assume that beat_length is in the map
        int ts = time_sig_map[round(bar.beat_length())];
        tokens.push_back( rep->encode(TIME_SIGNATURE, ts) );
      }
      if ((e->do_multi_fill) && (e->multi_fill.find(make_pair(track_num,bar_num)) != e->multi_fill.end())) {
        tokens.push_back( rep->encode(FILL_IN, 0) );
      }
      else {
        vector<int> bar_tokens = to_performance_dev(
          &bar, p, rep, cur_transpose, is_drum, e);
        tokens.insert(tokens.end(), bar_tokens.begin(), bar_tokens.end());
      }
      tokens.push_back( rep->encode(BAR_END, 0) );
    }
    tokens.push_back( rep->encode(TRACK_END, 0) );
  }

  if (e->do_multi_fill) {
    for (const auto track_bar : e->multi_fill) {
      int fill_track = get<0>(track_bar);
      int fill_bar = get<1>(track_bar);
      bool is_drum = is_drum_track( p->tracks(fill_track).type() );

      cur_transpose = e->transpose;
      if (is_drum) {
        cur_transpose = 0;
      }
      tokens.push_back( rep->encode(FILL_IN, 1) ); // begin fill-in
      midi::Bar bar = p->tracks(fill_track).bars(fill_bar);
      vector<int> bar_tokens = to_performance_dev(
        &bar, p, rep, cur_transpose, is_drum, e);
      tokens.insert(tokens.end(), bar_tokens.begin(), bar_tokens.end());
      tokens.push_back( rep->encode(FILL_IN, 2) ); // end fill-in
    }
  }

  return tokens;
}

// fix the deconversion
void decode_track_dev(vector<int> &tokens, midi::Piece *p, REPRESENTATION *rep, EncoderConfig *ec) {
  p->set_tempo(ec->default_tempo);
  p->set_resolution(ec->resolution);

  midi::Event *e = NULL;
  midi::Track *t = NULL;
  midi::Bar *b = NULL;
  int current_time, current_instrument, bar_start_time, current_track, current_velocity, beat_length;
  int track_count = 0;
  for (const auto token : tokens) {
    if (rep->is_token_type(token, TRACK)) {
      current_time = 0; // restart the time
      current_instrument = 0; // reset instrument
      current_track = SAFE_TRACK_MAP[track_count];
      t = p->add_tracks();
      if (is_drum_track(rep->decode(token))) {
        current_track = 9; // its a drum channel
        t->set_is_drum(true); // by default its false
      }
      else {
        t->set_is_drum(false);
      }
    }
    else if (rep->is_token_type(token, TRACK_END)) {
      track_count++;
    }
    else if (rep->is_token_type(token, BAR)) {
      beat_length = 4; // default value optionally overidden with TIME_SIGNATURE
      bar_start_time = current_time;
      b = t->add_bars();
      b->set_is_four_four(true);
      b->set_has_notes(false); // no notes yet
      b->set_time( bar_start_time );
    }
    else if (rep->is_token_type(token, TIME_SIGNATURE)) {
      beat_length = rev_time_sig_map[rep->decode(token)];
      
    }
    else if (rep->is_token_type(token, BAR_END)) {
      current_time += (ec->resolution * beat_length) - (current_time - bar_start_time);
    }
    else if (rep->is_token_type(token, TIME_DELTA)) {
      current_time += (rep->decode(token) + 1);
    }
    else if (rep->is_token_type(token, INSTRUMENT)) {
      current_instrument = rep->decode(token);
      t->set_instrument( current_instrument );
    }
    else if (rep->is_token_type(token, VELOCITY_LEVEL)) {
      current_velocity = rep->decode(token);
    }
    else if (rep->is_token_type(token, NOTE_ONSET) || rep->is_token_type(token, NOTE_OFFSET)) {
      int current_note_index = p->events_size();
      e = p->add_events();
      e->set_pitch( rep->decode(token) );
      e->set_velocity( 100 );
      if (rep->is_token_type(token, NOTE_OFFSET)) {
        e->set_velocity( 0 );
      }
      e->set_time( current_time );
      e->set_qtime( current_time - bar_start_time );
      e->set_instrument( current_instrument );
      e->set_track( current_track );
      e->set_is_drum( current_track == 9 );
      b->add_events( current_note_index );
      b->set_has_notes( true );
    }

  }
  p->add_valid_segments(0);
  p->add_valid_tracks((1<<p->tracks_size())-1);
  
  // update the meta-data
  //update_pitch_limits(p);
  //update_note_density(p);
  //update_polyphony(p);
}



// ================================================================
// ================================================================
// ================================================================
// ENCODING HELPERS

/*

// this now support no drum offsets
vector<int> to_performance(midi::Bar *bar, midi::Piece *p, REPRESENTATION *rep, int transpose, EncoderConfig *ec) {
  vector<int> tokens;
  int current_step = 0;
  int current_velocity = -1;
  int current_instrument = -1;
  int N_TIME_TOKENS = rep->get_domain(TIME_DELTA);
  bool added_instrument = false;
  for (const auto i : bar->events()) {
    midi::Event event = p->events(i);
    if ((!bar->is_drum()) || (event.velocity()>0) || (ec->use_drum_offsets)) {
      int qvel = velocity_maps[rep->velocity_map_name][event.velocity()];

      if (event.time() > current_step) {
        while (event.time() > current_step + N_TIME_TOKENS) {
          tokens.push_back( rep->encode({{TIME_DELTA,N_TIME_TOKENS-1}}) );
          current_step += N_TIME_TOKENS;
        }
        if (event.time() > current_step) {
          tokens.push_back( rep->encode(
            {{TIME_DELTA,event.time()-current_step-1}}) );
        }
        current_step = event.time();
      }
      else if (event.time() < current_step) {
        throw std::runtime_error("Events are not sorted!");
      }
      // if the rep contains velocity levels
      if (ec->use_velocity_levels) {
        if ((qvel > 0) && (qvel != current_velocity)) {
          tokens.push_back( rep->encode({{VELOCITY_LEVEL,qvel}}) );
          current_velocity = qvel;
        }
        qvel = min(1,qvel); // flatten down to binary for note
      }
      tokens.push_back( rep->encode(
        {{PITCH,event.pitch() + transpose},{VELOCITY,qvel}}) );
    }
  }
  return tokens;
}

vector<int> to_performance_legacy(midi::Bar *bar, midi::Piece *p, REPRESENTATION *rep, int transpose, EncoderConfig *ec) {
  vector<int> tokens;
  int current_step = 0;
  int current_velocity = -1;
  int current_instrument = -1;
  int N_TIME_TOKENS = rep->get_domain(TIME_DELTA);
  bool added_instrument = false;
  for (const auto i : bar->events()) {
    midi::Event event = p->events(i);
    int qvel = velocity_maps[rep->velocity_map_name][event.velocity()];

    if (event.time() > current_step) {
      while (event.time() > current_step + N_TIME_TOKENS) {
        tokens.push_back( rep->encode({{TIME_DELTA,N_TIME_TOKENS-1}}) );
        current_step += N_TIME_TOKENS;
      }
      if (event.time() > current_step) {
        tokens.push_back( rep->encode(
          {{TIME_DELTA,event.time()-current_step-1}}) );
      }
      current_step = event.time();
    }
    else if (event.time() < current_step) {
      cout << "ERROR : events are not sorted." << endl;
      throw(2);
    }
    // if the rep contains velocity levels
    if (ec->use_velocity_levels) {
      if ((qvel > 0) && (qvel != current_velocity)) {
        tokens.push_back( rep->encode({{VELOCITY_LEVEL,qvel}}) );
        current_velocity = qvel;
      }
      qvel = min(1,qvel); // flatten down to binary for note
    }
    tokens.push_back( rep->encode(
      {{PITCH,event.pitch() + transpose},{VELOCITY,qvel}}) ); 
  }
  return tokens;
}

// need to make sure that the right source of randomness will be used
struct RNG {
    int operator() (int n) {
        return std::rand() / (1.0 + RAND_MAX) * n;
    }
};

int select_section(midi::Piece *p, vector<int> &vtracks, EncoderConfig *e) {
  // select the section and the tracks

  if (e->seed >= 0) {
    srand(e->seed);
  }

  if (p->valid_segments_size() == 0) {
    cout << "WARNING : no valid segments" << endl;
    return -1; // return empty sequence
  }

  int index = e->segment_idx;
  if (index == -1) {
    index = rand();
  }
  index = index % p->valid_segments_size();
  
  for (int i=0; i<32; i++) {
    if (p->valid_tracks(index) & (1<<i)) {
      vtracks.push_back(i);
    }
  }

  if (e->do_track_shuffle) {
    random_shuffle(vtracks.begin(), vtracks.end(), RNG());
  }

  // for te we need to just go through and pick the first for each track type
  if (e->te) {
    int used[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    vector<int> pruned_vtracks;
    for (int i=0; i<vtracks.size(); i++) {
      int tt = p->tracks(vtracks[i]).te_track();
      if ((used[tt] == 0) && (tt < 8)) {
        pruned_vtracks.push_back( vtracks[i] );
      }
      if ((used[tt] >= 0) && (used[tt] < 16)) {
        used[tt] = 1;
      }
    }
    vtracks = pruned_vtracks;
  }
  else {
    int ntracks = min((int)vtracks.size(), e->max_tracks); // limit tracks
    cout << ntracks << endl;
    vtracks.resize(ntracks); // remove extra tracks
  }

  return index;
}

int get_track_type(midi::Piece *p, EncoderConfig *e, int i) {
  if (p->tracks(i).is_drum()) { return 1; }
  else if ((e->mark_polyphony) && (p->tracks(i).av_polyphony() >= 1.1)) { return 2; }
  return 0;
}


vector<int> to_performance_w_tracks(midi::Piece *p, REPRESENTATION *rep, EncoderConfig *e) {

  // PIECE_START
  // TRACK
  // BAR
  // <NOTES>
  // BAR_END
  // TRACK_END
  // for fill in just replace notes will FILL TOKEN

  // this double seed is an issue
  //srand(time(NULL));

  vector<int> tokens;
  vector<int> vtracks;
  int segment_idx = select_section(p, vtracks, e);
  if (segment_idx == -1) return tokens;
  if (vtracks.size() <= 0) return tokens;
  //cout << vtracks[0] << endl;
  int start = p->valid_segments(segment_idx);

  int fill_track = e->fill_track;
  int fill_bar = e->fill_bar;
  if (fill_track == -1) fill_track = rand() % vtracks.size();
  if (fill_bar == -1) fill_bar = rand() % e->num_bars;


  // if multi fill bars not selected ... select some
  if ((e->do_multi_fill) && (e->multi_fill.size() == 0)) {
    if (e->fill_percentage > 0) {
      // pick a random percentange up to fill percentage and fill those
      int n_fill = rand() % (int)round(vtracks.size() * e->num_bars * e->fill_percentage);
      vector<tuple<int,int>> choices;
      for (int i=0; i<vtracks.size(); i++) {
        for (int j=0; j<e->num_bars; j++) {
          choices.push_back(make_pair(i,j));
        }
      }
      random_shuffle(choices.begin(), choices.end(), RNG());
      for (int i=0; i<n_fill; i++) {
        e->multi_fill.insert(choices[i]);
      }
    }
    else {
      int track_num = rand() % vtracks.size();
      for (const auto bar_num : TRACK_MASKS[(rand() % 15) + 1]) {
        e->multi_fill.insert(make_pair(track_num, bar_num));
      }
    }
  }
  
  int cur_transpose;
  tokens.push_back( rep->encode({{PIECE_START,0}}) );


  // GENRE HEADER
  // data must be formated to always have two fields
  if (e->genre_header) {
    // must pass in two or fail
    if (e->genre_tags.size()) {
      for (const auto tag : e->genre_tags) {
        tokens.push_back( rep->encode({{GENRE,genre_maps["msd_cd2"][tag]}}) );
      }
    }
    else {
      for (const auto tag : p->msd_cd1()) {
        tokens.push_back( rep->encode({{GENRE,genre_maps["msd_cd2"][tag]}}) );
      }
    }
  }

  // INSTRUMENT HEADER
  if (e->instrument_header) {
    tokens.push_back( rep->encode({{HEADER,0}}) );
    for (int i=0; i<(int)vtracks.size(); i++) {
      int track = get_track_type(p,e,vtracks[i]);
      tokens.push_back( rep->encode({{TRACK,track}}) );
      int inst = p->tracks(vtracks[i]).instrument();
      tokens.push_back( rep->encode({{INSTRUMENT,inst}}) );
    }
    tokens.push_back( rep->encode({{HEADER,1}}) );
  }


  for (int i=0; i<(int)vtracks.size(); i++) {
    int track_type = 0;
    cur_transpose = e->transpose;

    midi::Track track = p->tracks(vtracks[i]);

    tokens.push_back( rep->encode({{TRACK,track.type()}}) );
    // add instrument at start of track ...
    if (e->force_instrument) {
      int inst = track.instrument();
      tokens.push_back( rep->encode({{INSTRUMENT,inst}}) );
    }
    if (e->mark_density) {
      int density_level = track.note_density_v2();
      tokens.push_back( rep->encode({{DENSITY_LEVEL,density_level}}) );
    }

    for (int j=0; j<e->num_bars; j++) {
      tokens.push_back( rep->encode({{BAR,0}}) );
      if (e->mark_time_sigs) {
        // assume that beat_length is in the map
        int ts = time_sig_map[round(track.bars(start+j).beat_length())];
        tokens.push_back( rep->encode({{TIME_SIGNATURE,ts}}) );
      }
      if ((e->do_fill) && (fill_track==i) && (fill_bar==j)) {
        tokens.push_back( rep->encode({{FILL_IN,0}}) );
      }
      else if ((e->do_multi_fill) && (e->multi_fill.find(make_pair(i,j)) != e->multi_fill.end())) {
        tokens.push_back( rep->encode({{FILL_IN,0}}) );
      }
      else {
        midi::Bar b = track.bars(start+j);
        vector<int> bar = to_performance(
          &b, p, rep, cur_transpose, e);
        tokens.insert(tokens.end(), bar.begin(), bar.end());
      }
      tokens.push_back( rep->encode({{BAR_END,0}}) );
    }
    tokens.push_back( rep->encode({{TRACK_END,0}}) );
  }

  // add the fill in bar at the end
  if (e->do_fill) {
    cur_transpose = e->transpose;
    if (p->tracks(vtracks[fill_track]).is_drum()) {
      cur_transpose = 0;
    }
    tokens.push_back( rep->encode({{FILL_IN,1}}) ); // begin fill-in
    midi::Bar b = p->tracks(vtracks[fill_track]).bars(start+fill_bar);
    vector<int> bar = to_performance(
      &b, p, rep, cur_transpose, e);
    tokens.insert(tokens.end(), bar.begin(), bar.end());
    tokens.push_back( rep->encode({{FILL_IN,2}}) ); // end fill-in
  }

  // do multiple fills at the end
  if (e->do_multi_fill) {
    for (const auto track_bar : e->multi_fill) {
      int fill_track = get<0>(track_bar);
      int fill_bar = get<1>(track_bar);

      cur_transpose = e->transpose;
      if (p->tracks(vtracks[fill_track]).is_drum()) {
        cur_transpose = 0;
      }
      tokens.push_back( rep->encode({{FILL_IN,1}}) ); // begin fill-in
      midi::Bar b = p->tracks(vtracks[fill_track]).bars(start+fill_bar);
      vector<int> bar = to_performance(
        &b, p, rep, cur_transpose, e);
      tokens.insert(tokens.end(), bar.begin(), bar.end());
      tokens.push_back( rep->encode({{FILL_IN,2}}) ); // end fill-in
    }
  }

  return tokens;
}



// ================================================================
// ================================================================
// ================================================================
// DECODING HELPERS

// pass in encoder config so that tempo can be set
void decode_track(vector<int> &tokens, midi::Piece *p, REPRESENTATION *rep, EncoderConfig *ec) {
  p->set_tempo(ec->default_tempo);
  p->set_resolution(ec->resolution);

  midi::Event *e = NULL;
  midi::Track *t = NULL;
  midi::Bar *b = NULL;
  int current_time, current_instrument, bar_start_time, current_track, current_velocity, beat_length;
  int track_count = 0;
  for (const auto token : tokens) {
    if (rep->is_token_type(token, TRACK)) {
      current_time = 0; // restart the time
      current_instrument = 0; // reset instrument
      current_track = SAFE_TRACK_MAP[track_count];
      t = p->add_tracks();
      if (rep->decode(token,TRACK)==1) {
        current_track = 9; // its a drum channel
        t->set_is_drum(true); // by default its false
      }
      else {
        t->set_is_drum(false);
      }
    }
    else if (rep->is_token_type(token, TRACK_END)) {
      track_count++;
    }
    else if (rep->is_token_type(token, BAR)) {
      beat_length = 4; // default value optionally overidden with TIME_SIGNATURE
      bar_start_time = current_time;
      b = t->add_bars();
      b->set_is_four_four(true);
      b->set_has_notes(false); // no notes yet
      b->set_time( bar_start_time );
    }
    else if (rep->is_token_type(token, TIME_SIGNATURE)) {
      beat_length = rev_time_sig_map[rep->decode(token, TIME_SIGNATURE)];
      
    }
    else if (rep->is_token_type(token, BAR_END)) {
      current_time += (ec->resolution * beat_length) - (current_time - bar_start_time);
    }
    else if (rep->is_token_type(token, TIME_DELTA)) {
      current_time += (rep->decode(token, TIME_DELTA) + 1);
    }
    else if (rep->is_token_type(token, INSTRUMENT)) {
      current_instrument = rep->decode(token, INSTRUMENT);
      t->set_instrument( current_instrument );
    }
    else if (rep->is_token_type(token, VELOCITY_LEVEL)) {
      current_velocity = rep->decode(token, VELOCITY_LEVEL);
    }
    else if (rep->is_token_type(token, PITCH)) {
      int current_note_index = p->events_size();
      e = p->add_events();
      e->set_pitch( rep->decode(token, PITCH) );
      if ((!ec->use_velocity_levels) || (rep->decode(token, VELOCITY)==0)) {
        e->set_velocity( rep->decode(token, VELOCITY) );
      }
      else {
        e->set_velocity( current_velocity );
      }
      e->set_time( current_time );
      e->set_qtime( current_time - bar_start_time );
      e->set_instrument( current_instrument );
      e->set_track( current_track );
      e->set_is_drum( current_track == 9 );
      b->add_events(current_note_index);
      b->set_has_notes(true);
    }
  }
  p->add_valid_segments(0);
  p->add_valid_tracks((1<<p->tracks_size())-1);
  
  // update the meta-data
  update_pitch_limits(p);
  update_note_density(p);
  update_polyphony(p);
}

*/