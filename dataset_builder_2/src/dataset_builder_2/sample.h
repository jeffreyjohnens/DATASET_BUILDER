#include <torch/script.h> // One-stop header.
#include <torch/nn/functional/activation.h>

#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <set>

using namespace std;

#include "encoder/encoder_all.h"
#include "enum/gm.h"
#include "enum/model_type.h"

#include "protobuf/validate.h"
#include "protobuf/util.h"

static const int NUM_LAYERS = 6;

class Control {
public:
  Control(ENCODER *e) {
    encoder = e;
    int vocab_size = encoder->rep->max_token();
    null_trigger = std::vector<int>(vocab_size,0);
    null_mask = std::vector<int>(vocab_size,1);
  }
  std::vector<int> encode(TOKEN_TYPE tt, vector<int> values) {
    if (tt == NONE) {
      return null_mask;
    }
    return encoder->rep->encode_to_one_hot(values);
  }
  void add(TOKEN_TYPE trigger_tt, vector<int> trigger_values, TOKEN_TYPE mask_tt, vector<int> mask_values) {
    controls.push_back( std::make_pair(
      encode(trigger_tt, trigger_values), encode(mask_tt, mask_values)) );
  }
  void start(int n) {
    pos = std::vector<int>(n,0);
  }
  void show_mask(vector<int> mask) {
    for (const auto v : mask) {
      if (v==0) { cout << "-"; }
      else { cout << "x"; }
    }
    cout << endl;
  }
  void show() {
    for (const auto control : controls) {
      cout << "TRIGGER : ";
      show_mask(get<0>(control));
      cout << "MASK : ";
      show_mask(get<1>(control));
    }
  }
  bool is_finished(int index) {
    return pos[index] >= controls.size();
  }
  bool all_finished() {
    for (int i=0; i<pos.size(); i++) {
      if (!is_finished(i)) {
        return false;
      }
    }
    return true;
  }
  bool check_trigger(int index, int value) {
    if (is_finished(index)) {
      return false; // no more triggers
    }
    return (get<0>(controls[pos[index]])[value] == 1);
  }
  std::vector<int> get_mask(int index) {
    return get<1>(controls[pos[index]]);
  }
  void increment(int index) {
    pos[index]++;
  }
  std::vector<std::tuple<std::vector<int>,std::vector<int>>> controls;
  ENCODER *encoder;
  vector<int> pos;
  vector<int> null_trigger; // all zeros
  vector<int> null_mask; // all ones
};

class Sampler {
public:
  Sampler(std::string ckpt_path, ENCODER_TYPE e) {
    // ckpt_path must be absolute path
    load_model(ckpt_path);
    encoder = getEncoder(e);
    //TODO : check that the encoder and model have the same dimensionality
    //for (const auto v : model.attributes()) {
    //  std::cout << v << std::endl;
    //}
  }
  void load_model(std::string &ckpt_path) {
    try {
      model = torch::jit::load(ckpt_path);
    }
    catch (const c10::Error& e) {
      std::cerr << "error loading the model\n";
      return;
    }
  }

  void sample(std::vector<int> &prompt, Control *ctrl, std::vector<midi::Piece> &output, float temperature, int batch_size=1) {

    for (const auto token : prompt) {
      cout << token << " ";
    }
    cout << endl;

    // allow for controls to influence the thing
    vector<vector<int>> seqs;
    inputs.clear(); // empty previous inputs

    // create the inputs by tileing prompt
    auto opts = torch::TensorOptions().dtype(torch::kInt64);
    torch::Tensor x = torch::zeros({batch_size, (int)prompt.size()}, opts);
    for (int k=0; k<batch_size; k++) {
      for (int i=0; i<prompt.size(); i++) {
        x[k][i] = prompt[i];
      }
    }
    inputs.push_back( x );

    // create empty state
    // TODO :: infer the rest of the state dimensions from the model
    std::vector<torch::jit::IValue> state;
    for (int i=0; i<NUM_LAYERS; i++) {
      state.push_back(torch::zeros({2, batch_size, 8, 0, 64}));
    }
    inputs.push_back( torch::ivalue::Tuple::create(state) );

    // create empty sequnces
    for (int k=0; k<batch_size; k++) {
      seqs.push_back( prompt );
    }

    ctrl->start(batch_size); // initialize the control 
    while (!ctrl->all_finished()) {
      sample_inner(ctrl, seqs, temperature);
    }

    // convert back to piece
    EncoderConfig ec;
    encoder->tokens_to_json_array(seqs, output);
  }

  void sample_inner(Control *ctrl, vector<vector<int>> &seqs, float temperature) {
    auto outputs = model.forward(inputs).toTuple();
    auto logits = outputs->elements()[0].toTensor().index({torch::indexing::Slice(),-1,torch::indexing::Slice()});
    auto past_key_values = outputs->elements()[1];

    // override values in next_tokens if necessary
    // set avoided values to a very small value 

    for (int i=0; i<seqs.size(); i++) {
      if ((seqs[i].size() > 0) && (ctrl->check_trigger(i, seqs[i].back()))) {
        vector<int> mask = ctrl->get_mask(i);
        for (int j=0; j<mask.size(); j++) {
          if (mask[j] == 0) {
            // set this to a very small possibility
            logits[i][j] = -1 * std::numeric_limits<float>::max();
          }
        }
        ctrl->increment(i);
        //cout << "EXERT CONTROL ..." << endl;
      }
    }

    namespace F = torch::nn::functional;
    auto probs = (logits / temperature).softmax(1);
    auto next_tokens = probs.multinomial(1);

    inputs.clear();
    inputs.push_back( next_tokens );
    inputs.push_back( past_key_values );

    // add next token to the sequences
    for (int i=0; i<seqs.size(); i++) {
      seqs[i].push_back( next_tokens[i][0].item<int64_t>() );
    }

  }

  // api will accept midi::Piece and return midi::Piece
  void generate_bars(std::vector<std::tuple<int,int>> &bars, midi::Piece *p, vector<midi::Piece> &seqs, int nbars, float temperature) {

    std::vector<int> prompt;
    if (p) {
      EncoderConfig ec;
      std::set<std::tuple<int,int>> barset;
      std::copy(bars.begin(), bars.end(), std::inserter(barset, barset.end()));
      ec.multi_fill = barset;
      ec.do_track_shuffle = false;
      ec.force_valid = true;
      ec.num_bars = nbars;
      prompt = encoder->encode(p);

      // remove everything after fill_start token
      int fill_start = encoder->rep->encode(FILL_IN,1);
      for (int i=0; i<prompt.size(); i++) {
        if (prompt[i] == fill_start) {
          prompt.resize(i);
          break;
        }
      }
    }
    else {
      prompt.push_back( encoder->rep->encode(PIECE_START,0) );
    }

    Control control(encoder);
    for (int i=0; i<bars.size(); i++) {
      control.add(FILL_IN, {2}, NONE, {});
    }

    return sample(prompt, &control, seqs, temperature);
  }

  // for python only ...
  /*
  std::vector<std::vector<int>> generate_tracks_py(std::vector<tuple<int,string>> &tracks, string &json_string, int num_bars, int density, float temperature) {
    if (json_string.size() == 0) {
      return generate_tracks(tracks, NULL, num_bars, density, temperature);
    }
    midi::Piece p;
    google::protobuf::util::JsonStringToMessage(json_string.c_str(), &p);
    generate_tracks(tracks, &p, num_bars, density, temperature);
  }
  */

  void generate_tracks(std::vector<tuple<int,string>> &tracks, midi::Piece *p, vector<midi::Piece> &seqs, int num_bars, float temperature) {
    
    int density = 5;
    Control control(encoder);

    int track_num = 0;
    for (const auto track : tracks) {
      vector<int> insts = GM[get<1>(track)];
      int track_type = get<0>(track);
      
      for (int i=0; i<insts.size(); i++) {
        insts[i] %= 128;
      }
      if ((track_num == 0) && (!p)) {
        control.add(PIECE_START, {0}, TRACK, {track_type});
      }
      else {
        control.add(TRACK_END, {0}, TRACK, {track_type});
      }
      control.add(TRACK, {-1}, INSTRUMENT, insts);

      // density control
      if (density >= 0) {
        control.add(INSTRUMENT, {-1}, DENSITY_LEVEL, {density});
      }
      
      track_num++;
    }
    control.add(TRACK_END, {0}, NONE, {});
    //control.show();

    cout << p->tracks_size() << endl;

    vector<int> prompt;
    if (p) {
      EncoderConfig ec;
      ec.do_track_shuffle = false;
      ec.force_valid = false; //true;
      ec.segment_idx = 0;
      ec.num_bars = num_bars;
      prompt = encoder->encode(p);
    }
    else {
      prompt.push_back( encoder->rep->encode(PIECE_START,0) );
    }

    sample(prompt, &control, seqs, temperature);
  }

  torch::jit::script::Module model;
  std::vector<torch::jit::IValue> inputs;
  ENCODER *encoder;
};

class Generator {
public:
  Generator(map<tuple<int,MODEL_TYPE>,string> &ckpt_paths) {
    for (const auto kv : ckpt_paths) {
      samplers[kv.first] = new Sampler(kv.second, TE_TRACK_DENSITY_ENCODER);
    }
  }

  vector<midi::Piece> generate(midi::Status *status, midi::Piece *piece, float temp) {
    validate_status(status, piece);
    update_has_notes(piece);
    

    vector<midi::Piece> output(1);

    int num_bars = status->tracks(0).selected_bars_size();
    tuple<int,MODEL_TYPE> model_key;

    // for bar-fill each tradk_id must be in range
    // for track each track_id of condition must be in range
    // there should not be extra tracks in piece that are unreferenced

    vector<tuple<int,string>> tracks;
    vector<tuple<int,int>> bars;
    int num_cond_tracks = 0;
    int num_resample_tracks = 0;
    int num_infill_tracks = 0;
    vector<STATUS_TRACK_TYPE> track_types;
    vector<int> order;
    vector<int> cond_tracks;

    int track_num = 0;
    for (const auto track : status->tracks()) {
      STATUS_TRACK_TYPE tt = infer_track_type(track);
      switch( tt ) {
        case CONDITION :
          order.push_back( num_cond_tracks );
          cond_tracks.push_back( track.track_id() );
          num_cond_tracks++;
          break;
        case RESAMPLE : 
          order.push_back( num_resample_tracks );
          tracks.push_back( make_pair(track.track_type(), track.instrument()) );
          //prune_tracks.push_back( track.track_id() );
          num_resample_tracks++;
          break;
        case INFILL :     
          num_infill_tracks++;
          break;
      }
      track_types.push_back( tt );
      int bar_num = 0;
      for (const auto selected : track.selected_bars()) {
        if (selected) {
          bars.push_back( make_pair(track_num, bar_num) );
        }
        bar_num++;
      }
      track_num++;
    }

    cout << num_cond_tracks << " " << num_resample_tracks << endl;

    // use bar infill model
    if (num_infill_tracks > 0) {

      model_key = make_pair(num_bars, BAR_INFILL_MODEL);
      samplers[model_key]->generate_bars(bars, piece, output, 4, temp);

    }
    else {

      // fix the order
      for (track_num=0; track_num<status->tracks_size(); track_num++) {
        if (track_types[track_num] == RESAMPLE) {
          order[track_num] = order[track_num] + num_cond_tracks;
        }
      }

      // prune unneeded tracks
      prune_tracks_dev2(piece, cond_tracks, {0,1,2,3});

      // call generation
      model_key = make_pair(num_bars, TRACK_MODEL);
      samplers[model_key]->generate_tracks(tracks, piece, output, 4, temp);

      // reorder the tracks


    }
    
    // go from tokens back to midi::Piece



    return output;
  }

  vector<string> generate_py(string &status_str, string &piece_str, float temp) {
    midi::Piece piece;
    google::protobuf::util::JsonStringToMessage(piece_str.c_str(), &piece);
    midi::Status status;
    google::protobuf::util::JsonStringToMessage(status_str.c_str(), &status);
    auto pieces = generate(&status, &piece, temp);
    vector<string> output;
    for (const auto piece : pieces) {
      string json_str;
      google::protobuf::util::MessageToJsonString(piece, &json_str);
      output.push_back( json_str );
    }
    return output;
  }

  vector<string> generate_w_midi_py(string &status_str, string &filepath, float temp) {
    midi::Piece piece;
    parse_new(filepath, &piece, NULL, NULL);
    midi::Status status;
    google::protobuf::util::JsonStringToMessage(status_str.c_str(), &status);
    auto pieces = generate(&status, &piece, temp);
    vector<string> output;
    for (const auto piece : pieces) {
      string json_str;
      google::protobuf::util::MessageToJsonString(piece, &json_str);
      output.push_back( json_str );
    }
    return output;
  }

  map<tuple<int,MODEL_TYPE>,Sampler*> samplers;

};

