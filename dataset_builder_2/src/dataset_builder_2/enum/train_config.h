#pragma once

class TrainConfig {
public:
  TrainConfig() {
    num_bars = 4;
    min_tracks = 2;
    max_tracks = 12;
    max_mask_percentage = 0.75;
    opz = false;
  }
  map<string,string> to_json() {
    map<string,string> x;
    x["num_bars"] = to_string(num_bars);
    x["min_tracks"] = to_string(min_tracks);
    x["max_tracks"] = to_string(max_tracks);
    x["max_mask_percentage"] = to_string(max_mask_percentage);
    x["opz"] = to_string((int)opz);
    return x;
  }
  void from_json(map<string,string> &x) {
    num_bars = stoi(x["num_bars"]);
    min_tracks = stoi(x["min_tracks"]);
    max_tracks = stoi(x["max_tracks"]);
    max_mask_percentage = stof(x["max_mask_percentage"]);
    opz = (bool)stoi(x["opz"]);
  }
  int num_bars;
  int min_tracks;
  int max_tracks;
  float max_mask_percentage;
  bool opz;
};