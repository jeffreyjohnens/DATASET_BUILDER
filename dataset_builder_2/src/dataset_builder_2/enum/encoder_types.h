#pragma once

#include "../encoder/encoder_all.h"
#include <string>

enum ENCODER_TYPE {
  TE_TRACK_DENSITY_ENCODER,
  TRACK_INTERLEAVED_ENCODER,
  TRACK_ENCODER,
  NO_ENCODER
};

ENCODER* getEncoder(ENCODER_TYPE et) {
  switch (et) {
    case TE_TRACK_DENSITY_ENCODER: return new TeTrackDensityEncoder();
    case TRACK_INTERLEAVED_ENCODER: return new TrackInterleavedEncoder();
    case TRACK_ENCODER: return new TrackEncoder();
    case NO_ENCODER: return NULL;
  }
}

ENCODER_TYPE getEncoderType(string &s) {
  if (s == "TE_TRACK_DENSITY_ENCODER") return TE_TRACK_DENSITY_ENCODER;
  if (s == "TRACK_INTERLEAVED_ENCODER") return TRACK_INTERLEAVED_ENCODER;
  if (s == "TRACK_ENCODER") return TRACK_ENCODER;
  return NO_ENCODER;
}

int getEncoderSize(ENCODER_TYPE et) {
  ENCODER *encoder = getEncoder(et);
  if (!encoder) {
    return 0;
  }
  int size = encoder->rep->max_token();
  delete encoder;
  return size;
}