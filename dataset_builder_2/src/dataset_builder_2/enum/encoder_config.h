#pragma once
  
#include <vector>
#include <tuple>
#include <map>
class EncoderConfig {
public:
  EncoderConfig() {
    te = false;
    do_fill = false;
    do_multi_fill = false;
    do_track_shuffle = true;
    force_instrument = false;
    mark_polyphony = false;
    mark_density = false;
    mark_time_sigs = false;
    instrument_header = false;
    use_velocity_levels = false;
    genre_header = false;
    piece_header = true;
    bar_major = false;
    force_four_four = false;
    segment_mode = false;
    force_valid = false;
    use_drum_offsets = true;
    transpose = 0;
    seed = -1;
    segment_idx = -1;
    fill_track = -1;
    fill_bar = -1;
    max_tracks = 12;
    resolution = 12;
    default_tempo = 104;
    num_bars = 4;
    min_tracks = 1;
    fill_percentage = 0;
  }
  bool te;
  bool do_fill;
  bool do_multi_fill;
  bool do_track_shuffle;
  bool force_instrument;
  bool mark_polyphony;
  bool mark_density;
  bool mark_time_sigs;
  bool instrument_header;
  bool use_velocity_levels;
  bool genre_header;
  bool piece_header;
  bool bar_major;
  bool force_four_four;
  bool segment_mode;
  bool force_valid;
  bool use_drum_offsets;
  int transpose;
  int seed;
  int segment_idx;
  int fill_track;
  int fill_bar;
  int max_tracks;
  int resolution;
  int default_tempo;
  int num_bars;
  int min_tracks;
  float fill_percentage;
  set<tuple<int,int>> multi_fill;
  vector<string> genre_tags;
};