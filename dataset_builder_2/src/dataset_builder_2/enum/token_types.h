#pragma once

enum TOKEN_TYPE {
  PIECE_START,
  NOTE_ONSET,
  NOTE_OFFSET,
  PITCH,
  NON_PITCH,
  VELOCITY,
  TIME_DELTA,
  INSTRUMENT,
  BAR,
  BAR_END,
  TRACK,
  TRACK_END,
  DRUM_TRACK,
  FILL_IN,
  HEADER,
  VELOCITY_LEVEL,
  GENRE,
  DENSITY_LEVEL,
  TIME_SIGNATURE,
  SEGMENT,
  SEGMENT_END,
  SEGMENT_FILL_IN,
  NONE
};

inline const char* toString(TOKEN_TYPE tt) {
  switch (tt) {
    case PIECE_START: return "PIECE_START";
    case NOTE_ONSET: return "NOTE_ONSET";
    case NOTE_OFFSET: return "NOTE_OFFSET";
    case PITCH: return "PITCH";
    case NON_PITCH: return "NON_PITCH";
    case VELOCITY: return "VELOCITY";
    case TIME_DELTA: return "TIME_DELTA";
    case INSTRUMENT: return "INSTRUMENT";
    case BAR: return "BAR";
    case BAR_END: return "BAR_END";
    case TRACK: return "TRACK";
    case TRACK_END: return "TRACK_END";
    case DRUM_TRACK: return "DRUM_TRACK";
    case FILL_IN: return "FILL_IN";
    case HEADER: return "HEADER";
    case VELOCITY_LEVEL: return "VELOCITY_LEVEL";
    case GENRE: return "GENRE";
    case DENSITY_LEVEL: return "DENSITY_LEVEL";
    case TIME_SIGNATURE: return "TIME_SIGNATURE";
    case SEGMENT: return "SEGMENT";
    case SEGMENT_END: return "SEGMENT_END";
    case SEGMENT_FILL_IN: return "SEGMENT_FILL_IN";
    case NONE: return "NONE";
  }
}

