import dataset_builder_2 as db
import numpy as np
from tqdm import tqdm
import json

# build a quick dataset

# python3 build_dataset.py --data_dir /Users/Jeff/DATA/lmd_full --output test.arr --max_size 5000

# time how fast a batch is formed

if __name__ == "__main__":

  import time
  np.random.seed(int(time.time()))

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--mode", type=str, required=True)
  parser.add_argument("--batch", action="store_true")
  parser.add_argument("--from_dataset", action="store_true")
  args = parser.parse_args()

  mode = db.getEncoderType(args.mode)
  enc = db.getEncoder(mode)
  e = db.EncoderConfig()
  e.seed = np.random.randint(2*20)
  e.genre_tags = ["Jazz", "none"]
  #e.segment_idx = 0
  e.num_bars = 8
  e.force_four_four = True
  e.force_valid = False
  print(db.getEncoderSize(mode))


  if args.from_dataset:
    jag = db.Jagged("test_NUM_BARS=4.arr")
    index = np.random.randint(1000)
    jstr = jag.read_json(index,0)
    tokens = np.array(enc.json_to_tokens(jstr,e))
    ntracks = np.sum((tokens==enc.rep.encode({db.TOKEN_TYPE.TRACK:0}))|(tokens==enc.rep.encode({db.TOKEN_TYPE.TRACK:1}))) 
    nbars = np.sum(tokens==enc.rep.encode({db.TOKEN_TYPE.BAR:0}))
    print(ntracks, nbars, len(tokens))
  else:
    #path = "../test_midi/MIDI Files/Electronica - Roygbiv (Boards of Canada).mid"
    path = "./stems/stem_30.mid"
    tokens = enc.midi_to_tokens(path,e)
  

  pretty = enc.rep.pretty(tokens)

  # print to a file
  with open("test_rep.json", "w") as f:
    json.dump(pretty, f, indent=4)


  enc.tokens_to_midi(tokens,"test_rep.mid",e)

  """
  exit()

  # split at track
  section = []
  for tok in pretty:
    if "TRACK=" in tok or "BAR=" in tok:
      print(section)
      section = [tok]
    else:
      section.append(tok)
  print(section)

  enc.tokens_to_midi(tokens,"test_rep.mid",e)
  """

  if args.batch:
    split_id = 0
    batch_size = 32
    jag = db.Jagged("test_NUM_BARS=16.arr")
    jag.set_seed(8123467)

    for _ in tqdm(range(100)):
      #batch, is_continued = jag.read_batch_w_continue(
      #  batch_size, split_id, mode, 1024, False)
      batch, mask = jag.read_batch(batch_size, split_id, mode, 0)
      #print(is_continued)
