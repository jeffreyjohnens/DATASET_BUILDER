import dataset_builder_2 as db
from glob import glob
from tqdm import tqdm
import numpy as np

data_dir = "/Users/Jeff/DATA/lmd_full"
paths = list(glob(data_dir + "/**/*.mid", recursive=True))

file_types = []
for path in tqdm(paths):
  try:
    file_types.append( db.check_type(path) )
  except:
    pass

ft = np.array(file_types)
ft = ft[ft>=0]
counts = np.bincount(ft)[:3]
print(counts)
print(counts.astype(np.float32) / np.sum(counts))
