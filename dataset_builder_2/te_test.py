import dataset_builder_2 as db
import json

# TODO : change the way drums are represented (i.e. ignore note offs)
# TODO : change the quantization scheme
# TODO : find way to add the google drum dataset

enc = db.TeTrackDensityEncoder()
ec = db.EncoderConfig()
ec.num_bars = 4

path = '/Users/jeff/DATA/lmd_full/0/0a0ce238fb8c672549f77f3b692ebf32.mid'
midi_json = json.loads(enc.midi_to_json(path,ec))

# loop over a bunch and determine what the average length is
# we want
# average length of sequence
# average length of track


print( len(enc.midi_to_tokens(path,ec)) )

exit()

for track in midi_json.get("tracks",[]):
  print(track["teTrack"], track["maxPolyphony"])

with open("te_test.json", "w") as f:
  json.dump(midi_json,f,indent=4)
