# parse the genre annotations by tagtraum into a json
# NOTE that the data is only for non-commercial use
# THERE ARE UP TO TWO GENRE KEYS PER PIECE

import csv
import json
import os
import glob
import numpy as np
from tqdm import tqdm

paths = [
	"/Users/jeff/DATA/msd_tagtraum_cd1.cls",
	"/Users/jeff/DATA/msd_tagtraum_cd2c.cls",
	"/Users/jeff/DATA/msd_tagtraum_cd2.cls"
]

root_data_dir = "/users/jeff/DATA"
match_folder = os.path.join(root_data_dir, "lmd_matched")
full_folder = os.path.join(root_data_dir, "lmd_full")
full_paths = list(glob.glob(full_folder+"/**/*.mid",recursive=True))
match_paths = list(glob.glob(match_folder+"/**/*.mid",recursive=True))
bpath_to_msd = {os.path.basename(p) : os.path.splitext(os.path.basename(os.path.dirname(p)))[0] for p in match_paths}

# read raw data downloaded as .cls files
vocab = {}
data = {}
for path in paths:
	bpath = os.path.splitext(os.path.basename(path))[0]
	data[bpath] = {}

	with open(path, "r") as f:
		for line in f.readlines():
			if not line.startswith("#"):
				row = [x.strip() for x in line.split("\t")]
				data[bpath][row[0]] = sorted(row[1:])

	vocab[bpath] = list(np.unique(np.hstack(list(data[bpath].values()))))

# make a map from full_path to genre dict
genre_data = {}
for path in tqdm(full_paths,leave=False):
	bpath = os.path.basename(path)
	match_path = bpath_to_msd.get(bpath, None)
	genre_data[bpath] = {}
	for k in data.keys():
		genres = data[k].get(match_path,[])
		while len(genres) < 2:
			genres.append("none")
		genre_data[bpath][k] = genres

with open("genre_data.json", "w") as f:
	json.dump(genre_data, f, indent=4)

# print some stats about how much info we got
for k in data.keys():
	count = 0
	for midi_path in genre_data.keys():
		if len([g for g in genre_data[midi_path][k] if g is not "none"]):
			count += 1
	print(k, count)

# make the const maps for c++
for k,v in vocab.items():
	v = list(v) + ["none"]
	header = '{{"{}",{{\n'.format(k)
	content = ',\n'.join(['  {{"{}",{}}}'.format(vv,i) for i,vv in enumerate(v)])
	footer = '}\n},'
	print(header + content + footer)

