import dataset_builder_2 as db
import glob
import random
import json
import time
import numpy as np
from tqdm import tqdm

random.seed(int(time.time()))

def check_bars(midi_json, num_bars):
  lengths = set()
  for track in midi_json.get("tracks",[]):
    lengths.add( len(track.get("bars",[])) )
  assert len(lengths) == 1
  assert num_bars == list(lengths)[0]

def worker(args):
	path,sid,genre,num_bars = args
	encoder = db.TeTrackDensityEncoder()
	tc = db.TrainConfig()
	tc.num_bars = 4
	try:
		return sid, encoder.midi_to_json_bytes(path,tc)
	except Exception as e:
		print(e)
		return None,None

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)
#paths = glob.glob("/Users/Jeff/DATA/groove/**/*.mid", recursive=True)

path = random.choice(paths)

enc = db.TrackInterleavedEncoder()
raw = enc.midi_to_json(path)
raw = db.select_random_segment(raw, 4, 2, 12, False)
tokens = enc.json_to_tokens(raw)


print(len(tokens), enc.rep.vocab_size)

enc = db.TrackEncoder()
tokens = enc.json_to_tokens(raw)
#for token in tokens:
#  print(enc.rep.pretty(token))

print(len(tokens), enc.rep.vocab_size)

exit()


raw = encoder.midi_to_json(path)
raw = db.select_random_segment(raw, 4, 2, 12, True)
for transpose in [0,3]:
  encoder.config.transpose = transpose
  tokens = encoder.json_to_tokens(raw)
  encoder.tokens_to_midi(tokens, "test_{}.mid".format(transpose))




exit()

lengths = []
for _ in tqdm(range(100)):
  try:

    path = random.choice(paths)
    raw = encoder.midi_to_json(path)
    raw = db.select_random_segment(raw, 4, 2, 12, True)
    num_tracks = len(json.loads(raw).get("tracks",[]))
    tokens = encoder.json_to_tokens(raw)
    encoder.tokens_to_midi(tokens, "test2.mid")
    #print("="*50)
    #for token in tokens:
    #  print(encoder.rep.pretty(token))
    exit()
    lengths.append( len(tokens) / num_tracks )
    #midi_json = json.loads(raw)
    #check_bars(midi_json,4)
  
  except Exception as e:
    pass

print(np.median(lengths))
