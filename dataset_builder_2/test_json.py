import dataset_builder_2 as db
import json
import numpy as np

path = "stems/stem_2.mid"
#path = "../test_midi/MIDI Files/Pop - Rock with you (MJ).mid"

encoder = db.TrackEncoder()
e = db.EncoderConfig()

midi_json = json.loads(encoder.midi_to_json(path,e))
#midi_json["events"] = []
#print( json.dumps(midi_json, indent=4) )

# make a svg please
def get_track_notes(js, track, compress_pitches=False):
  notes = []
  onsets = {}
  for bar in track.get("bars",[]):
    for event_id in bar.get("events",[]):
      event = js["events"][event_id]
      if event["velocity"] > 0:
        onsets[event["pitch"]] = event
      elif event["pitch"] in onsets:
        notes.append({
          "start" : onsets[event["pitch"]]["time"],
          "end" : event["time"],
          "pitch" : event["pitch"],
        })
        onsets.pop(event["pitch"])
  if compress_pitches:
    pitches = np.sort(np.unique([n["pitch"] for n in notes]))
    pitch_map = {p:i for i,p in enumerate(pitches)}
    for note in notes:
      note["pitch"] = pitch_map[note["pitch"]]
  return notes

def make_svg_rect(x, y, width, height, fill, stroke_width=0, stroke="none"):
  return '<rect x="{}%" y="{}%" width="{}%" height="{}%" style="fill:{};stroke-width:{};stroke:{};" />'.format(x,y,width,height,fill,stroke_width,stroke)

def make_svg_track(notes):

  resolution = 12
  max_notes = 20
  xticks = resolution * 16 # 16 beats
  beat_width = 100. / 32.
  beat_color = "#9E9E9E"
  
  bar_width = 100. / 4.
  bar_colors = ["#A9A9A9", "#B4B4B4"]

  pitch_offset = max(0,max_notes - np.max([n["pitch"] for n in notes])) / 2
  small = 1. / xticks / 4

  header = '''<div width="800px">
<svg width="100%" height="150px" viewBox="0 0 100 100" preserveAspectRatio="none">
'''
  content = []
  for i in range(4):
    content.append(
      make_svg_rect(i*bar_width, 0, bar_width, 100, bar_colors[i%2]))
  for i in range(32):
    content.append(
      make_svg_rect(i*beat_width, 0, beat_width, 100, "none", .15, beat_color))
  for note in notes:
    y = 100 - (float(note["pitch"] + pitch_offset) / max_notes * 100)
    x = float(note["start"]) / xticks  * 100
    w = ((float(note["end"] - note["start"]) / xticks) - small) * 100
    h = 1. / max_notes * 100
    content.append(make_svg_rect(x,y,w,h,"red"))
  footer = '''</svg></div>'''

  return header + "\n".join(content) + footer

for track in midi_json.get("tracks", []):
  notes = get_track_notes(midi_json, track, compress_pitches=True)
  print(make_svg_track(notes))

  
