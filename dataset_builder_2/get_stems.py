# turn a midi file into a bunch of stems
import os
import json
import glob
import random
import dataset_builder_2 as db

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)

stems = []
while len(stems) < 10:
  path = random.choice(paths)
  encoder = db.TrackEncoder()
  e = db.EncoderConfig()
  e.num_bars = 4
  x = encoder.midi_to_json(path,e)
  x = db.select_segment(x, 4, -1)
  if len(x):
    stems.append( json.loads(x) )

with open("get_stems.json", "w") as f:
  json.dump(stems,f)