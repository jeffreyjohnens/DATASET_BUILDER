import numpy as np
from tqdm import tqdm

data = np.load("STAT_OPZ_note_density_160000.npz", allow_pickle=True)["data"]

density = {}
for tt,inst,n in tqdm(data,leave=False):
  if not (tt,inst) in density:
    density[(tt,inst)] = []
  density[(tt,inst)].append(n)

def qnt(x):
  qnts = [.1,.2,.3,.4,.5,.6,.7,.8,.9]
  return np.hstack([np.quantile(x,qnts), [2**30]]).astype(np.int32)

density = {k:qnt(v) for k,v in density.items()}

rows = []
for k,v in density.items():
  v = "{" + ",".join([str(vv) for vv in v]) + "}"
  rows.append( "{{{key},{value}}}".format(key=k,value=v) )

with open("src/dataset_builder_2/enum/density_opz.h", "w") as f:
  f.write("""#include <unordered_map>

unordered_map<tuple<int,int>,vector<int>> DENSITY_QUANTILES = {
  """)
  f.write(",\n  ".join(rows))
  f.write("\n};")