import dataset_builder_2 as db
from tqdm import tqdm

tc = db.TrainConfig()
d = db.Jagged("/Users/Jeff/DATA/lmd_NUM_BARS=4_OPZ_False.arr")

for _ in tqdm(range(10000)):
  batch,mask = d.read_batch(32, 0, db.ENCODER_TYPE.TRACK_INTERLEAVED_ENCODER, tc)
