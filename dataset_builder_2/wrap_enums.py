
path = "src/dataset_builder_2/encoder.h"

def split_on_top_level(text, start_token, end_token, split_token):
  stack = []
  splits = []
  segment = ""
  for char in text:
    if char == start_token:
      stack.append("@")
    if char == end_token:
      stack.pop()
    if len(stack) == 0 and char == split_token:
      splits.append(segment)
      segment = ""
    else:
      segment += char
  if len(segment) :
    splits.append(segment)
  return splits

with open(path, "r") as f:
  text = " ".join(f.readlines())


for chunk in text.split("enum")[1:]:
  code = chunk.split("};")[0]
  name,content = code.split("{")
  values = content.split(",")
  values = [v.strip() for v in values if len(v.strip())]
  name = name.strip()

  print('py::enum_<{}>(m, "{}", py::arithmetic())'.format(name,name))
  for value in values:
    print('  .value("{}", {}::{})'.format(value,name,value))
  print('  .export_values();')
  print("\n\n")

# use a stack to parse classes
for chunk in text.split("class")[1:]:
  methods = []
  stack = []
  line = ""
  code = chunk.split("};")[0]
  name = code.split("{")[0].lstrip().split()[0]
  content = code[len(code.split("{")[0])+1:]
  for char in content:
    if char == "{":
      stack.append("{")
    elif char == "}":
      stack.pop()
      if "(" in line:
        method_name,params = line.split("(")
        method_name = method_name.split()[-1]
        if method_name == name:
          params = split_on_top_level(params,"<",">",",")
          data_types = []
          for p in params:
            data_type = " ".join(p.rstrip().split()[:-1])
            if p.rstrip().split()[-1][0] == "*":
              data_type += "*"
            data_types.append( data_type )
        else:
          methods.append( method_name )
      line = ""
    elif len(stack) == 0:
      line += char
  
  methods = [m for m in methods if not m.startswith("~")]

  print('py::class_<{}>(m, "{}")'.format(name,name))
  print('.def(py::init<{}>())'.format(",".join(data_types)))
  for method in methods:
    semi_colon = ";" if method == methods[-1] else ""
    print('.def("{}", &{}::{})'.format(method,name,method) + semi_colon)
  print("\n\n")