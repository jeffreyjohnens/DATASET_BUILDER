# turn a midi file into a bunch of stems
import os
import json
import dataset_builder_2 as db

if __name__ == "__main__":

  #python3 stem_maker.py --path '../test_midi/MIDI Files/Pop - Rock with you (MJ).mid'

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--path", required=True, type=str)
  args = parser.parse_args()

  encoder = db.TrackEncoder()
  e = db.EncoderConfig()
  mj = json.loads(encoder.midi_to_json(args.path,e))

  os.makedirs("stems", exist_ok=True)

  for i in range(len(mj["validSegments"])):
    ji = db.select_segment(json.dumps(mj), 32, i)
    encoder.json_to_midi(ji, "stems/stem_{}.mid".format(i), e)