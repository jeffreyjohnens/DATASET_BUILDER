template = """
py::class_<{encoder_name}>(m, "{encoder_name}")
  .def(py::init<>())
  .def("encode", &{encoder_name}::encode)
  .def("decode", &{encoder_name}::decode)
  .def("midi_to_json", &{encoder_name}::midi_to_json)
  .def("midi_to_tokens", &{encoder_name}::midi_to_tokens)
  .def("json_to_midi", &{encoder_name}::json_to_midi)
  .def("json_to_tokens", &{encoder_name}::json_to_tokens)
  .def("tokens_to_json", &{encoder_name}::tokens_to_json)
  .def("tokens_to_midi", &{encoder_name}::tokens_to_midi)
  .def_readwrite("rep", &{encoder_name}::rep);
"""

encoder_names = [
  "TrackEncoder",
  "TrackOneBarFillEncoder",
  "TrackMonoPolyEncoder"
]

for encoder_name in encoder_names:
  print(template.format(encoder_name=encoder_name))

fields = [
  'do_fill', 
  'do_track_shuffle', 
  'force_instrument', 
  'transpose', 
  'seed', 
  'segment_idx', 
  'fill_track',
  'fill_bar',
  'max_tracks',
  'resolution'
]

for field in fields:
  print(""".def_readwrite("{}", &EncoderConfig::{})""".format(field,field))
