import dataset_builder_2 as db
import glob
import numpy as np
from subprocess import call
from tqdm import tqdm
from multiprocessing import Pool
import time
import random

# issues
# cd src/dataset_builder_2; protoc --cpp_out . midi.proto; cd ../..;  python setup.py install
# the dataset building hangs at the end
# DONE transposition is not enabled yet
# DONE need to be able to filter somwhere in the pipeline

# get random file from lahk midi dataset
paths = glob.glob("/Users/jeff/DATA/lmd_full/**/*.mid", recursive=True)
#paths = glob.glob("/Users/Jeff/SFU/DATA/clean_midi/**/*.mid", recursive=True)

mode = db.ENCODER_TYPE.TRACK_BAR_ENCODER
for _ in range(100):
  path = np.random.choice(paths)
  db.back_and_forth(path, "bandf.mid", 12, mode, 3)
  call("cp '" + path + "' bandf_orig.mid", shell=True)
  call("open bandf_orig.mid", shell=True)
  call("open bandf.mid", shell=True)
  break
exit()

# read a batch
jag = dataset_builder_2.Jagged("test.arr")
start = time.time()
batch,mask = jag.read_batch(512, 0, dataset_builder_2.ENCODER_TYPE.TRACK_ENCODER)

print([len(_) for _ in batch])
print(time.time() - start)
exit()

# build the dataset
def worker(args):
  path,sid = args
  try:
    return sid, dataset_builder_2.encode(path,12)
  except:
    return None,None

jag = dataset_builder_2.Jagged("test.arr")
sids = np.random.choice([0,1,2], p=[.8,.1,.1], size=(len(paths),))
inputs = list(zip(paths,sids))
random.shuffle(inputs)

pool = Pool(8)
for sid,b in tqdm(pool.imap_unordered(worker, inputs), total=len(paths)):
  if b is not None and len(b):
    jag.append(b,sid)
jag.close()