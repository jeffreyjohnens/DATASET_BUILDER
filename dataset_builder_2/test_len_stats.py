import os
import json
import random
import numpy as np
import dataset_builder_2 as db
from tqdm import tqdm
import glob
from multiprocessing import Pool

def sequence_length(midi_json, **kwargs):
  if len(midi_json.get("tracks",[])) < kwargs["min_tracks"]:
    return []
  encoder = db.TrackDensityEncoder()
  e = db.EncoderConfig()
  e.num_bars = kwargs["num_bars"]
  e.min_tracks = kwargs["min_tracks"]
  e.max_tracks = kwargs["max_tracks"]
  raw_json = db.update_segments(json.dumps(midi_json),e)
  valid_segments = json.loads(raw_json)["validSegments"]
  e.segment_idx = np.random.randint(len(valid_segments))
  tokens = encoder.json_to_tokens(raw_json,e)
  return [len(tokens)]

def worker(args):
  path,kwargs = args
  try:
    e = db.EncoderConfig()
    enc = db.TrackEncoder()
    data = json.loads(enc.midi_to_json(path,e))
    return sequence_length(data,**kwargs)
  except Exception as e:
    return []

def compute_stat_on_dataset(pool=None, n=100, **kwargs):
  stat = []
  random.shuffle(paths)
  inputs = [(path,kwargs) for path in paths]
  pbar = tqdm(total=n,leave=False)
  p = Pool(8)
  for out in p.imap_unordered(worker, inputs):  
    stat.extend(out)
    pbar.update(len(out))
    if len(stat) >= n:
      break
  pbar.close()
  return stat

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)

data = {}
for nbars in tqdm([4,8,16],leave=False):
  for ntracks in tqdm([10],leave=False):
    kwargs = {
      "num_bars" : nbars, "min_tracks" : ntracks, "max_tracks" : ntracks}
    stat = compute_stat_on_dataset(n=500, **kwargs)
    data[(nbars,ntracks)] = stat
np.savez_compressed("lmd_seq_len_stats.npz", data=data)

data = np.load("lmd_seq_len_stats.npz", allow_pickle=True)["data"][()]
for k,v in data.items():
  print(k,np.nanmean(np.array(v)<=1024),np.nanmean(np.array(v)<=1536),np.nanmean(np.array(v)<=2048),len(v))