from dataset_builder_2 import Jagged
import numpy as np
from subprocess import call

def array_test():
  data = np.random.randint(100, size=(1000,10))
  sids = np.random.choice([0,1,2], p=[.8,.1,.1], size=(len(data),))
  data_split = [[data[i] for i in np.where(sids==k)[0]] for k in range(3)]

  jag = Jagged("trash.arr")
  for x,sid in zip(data,sids):
    jag.append(x.tobytes(),sid)
  jag.close()

  for sid,size in enumerate(np.bincount(sids)):
    assert jag.get_split_size(sid) == size
    for i in range(size):
      x = np.frombuffer(jag.read_bytes(i,sid),dtype=data.dtype)
      assert np.array_equal(x, data_split[sid][i])


if __name__ == "__main__":

  for _ in range(10):
    array_test()
  
  # delete the stored arrays
  call("rm trash.arr", shell=True)
  call("rm trash.arr.header", shell=True)
  