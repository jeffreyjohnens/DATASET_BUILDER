# calculate stats on the genre data available
import json

with open("genre_data.json", "r") as f:
  data = json.load(f)

counts = {
  "msd_tagtraum_cd1": {},
  "msd_tagtraum_cd2c": {},
  "msd_tagtraum_cd2" : {}
}

counts_by_genre = {}
for k,v in data.items():
  for dataset,genres in v.items():
    for genre in genres:
      if genre != "none":
        counts[dataset][genre] = counts[dataset].get(genre,0) + 1
        counts_by_genre[genre] = counts_by_genre.get(genre,0) + 1
    if any([genre != "none" for genre in genres]):
      counts[dataset]["TOTAL"] = counts[dataset].get("TOTAL",0) + 1


sorted_genres = [genre for genre,_ in sorted(counts_by_genre.items(),key=lambda x : x[1])[::-1]]
sorted_genres.append( "TOTAL" )

for genre in sorted_genres:
  print( " & ".join([genre,str(counts["msd_tagtraum_cd1"].get(genre,"-")),str(counts["msd_tagtraum_cd2"].get(genre,"-")),str(counts["msd_tagtraum_cd2c"].get(genre,"-")) ]) + "\\\\" )