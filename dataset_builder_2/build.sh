source ~/HUGGING/bin/activate
rm -rf build
rm -rf dist
rm -rf dataset_builder_2.egg-info
module load protobuf/3.3.0
cd src/dataset_builder_2/protobuf
protoc --cpp_out . midi.proto
cd ../../..
python setup.py install
deactivate