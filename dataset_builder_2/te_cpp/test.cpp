#include <torch/script.h> // One-stop header.

#include <iostream>
#include <memory>
#include <vector>

#include <fstream>

int main(int argc, const char* argv[]) {
  torch::jit::script::Module module;
  try {
    // Deserialize the ScriptModule from a file using torch::jit::load().
    module = torch::jit::load("/Users/Jeff/CODE/DATASET_BUILDER/dataset_builder_2/te_cpp/convert.pt");
  }
  catch (const c10::Error& e) {
    std::cerr << "error loading the model\n";
    return -1;
  }

  // create a tuple and then pass it as inputs 
  std::vector<torch::jit::IValue> inputs;

  // create a tensor from a vector instead
  std::ifstream infile;
  infile.open("../LOGITS_INPUT.txt");

  int val;
  std::vector<long> x;
  while (infile >> val) {
    x.push_back(val);
  }
  auto options = torch::TensorOptions().dtype(torch::kInt64);
  auto input = torch::from_blob(x.data(),{1,100},options);

  //auto options = torch::TensorOptions().dtype(torch::kInt64);
  //torch::Tensor input = torch::zeros({2, 300}, options);
  inputs.push_back(input);
  
  auto outputs = module.forward(inputs);
  auto logits = outputs.toTuple()->elements()[0].toTensor().index({torch::indexing::Slice(),-1,torch::indexing::Slice()});



  // write logits to file
  std::ofstream myfile;
  myfile.open("../LOGITS_CPP.txt");
  for (int i=0; i<448; i++) {
    myfile << (int)( (*logits.index({0,i}).data<float>()) * 100000 ) << "\n"; 
  }
  myfile.close();
  
  





}