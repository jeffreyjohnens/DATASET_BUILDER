# take a trained pytorch model and convert it

import torch
from transformers import GPT2LMHeadModel

ckpt_path = "/Users/jeff/Code/DATASET_BUILDER/examples/local_notebook/checkpoints/TRACK_DENSITY_ENCODER_gpt2_version3_Aug_04_14_29_False/checkpoint-405000"

model = GPT2LMHeadModel.from_pretrained(ckpt_path, torchscript=True)

example_input = torch.zeros(1,300).type(torch.LongTensor)

traced_script_module = torch.jit.trace(model, example_input)

traced_script_module.save("convert.pt")