# check that the output are the same

import torch
import numpy as np
from transformers import GPT2LMHeadModel

ckpt_path = "/Users/jeff/Code/DATASET_BUILDER/examples/local_notebook/checkpoints/TRACK_DENSITY_ENCODER_gpt2_version3_Aug_04_14_29_False/checkpoint-405000"

model = GPT2LMHeadModel.from_pretrained(ckpt_path)

# write inputs to a file to be read by the c code
inputs = np.random.randint(400, size=(100,))

with open("LOGITS_INPUT.txt", "w") as f:
  for val in inputs:
    f.write(str(val) + "\n")

# pass through python model
inputs = {"input_ids" : torch.from_numpy(inputs[None,:]).to(torch.long) }
logits = model(**inputs)[0][:,-1,:]
with open("LOGITS_PY.txt", "w") as f:
  for val in logits.detach().numpy().flatten():
    f.write(str(int(val*100000)) + "\n")

# pass through cpp model
from subprocess import call
call(["sh", "build.sh"])

with open("LOGITS_PY.txt", "r") as f:
  py_data = [int(_) for _ in f.readlines()]

with open("LOGITS_CPP.txt", "r") as f:
  cpp_data = [int(_) for _ in f.readlines()]

for p,c in zip(py_data, cpp_data):
  assert abs(p-c) <= 1