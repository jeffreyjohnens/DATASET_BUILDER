#include <torch/script.h> // One-stop header.

#include <iostream>
#include <string>
#include <vector>

class Sampler {
public:
  Sampler(string ckpt_path) {
    // ckpt_path must be absolute path
    load_model(ckpt_path);
  }
  void load_model(string &ckpt_path) {
    try {
      module = torch::jit::load(ckpt_path);
    }
    catch (const c10::Error& e) {
      std::cerr << "error loading the model\n";
      return -1;
    }
  }
  torch::Tensor forward(torch::Tensor &x) {
    std::vector<torch::jit::IValue> inputs;
    inputs.push_back(x);
    auto outputs = module.forward(inputs);
    return outputs.toTuple()->elements()[0].toTensor().index({torch::indexing::Slice(),-1,torch::indexing::Slice()});
  }

  torch::jit::script::Module model;
};

