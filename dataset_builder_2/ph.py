import dataset_builder_2 as db
import glob
import random
import json
import numpy as np

def make_test_json(ntracks, nbars):
  midi_json = {"tracks" : [], "events" : []}
  for i in range(ntracks):
    track = []
    for j in range(nbars):
      bar = []
      for k in range(1):
        bar.append( len(midi_json["events"]) )
        midi_json["events"].append({
          "pitch" : int(np.random.randint(128)),
          "time" : int(np.random.randint(2048)),
          "velocity" : int(np.random.choice([0,100])),
        })
      track.append( {"events" : bar} )
    midi_json["tracks"].append( {"bars" : track} )
  return midi_json

def track_summary(p):
  midi_json = json.loads(p)
  for track in midi_json.get("tracks",[]):
    track["bars"] = [len(b) for b in track["bars"]]
  #midi_json["events"] = []
  print( json.dumps(midi_json,indent=4) )

encoder = db.TeTrackDensityEncoder()
raw = encoder.midi_to_json("9_apollo_seeds.midi")
with open("ph.json", "w") as f:
  json.dump(json.loads(raw),f,indent=4)

exit()

for token in tokens[:20]:
  print( token, encoder.rep.pretty(token) )

exit()
encoder = db.TrackDensityEncoder()
tokens_orig = encoder.midi_to_tokens("9_apollo_seeds.midi", ec)
for token in tokens_orig[:20]:
  print( token, encoder.rep.pretty([token])[0] )

for i,(a,b) in enumerate(zip(tokens, tokens_orig)):
  print(i,a,b)
  assert( a == b )

exit()

#raw = encoder.tokens_to_json(tokens, ec)
#print(json.dumps(json.loads(raw),indent=4))

encoder.tokens_to_midi(tokens, "ph2.mid", ec)

exit()

track_path = "/Users/Jeff/CODE/TE_MMM/code/track.pt"
ckpts = {
  (4,db.MODEL_TYPE.TRACK_MODEL) : track_path
}
generator = db.Generator(ckpts)

status = '{"tracks":[{"track_id":0,"track_type":1,"instrument":"drum_0","density":1,"selected_bars":[false,false,false,false]},{"track_id":1,"track_type":0,"instrument":"Acoustic Grand Piano","density":1,"selected_bars":[false,false,false,false]},{"track_id":2,"track_type":1,"instrument":"Acoustic Grand Piano","density":1,"selected_bars":[false,false,false,false]},{"track_id":3,"track_type":0,"instrument":"bass","density":1,"selected_bars":[true,true,true,true]}]}'
jsons = generator.generate_w_midi(status, "9_apollo_seeds.midi", .975)

ec = db.EncoderConfig()
encoder = db.TrackDensityEncoder()

#raw = encoder.midi_to_json("9_apollo_seeds.midi", ec)
#print(json.dumps(json.loads(jsons[0]),indent=4))
#exit()

encoder.json_to_midi(jsons[0], "ph.mid", ec)

exit()


path = "9_apollo_seeds.midi"
encoder = db.TrackDensityEncoder()
ec = db.EncoderConfig()

p = encoder.midi_to_json(path,ec)
tokens = encoder.midi_to_tokens(path,ec)

print(tokens)
exit()

with open("ph.json", "w") as f:
  json.dump(json.loads(p),f,indent=4)

#track_summary(p)
exit()

midi_json = make_test_json(3,4)
print(json.dumps(midi_json,indent=4))

raw = db.prune_tracks_dev(json.dumps(midi_json), [0,2], [0,3])
midi_json = json.loads(raw)

print(json.dumps(midi_json,indent=4))

exit()



paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)
path = random.choice(paths)

encoder = db.TrackDensityEncoder()
ec = db.EncoderConfig()

p = encoder.midi_to_json(path,ec)

track_summary(p)

p = db.prune_tracks_dev(p, [1,2,3,4,5,6,7,8,9,10])

print(json.dumps(json.loads(p),indent=4))