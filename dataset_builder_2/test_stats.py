import os
import json
import numpy as np
import dataset_builder_2 as db
from tqdm import tqdm
import glob
from multiprocessing import Pool

def num_notes_per_bar(midi_json):
  output = []
  for track in midi_json.get("tracks",[]):
    track_type = track["type"]
    track_inst = track["instrument"]
    for bar in track.get("bars",[]):
      num_notes = 0
      for event_id in bar.get("events",[]):
        if midi_json["events"][event_id]["velocity"]:
          num_notes += 1
      if num_notes:
        output.append( (track_type,track_inst,num_notes) )
  return output

def track_type_count(midi_json):
  output = []
  for track in midi_json.get("tracks",[]):
    if any([bar["hasNotes"] for bar in track.get("bars",[])]):
      output.append( track["instrument"] )
  return output

def drum_program_numbers(midi_json):
  output = []
  for track in midi_json.get("tracks",[]):
    if track["isDrum"]:
      output.append( track["instrument"] )
  return output

# get the quantiles for the number of note
# onsets and average polyphony
# these should just be stored in the file
def num_notes_per_track(midi_json):
  # get a random segment
  midi_json = json.loads(db.select_segment(json.dumps(midi_json),32,-1))
  output = []
  for track in midi_json.get("tracks",[]):
    num_notes = 0
    for bar in track.get("bars",[]):
      for event_id in bar.get("events",[]):
        if (midi_json["events"][event_id]["velocity"]):
          num_notes += 1
    # only calculate monophonic / polyphonic
    if track["avPolyphony"] >= 1.1:
      output.append(num_notes)
  return output

def sequence_lengths(midi_json, **kwargs):
  encoder = db.TrackMonoPolyDensityEncoder()
  e = db.EncoderConfig()
  e.num_bars = kwargs["num_bars"]
  e.min_tracks = kwargs["min_tracks"]
  e.max_tracks = kwargs["max_tracks"]
  raw_json = db.update_segments(json.dumps(midi_json),e)
  valid_segments = json.loads(raw_json)["validSegments"]
  e.segment_idx = np.random.randint(len(valid_segments))
  tokens = encoder.json_to_tokens(raw_json,e)
  return [len(tokens)]

def beat_length(midi_json):
  output = []
  for track in midi_json.get("tracks",[]):
    for bar in track.get("bars",[]):
      output.append(bar["beatLength"])
  return output

def compute_stat(stat_fn, path, n=1000, **kwargs):
  jag = db.Jagged(path)
  stat = []
  with tqdm(total=n,leave=False) as pbar:
    while len(stat) < n:
      try:
        i = np.random.randint(jag.get_split_size(0))
        data = json.loads(jag.read_json(i,0))
        output = stat_fn(data, **kwargs)
        stat.extend( output )
        pbar.update( len(output) )
      except Exception as e:
        pass
  return stat

def worker(path):
  try:
    enc = db.TeTrackDensityEncoder()
    data = json.loads(enc.midi_to_json(path))
    return num_notes_per_bar(data)
    #return drum_program_numbers(data)
    #return track_type_count(data)
  except:
    return []

def compute_stat_on_dataset(output_file):
  if os.path.exists(output_file):
    return np.load(output_file, allow_pickle=True)["data"]
  stat = []
  args = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)
  p = Pool(8)
  count = 0
  for out in tqdm(p.imap_unordered(worker, args),total=len(args)):
    stat.extend(out)
    count += 1
    if count < 170000 and count % 10000 == 0:
      np.savez_compressed(os.path.splitext(output_file)[0] + "_{}.npz".format(count), data=stat)

def write_csv(data, header, path):
  import csv
  data = [{k:v for k,v in zip(header,row)} for row in data]
  with open(path, "w") as f:
    dict_writer = csv.DictWriter(f, header)
    dict_writer.writeheader()
    dict_writer.writerows(data)


if __name__ == "__main__":

  stat = compute_stat_on_dataset("STAT_OPZ_note_density.npz")
  exit()


  data = {}
  for nbars in tqdm([4,8,16],leave=False):
    for ntracks in tqdm(range(4,16),leave=False):
      kwargs = {
        "num_bars" : nbars, "min_tracks" : ntracks, "max_tracks" : ntracks}
      stat = compute_stat(
        sequence_lengths, "/Users/Jeff/DATA/lmd_NUM_BARS=4.arr", 1000, **kwargs)
      data[(nbars,ntracks)] = stat

  np.savez_compressed("lmd_seq_len_stats.npz", data=data)
  exit()

  data = np.load("lmd_seq_len_stats.npz", allow_pickle=True)["data"][()]
  for k,v in data.items():
    print(k,np.mean(np.array(v)<=1024),np.mean(np.array(v)<=1536),np.mean(np.array(v)<=2048))

  exit()

  stat = compute_stat_on_dataset("STAT_drum_programs.npz")
  print(np.bincount(stat))
  exit()

  # make a pretty visualization for each instrument
  data = {i:[] for i in range(129)}
  for inst,n in stat:
    data[inst].append(n)

  for i in range(129):
    if len(data[i]):
      print(np.quantile(data[i], [0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1.]), len(data[i]))
    else:
      print( "NONE" )

  exit()

  """
  data = {}
  for nbars in tqdm([4,8,16],leave=False):
    for ntracks in tqdm(range(4,8),leave=False):
      kwargs = {
        "num_bars" : nbars, "min_tracks" : ntracks, "max_tracks" : ntracks}
      stat = compute_stat(
        sequence_lengths, "/Users/Jeff/DATA/lmd_NUM_BARS=4.arr", 1000, **kwargs)
      data[(nbars,ntracks)] = stat

  np.savez_compressed("lmd_seq_len_stats.npz", data=data)
  exit()
  """




  from matplotlib import pyplot as plt
  plt.hist(stat, bins=np.arange(2048), cumulative=True, histtype='step')
  plt.show()
  exit()


  # compute percentage of each barlength
  # python3 build_dataset.py --data_dir /Users/Jeff/DATA/lmd_full --output test.arr --max_size 50000
  stat = compute_stat(beat_length, 10000)
  total = len(stat)
  stat = {str(k):v for k,v in zip(*np.unique(stat,return_counts=True))}
  stat = sorted(stat.items(), key=lambda x : x[1], reverse=True)
  header = ["time_signature", "count", "percentage"]
  data = []
  for k,v in stat:
    data.append( (k, v, np.around(float(v)/total,6)) )
  write_csv(data, header, "STAT_beat_length.csv")
  exit()

  #print(np.quantile(stat, np.linspace(0,1,11)).astype(np.int32))
  #exit()

