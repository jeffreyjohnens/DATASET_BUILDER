FIXED 1. playback crashes when there is a track selected for resample with nothing on it
  I think the issue is db.prune_tracks receives a list of tracks that is out of range. So I fixed the issue there to automatically remove items from the track list that are out of range

FIXED 2. Generation fails on initial stem for resample one track
  This seems to be connected to a faulty bug fix for (1). Accidentally excluded track 0 always.

FIXED 3. move multi_fill.clear() into (!force_valid) section in the encode functions'

FIXED 4. fix the issues with small screen and the top bar by adding width to div containing both of the top menu sections.

FIXED 5. is there a way to get snackbar on top ? My fix was just to explicitly set the iframe height to something reasonable

6. curate a set of stems

7. STARTED make the instructions for the demo