import json
#import dataset_builder_2 as db

"""
e = db.EncoderConfig()
e.force_four_four = True
enc = db.TrackEncoder()
x = enc.midi_to_json("../dataset_builder_2/stems/stem_50.mid",e)

with open("stem_50.json", "w") as f:
  f.write(x)

exit()
"""

GENERAL_MIDI_MAP = {
  "any" : list(range(129)),
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [128]
}

REVERSE_INSTRUMENT_TRACK = {}
for k,v in GENERAL_MIDI_MAP.items():
  if k not in ["no_drums", "any"]:
    for vv in v:
      REVERSE_INSTRUMENT_TRACK[vv] = k

print(REVERSE_INSTRUMENT_TRACK)
exit()


html = '''<script>
var x = '{}';
function printJson(x) {{
  // completely build a midi editor in javascript
  x = JSON.parse(x);
  console.log(x["something"]);



}}
printJson(x);
</script>
'''


import json
from subprocess import call

js = {"something" : [0, 1, 2, 3], "another" : {"two" : 3, "four" : 5}}




# code to test the html that we made ...
with open("test_main.html", "w") as f:
  f.write('''<!DOCTYPE html>
<html>
''')
  f.write(html.format(json.dumps(js)))
  f.write('''<body>
</body>
</html>''')

call("open test_main.html", shell=True)