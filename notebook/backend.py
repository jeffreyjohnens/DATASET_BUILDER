import json

#from google.colab import output
#from IPython.display import display, Javascript, HTML, Audio

import os
import re
import base64
import copy
import time
import glob
import json
import torch
import numpy as np
from subprocess import call
from transformers import *
import dataset_builder_2 as db

from midi2audio import FluidSynth

GENERAL_MIDI_MAP = {
  "any" : list(range(129)),
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [128]
}

REVERSE_INSTRUMENT_TRACK = {}
for k,v in GENERAL_MIDI_MAP.items():
  if k not in ["no_drums", "any"]:
    for vv in v:
      REVERSE_INSTRUMENT_TRACK[vv] = k

GENRE_TAG_MAP = {
  "Disabled" : -1,
  "Blues" : 0,
  "Country" : 1,
  "Electronic" : 2,
  "Folk" : 3,
  "Jazz" : 4,
  "Latin" : 5,
  "Metal" : 6,
  "New Age" : 7,
  "Pop" : 8,
  "Punk" : 9,
  "Rap" : 10,
  "Reggae" : 11,
  "RnB" : 12,
  "Rock" : 13,
  "World" : 14,
  "none" : 15
}

string2tokentype = {
  "PIECE_START": db.TOKEN_TYPE.PIECE_START,
  "NOTE_ONSET": db.TOKEN_TYPE.NOTE_ONSET,
  "NOTE_OFFSET": db.TOKEN_TYPE.NOTE_OFFSET,
  "PITCH": db.TOKEN_TYPE.PITCH,
  "NON_PITCH": db.TOKEN_TYPE.NON_PITCH,
  "VELOCITY": db.TOKEN_TYPE.VELOCITY,
  "TIME_DELTA": db.TOKEN_TYPE.TIME_DELTA,
  "INSTRUMENT": db.TOKEN_TYPE.INSTRUMENT,
  "BAR": db.TOKEN_TYPE.BAR,
  "BAR_END": db.TOKEN_TYPE.BAR_END,
  "TRACK": db.TOKEN_TYPE.TRACK,
  "TRACK_END": db.TOKEN_TYPE.TRACK_END,
  "DRUM_TRACK": db.TOKEN_TYPE.DRUM_TRACK,
  "FILL_IN": db.TOKEN_TYPE.FILL_IN,
  "HEADER": db.TOKEN_TYPE.HEADER,
  "VELOCITY_LEVEL": db.TOKEN_TYPE.VELOCITY_LEVEL,
  "GENRE": db.TOKEN_TYPE.GENRE,
  "DENSITY_LEVEL" : db.TOKEN_TYPE.DENSITY_LEVEL,
}

class Control:
  def __init__(self):
    self.controls = []
  def force_list(self, x):
    from collections import Iterable
    if not isinstance(x, Iterable):
      return [x]
    return x
  def encode(self, encoder, values):
    if values is None:
      return None
    values = {string2tokentype[k]:self.force_list(v) for k,v in values.items()}
    return np.array(encoder.rep.encode_to_one_hot(values)).astype(np.bool)
  def add(self, trigger, mask):
    self.controls.append((trigger,mask))
  def build(self, encoder):
    output = []
    for trigger,mask in self.controls:
      output.append((self.encode(encoder,trigger), self.encode(encoder,mask)))
    return output

#===============================================================================
#===============================================================================

class sampler:

  def __init__(self, ckpt_path=None, name=None, step=None, web=True, force=False):
    if ckpt_path is None:
      if web:
        ckpt_path = self.download_model_from_web(name, step, force=force)
      else:
        ckpt_path = self.download_model(name, step)
    # infer the model cls from the filename
    mstr = re.findall("(gpt2|xl)", ckpt_path)[0]
    if mstr == "gpt2":
      self.model = GPT2LMHeadModel.from_pretrained(ckpt_path, from_tf=False)
    elif mstr == "xl":
      self.model = TransfoXLLMHeadModel.from_pretrained(ckpt_path, from_tf=False)
    else:
      raise NotImplementedError
    # infer encoder from ckpt_path
    estr = re.findall("(TRACK_BAR_FILL_SIXTEEN_ENCODER|TRACK_DENSITY_ENCODER|TRACK_GENRE_ENCODER|TRACK_ENCODER|TRACK_BAR_FILL_ENCODER|TRACK_MONO_POLY_DENSITY_ENCODER)", ckpt_path)[0]
    self.encoder = db.getEncoder(db.getEncoderType(estr))

  def download_model_from_web(self,name,step,force=False):
    file_list = ["config.json", "pytorch_model.bin", "scheduler.pt", "training_args.bin"] # don't need optimizer
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    os.makedirs(ckpt_path, exist_ok=True)
    for file in file_list:
      path = os.path.join(ckpt_path,file)
      if force or not os.path.exists(path):
        call("wget http://www.sfu.ca/~jeffe/{}/{} -O {}".format(ckpt_path,file,path), shell=True)
    return ckpt_path

  def download_model(self, name, step):
    # download a model from compute canada (cedar)
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    model_dir = os.path.dirname(ckpt_path)
    dest_dir = os.path.join(current_dir, model_dir)
    os.makedirs(dest_dir, exist_ok=True)
    if not os.path.exists(ckpt_path):
      call("rsync -a -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/{}/checkpoint-{} {} --progress".format(name,step,dest_dir), shell=True)
    return ckpt_path

  def generate(self, input_ids=None, injects=None, temperature=.9, batch_size=2, verbose=True, global_mask=None):
    import torch.nn.functional as F

    #print("=" * 20)
    
    injects = injects.build(self.encoder) # we only use encoder here
    injects = [copy.deepcopy(injects) for _ in range(batch_size)]
    
    gen_start_time = time.time()

    if input_ids is None:
      input_ids = torch.zeros((batch_size,1), dtype=torch.long)
    else:
      input_ids = torch.from_numpy(input_ids).to(torch.long)
    # must be 2d

    past = None
    finished = np.zeros((batch_size,), dtype=np.bool)
    use_cache = self.model.config.use_cache
    vocab_size = self.model.config.vocab_size
    
    while np.any(finished==False):

      raw_input = input_ids
      inputs = self.model.prepare_inputs_for_generation(raw_input, past, use_cache=use_cache)

      outputs = self.model(**inputs)
      if self.model._use_cache(outputs, use_cache):
        past = outputs[1]
      
      logits = outputs[0][:,-1,:]
      if temperature != 1.0:
        logits = logits / temperature
      
      last_tokens = raw_input[:,-1].detach().numpy()
      masks = torch.zeros((batch_size, vocab_size), dtype=torch.float)
      if injects is not None:
        for i,(last_token,inject) in enumerate(zip(last_tokens,injects)):
          trigger, mask = inject[0]
          if trigger[last_token]:
            if mask is not None:
              assert mask.dtype == np.bool
              masks[i][~mask] = -float("Inf") # set avoided tokens to smallest value
            if len(inject) == 1:
              finished[i] = True
            else:
              inject.pop(0)
      
      if np.any(finished==False):
        if global_mask is not None:
          logits = masks + global_mask + logits
        else:
          logits = masks + logits
        
        probs = F.softmax(logits, dim=-1)
        next_token = torch.multinomial(probs, num_samples=1).squeeze(1)
        next_token[finished] = 0 # if the sequence has ended
        input_ids = torch.cat([input_ids, next_token.unsqueeze(-1)], dim=-1)

        pretty_token = self.encoder.rep.pretty([next_token])[0]
        #if "TRACK" in pretty_token or "BAR" in pretty_token:
        #  print(pretty_token, input_ids.shape[1])
        if verbose:
          print(pretty_token, input_ids.shape[1])
      
      #if input_ids.shape[1] >= 512:
      #  finished[:] = True

    #print(time.time() - gen_start_time)
    input_ids = input_ids.detach().numpy()

    # remove the padding at end of sequences
    output = []
    for seq in input_ids:
      idx = np.where(seq==0)[0]
      if len(idx) > 1:
        output.append( seq[:idx[1]] )
      else:
        output.append( seq )
    return output 

  def generate_bars(self, bars, midi_json, batch_size=1, temperature=.9):
    
    e = db.EncoderConfig()
    e.multi_fill = set(bars)
    e.do_track_shuffle = False

    FILL_START = self.encoder.rep.encode({db.TOKEN_TYPE.FILL_IN : 1})

    # prompt is only up to (and including) the first fill start token
    prompt = np.array(self.encoder.json_to_tokens(json.dumps(midi_json), e))
    prompt = prompt[:np.where(prompt==FILL_START)[0][0] + 1][None,:]

    control = Control()
    for _ in range(len(bars)):
      control.add({"FILL_IN" : 2}, None)

    return self.generate(input_ids=prompt, temperature=temperature, injects=control, batch_size=batch_size)

  def generate_tracks(self, tracks, midi_json=None, batch_size=2, temperature=1., genre_tags_raw=None, mono_poly=None, density=None, global_mask=None):

    # temporary helper for no fill
    c = Control()
    m = c.encode(self.encoder, {"FILL_IN":-1})
    global_mask = np.zeros_like(m).astype(np.float32)
    global_mask[m] = -float("Inf")

    # check that genre tags are valid
    genre_tags = None
    if genre_tags_raw is not None:
      genre_tags = []
      for tag in genre_tags_raw:
        if tag in GENRE_TAG_MAP:
          genre_tags.append(GENRE_TAG_MAP[tag])
        else:
          print("WARNING : UNKNOWN GENRE TAG")
          genre_tags.append(GENRE_TAG_MAP["none"])

    control = Control()

    for track_num, track in enumerate(tracks):

      # convert the track strings to integers
      if isinstance(track,str):
        if track in GENERAL_MIDI_MAP:
          track = GENERAL_MIDI_MAP[track]
        else:
          print("WARNING : UNKNOWN INSTRUMENT TYPE")
          track = GENERAL_MIDI_MAP["any"]
      assert isinstance(track,list)

      track_type = 0 # normal track
      if 128 in track:
        if len(track) == 1:
          track_type = 1 # drum track
        else:
          track_type = -1 # both tracks
      elif mono_poly is not None:
        if mono_poly[track_num] == "poly":
          track_type = 2
    
      # if we are generating from scratch
      if track_num == 0 and midi_json is None:
        if genre_tags is not None:
          control.add({"PIECE_START" : 0}, {"GENRE" : genre_tags[0]})
          control.add({"GENRE" : -1}, {"GENRE" : genre_tags[1]})
          control.add({"GENRE" : -1}, {"TRACK" : track_type})
        else:
          control.add({"PIECE_START" : 0}, {"TRACK" : track_type})
      else:
        control.add({"TRACK_END" : 0}, {"TRACK" : track_type})
      
      # make instrument
      if 128 not in track:
        control.add({"TRACK" : -1}, {"INSTRUMENT" : track})
      
      # control density
      if density is not None:
        control.add({"INSTRUMENT" : -1}, {"DENSITY_LEVEL" : density[track_num]})
      
    control.add({"TRACK_END" : 0}, None)

    #for _ in control.controls:
    #  print(_)
  
    # make the prompt from the encoder here
    # so that calling code doesn't have to know what encoder to use ...
    ec = db.EncoderConfig()
    ec.do_track_shuffle = False
    ec.segment_idx = 0; # always use the first one only
    if genre_tags_raw is not None:
      ec.genre_tags = genre_tags_raw # pass the strings in here

    prompt = None
    if midi_json is not None:
      prompt = self.encoder.json_to_tokens(json.dumps(midi_json),ec)
      #prompt.append( self.encoder.rep.encode({db.TOKEN_TYPE.TRACK:0}) )
      prompt = np.array(prompt)[None,:]

      # add track start to prompt

    #print([(np.where(t),np.where(m)) for t,m in control.build(self.encoder)])
    return self.generate(input_ids=prompt, temperature=temperature, injects=control, batch_size=batch_size, global_mask=global_mask)
  
  def resample_track(self, jstr, track_num, e, temperature=1):
    return
    data = json.loads(jstr)
    inst = data["tracks"][track_num]["instrument"]
    tracks = [[inst]]
    if data["tracks"][track_num]["isDrum"]:
      tracks = ["drums"]

    jstr = db.remove_track(jstr,track_num) # remove it
    input_ids = db.TrackEncoder().json_to_tokens(jstr, e)
    input_ids = np.array(input_ids)[None,:]
    print(input_ids)
    return self.generate_tracks(tracks, input_ids=input_ids, batch_size=1, temperature=temperature)

  def resample_bar(self, jstr, track_num, bar_num, temperature=1):
    return 
    e = db.EncoderConfig()
    e.fill_track = track_num
    e.fill_bar = bar_num
    e.do_track_shuffle = False # don't reshuffle
    enc = db.TrackOneBarFillEncoder()
    FILL_START = enc.rep.encode({db.TOKEN_TYPE.FILL_IN:1})
    input_ids = enc.json_to_tokens(jstr, e)
    input_ids = np.array(input_ids)
    input_ids = input_ids[:np.where(input_ids==FILL_START)[0][0]+1][None,:]
    control = [(enc.rep.encode({db.TOKEN_TYPE.FILL_IN:2}),None)]
    return self.generate(input_ids, temperature=temperature, batch_size=1, injects=control)


# =======================================================================


def generate_callback(status):
  midi_json = get_current_midi()

  # figure out which tracks to generater and which to condition on
  cond_tracks = []
  gen_tracks = []
  gen_insts = []
  gen_density = []
  gen_mono_poly = []
  order = []
  for i,track in enumerate(status["tracks"]):
    tid = int(track["track_id"])
    if track["resample"]:
      gen_tracks.append( tid )
      gen_insts.append( track["inst"] )
      gen_density.append( int(track["density"]) )
      #gen_mono_poly.append( track["mono_poly"] )
      order.append( len(gen_tracks) * -1 )
    else:
      order.append( len(cond_tracks) )
      cond_tracks.append( tid )

  # all values less than zero are generated tracks
  # since they come after conditioned tracks in the output
  # we must add len(cond_tracks) to the value
  order = np.array(order)
  order[order<0] = (order[order<0] * -1) + len(cond_tracks) - 1
  
  if len(gen_tracks) == 0:
    return
  
  temp = float(status["temperature"])

  model = models["track"]
  
  # remove the tracks we are going to generate
  if len(cond_tracks):
    midi_json = json.loads(db.prune_tracks(json.dumps(midi_json), cond_tracks))
  else:
    midi_json = None

  tokens = model.generate_tracks(
      gen_insts, midi_json=midi_json, temperature=temp, batch_size=1, 
      genre_tags_raw=None, mono_poly=None, density=gen_density)[0]

  e = db.EncoderConfig()
  e.do_track_shuffle = False # probably not needed for decode
  model.encoder.tokens_to_midi(tokens, "current.mid", e)
  midi_json = json.loads(model.encoder.tokens_to_json(tokens, e))

  # restore original order of the tracks
  midi_json["tracks"] = [midi_json["tracks"][o] for o in order]
  
  # update the midi
  update_gui_midi(midi_json)

  # save the midi
  save_current_midi(midi_json)


def get_current_midi(*args,**kwargs):
  pass

def save_current_midi(*args,**kwargs):
  pass

def update_gui_midi(*args,**kwargs):
  pass

import glob
import random

paths = glob.glob("/Users/Jeff/DATA/lmd_full/**/*.mid", recursive=True)

models = {}
name = "TRACK_BAR_FILL_SIXTEEN_ENCODER_xl_version3_Jul_29_04_49_False"
step = 100000

count = 0

while count < 10:

  try:

    path = random.choice(paths)
    g = sampler(name=name, step=step)

    ec = db.EncoderConfig()
    ec.num_bars = 16

    raw_json = g.encoder.midi_to_json(path, ec)
    raw_json = db.update_segments(raw_json,ec)
    raw_json = db.select_segment(raw_json, 2, -1)
    g.encoder.json_to_midi(raw_json, "test_input_{}.mid".format(count), ec)

    tokens = g.generate_tracks(
      ["any"], midi_json=json.loads(raw_json), batch_size=1, temperature=1.)[0]

    g.encoder.tokens_to_midi(tokens, "test_output_{}.mid".format(count), ec)
    count += 1
  
  except:
    pass