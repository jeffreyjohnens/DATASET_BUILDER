#rm -rf ../style_machine/model_storage/transformer_model
#cp -r transformer_model ../style_machine/model_storage/transformer_model
#cd ../style_machine/model_storage
#python3 create_interface.py

# add the main model code
rsync -av --progress transformer_modelv3 ../style_machine/model_storage --exclude checkpoints

# add dataset builder 2
rsync -av --progress dataset_builder_2 ../style_machine/model_storage/transformer_modelv2 --exclude '*.arr' --exclude '*.header' --exclude build --exclude dist --exclude dataset_builder_2.egg-info

cd ../style_machine/model_storage
python3 create_interface.py
cd ../../DATASET_BUILDER