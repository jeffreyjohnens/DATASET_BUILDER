cd ~
module load python/3.6
virtualenv --no-download ~/T2T_ENV
source ~/T2T_ENV/bin/activate
pip install --no-index --upgrade pip

pip install tensor2tensor==1.15.2
pip install --no-index tensorflow_cpu==1.15.0
pip install --no-index tensorflow_gpu==1.15.0