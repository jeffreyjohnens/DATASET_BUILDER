import os
import gin
import dataset_builder
import trax
import numpy as np

batch_size = 512
maxlen = 1024
vocab_size = 320

def gen():
  j = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    x = np.array(j.read_batch(batch_size, maxlen, 0))
    yield {"targets" : np.copy(x)}

def gen_eval():
  j = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    x = np.array(j.read_batch(batch_size, maxlen, 2))
    yield {"targets" : x.reshape(batch_size, maxlen, 1, 1)}

#PAD_AMOUNT = 512 * 1024 - len(IDS)
def input_fn_w_padding(n_devices):
  jag = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    inputs = []
    mask = []
    pad_amounts = np.random.choice(PAD_AMOUNT, n_devices)
    x = np.array(jag.read_batch(batch_size, maxlen, 0))
    inputs.append(np.pad(IDS, (pad_amounts[i], PAD_AMOUNT - pad_amounts[i]),
                          mode='constant'))
    mask.append(np.pad(np.ones_like(IDS, dtype=np.float32),
                        (pad_amounts[i], PAD_AMOUNT - pad_amounts[i]),
                        mode='constant'))
    inputs = np.stack(inputs)
    mask = np.stack(mask)
    yield (inputs, inputs, mask)

def input_fn(n_devices):
  jag = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
  while True:
    #x = np.array(jag.read_batch(batch_size, maxlen, 0))
    x = np.random.randint(vocab_size, size=(1,batch_size,maxlen))
    print(x.shape)
    yield (x,x)

# Configure hyperparameters.
gin.parse_config("""
import trax.layers
import trax.models
import trax.optimizers
import trax.supervised.inputs
import trax.supervised.trainer_lib

# Parameters that will vary between experiments:
# ==============================================================================
train.model = @trax.models.ReformerLM
# Our model will have 6 layers, alternating between the LSH attention proposed
# in the Reformer paper and local attention within a certain context window.
n_layers = 6
attn_type = [
  @SelfAttention,
  @LSHSelfAttention,  
  @SelfAttention,
  @LSHSelfAttention,
  @SelfAttention,
  @LSHSelfAttention,
  ]
share_qk = False  # LSH attention ignores this flag and always shares q & k
n_heads = 2
attn_kv = 64
dropout = 0.05
n_tokens = 1024

# Parameters for MultifactorSchedule:
# ==============================================================================
MultifactorSchedule.constant = 0.01
MultifactorSchedule.factors = 'constant * linear_warmup * cosine_decay'
MultifactorSchedule.warmup_steps = 100
MultifactorSchedule.steps_per_cycle = 900

# Parameters for Adam:
# ==============================================================================
Adam.weight_decay_rate=0.0
Adam.b1 = 0.86
Adam.b2 = 0.92
Adam.eps = 1e-9

# Parameters for SelfAttention:
# ==============================================================================
SelfAttention.attention_dropout = 0.05
SelfAttention.chunk_len = 64
SelfAttention.n_chunks_before = 1
SelfAttention.n_parallel_heads = 1

# Parameters for LSHSelfAttention:
# ==============================================================================
LSHSelfAttention.attention_dropout = 0.0
LSHSelfAttention.chunk_len = 64
LSHSelfAttention.n_buckets = [64, 128]
LSHSelfAttention.n_chunks_after = 0
LSHSelfAttention.n_chunks_before = 1
LSHSelfAttention.n_hashes = 1
LSHSelfAttention.n_parallel_heads = 1
LSHSelfAttention.predict_drop_len = 128
LSHSelfAttention.predict_mem_len = 1024

# Parameters for ReformerLM:
# ==============================================================================
ReformerLM.attention_type = %attn_type
ReformerLM.d_attention_key = %attn_kv
ReformerLM.d_attention_value = %attn_kv
ReformerLM.d_model = 256
ReformerLM.d_ff = 512
ReformerLM.dropout = %dropout
ReformerLM.ff_activation = @trax.layers.Relu
ReformerLM.max_len = %n_tokens
ReformerLM.mode = 'train'
ReformerLM.n_heads = %n_heads
ReformerLM.n_layers = %n_layers
ReformerLM.vocab_size = 320
ReformerLM.share_qk = %share_qk
ReformerLM.axial_pos_shape = (512, 1024)
ReformerLM.d_axial_pos_embs= (64, 192)
""")


output_dir = os.path.expanduser('~/train_reformer/')
#!rm -f ~/train_dir/model.pkl  # Remove old model
trainer = trax.supervised.Trainer(
  model=trax.models.ReformerLM,
  loss_fn=trax.layers.CrossEntropyLoss,
  optimizer=trax.optimizers.Adam,
  lr_schedule=trax.lr.MultifactorSchedule,
  inputs=trax.supervised.inputs.Inputs(input_fn),
  output_dir=output_dir,
  has_weights=True)

trainer.train_epoch(n_steps=1, n_eval_steps=1)