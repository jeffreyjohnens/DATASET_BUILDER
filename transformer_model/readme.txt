1. install the dataset_builder python module I made to parse midi files. Go to the dataset_builder folder and run python setup.py install.
2. install this version of trax
pip install --upgrade -q gin git+https://github.com/google/trax.git@v1.2.3
3. install the packages listed in requirements