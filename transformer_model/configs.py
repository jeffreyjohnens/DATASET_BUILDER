config_map = {
  "tran_defaults" : ["""
    import trax.layers
    import trax.models
    import trax.optimizers
    import trax.supervised.inputs
    import trax.supervised.trainer_lib

    train.model = @trax.models.TransformerLM
    TransformerLM.vocab_size = %vocab_size
    TransformerLM.d_model = 512
    TransformerLM.d_ff = 2048
    TransformerLM.n_layers = 6
    TransformerLM.n_heads = 8
    TransformerLM.dropout = 0.1
    TransformerLM.max_len = 2048

    # Parameters for MultifactorSchedule:
    # ===========================================================
    MultifactorSchedule.constant = 0.0001
    MultifactorSchedule.factors = 'constant'

    # Parameters for Adam:
    # ===========================================================
    Adam.weight_decay_rate=0.0
    Adam.b1 = 0.9
    Adam.b2 = 0.9999
    Adam.eps = 1e-5
    """],
  "tran_tiny" : ["""
    import trax.layers
    import trax.models
    import trax.optimizers
    import trax.supervised.inputs
    import trax.supervised.trainer_lib

    train.model = @trax.models.TransformerLM
    TransformerLM.vocab_size = %vocab_size
    TransformerLM.d_model = 512
    TransformerLM.d_ff = 512
    TransformerLM.n_layers = 2
    TransformerLM.n_heads = 2
    TransformerLM.dropout = 0.1
    TransformerLM.max_len = 512

    # Parameters for MultifactorSchedule:
    # ===========================================================
    MultifactorSchedule.constant = 0.0001
    MultifactorSchedule.factors = 'constant'

    # Parameters for Adam:
    # ===========================================================
    Adam.weight_decay_rate=0.0
    Adam.b1 = 0.9
    Adam.b2 = 0.9999
    Adam.eps = 1e-5
    """],
  "vanilla_rnn" : ["""
    import trax.layers
    import trax.models
    import trax.optimizers
    import trax.supervised.inputs
    import trax.supervised.trainer_lib

    train.model = @trax.models.RNNLM
    RNNLM.vocab_size = %vocab_size
    RNNLM.d_model=512
    RNNLM.n_layers=2

    # Parameters for MultifactorSchedule:
    # ==========================================================
    MultifactorSchedule.constant = 0.00001
    MultifactorSchedule.factors = 'constant'

    # Parameters for Adam:
    # ==========================================================
    Adam.weight_decay_rate=0.0
    Adam.b1 = 0.9
    Adam.b2 = 0.9999
    Adam.eps = 1e-5
  """]
}