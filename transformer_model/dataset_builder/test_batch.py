from dataset_builder import Jagged
import numpy as np
from tqdm import tqdm

j = Jagged("trash.arr", True, 0)
for i in range(10,30):
	j.append(list(range(1,i)),0)
j.flush()

j = Jagged("trash.arr", False, 0)
batch, mask = j.read_batch(10,20,0)
print(np.array(batch))
print(np.array(mask))

"""
jag = dataset_builder.Jagged("/home/jeffe/scratch/data.arr", False, 0)
#jag = dataset_builder.Jagged("data.arr", False, 0)

for i in tqdm(range(10000)):
	batch = np.array(jag.read_batch(128,2048,0))
	assert(np.all(batch < 44644))
	assert(np.all(batch >= 0))

	batch = np.array(jag.read_batch(128,2048,1))
	assert(np.all(batch < 44644))
	assert(np.all(batch >= 0))

	batch = np.array(jag.read_batch(128,2048,2))
	assert(np.all(batch < 44644))
	assert(np.all(batch >= 0))
"""