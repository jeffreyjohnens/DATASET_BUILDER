// efficiently store a large amount of data
// we compress each sequence seperately

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "lz4.h"

using namespace std;

class Jagged {
public:
  Jagged(string &filepath_, bool write_=true) {
    filepath = filepath_;
    write = write_;
    metadata_size = 3*sizeof(size_t); // (s,e,size_uncompressed)
    header_filepath = filepath + ".header";
    if (write) {
      fs.open(filepath, ios::out | ios::binary);
      header_fs.open(header_filepath, ios::out | ios::binary);
    }
    else {
      fs.open(filepath, ios::in | ios::binary);
      header_fs.open(header_filepath, ios::in | ios::binary);
      size = header_fs.tellg()/metadata_size;
    }
  }

  void append(vector<int> &arr) {
    if (write) {
      size_t start = fs.tellp();
      //fs.write((char*)arr.data(), sizeof(int)*arr.size());
      // begin compress
      size_t src_size = sizeof(int)*arr.size();
      size_t dst_capacity = LZ4_compressBound(src_size);
      char* dst = new char[dst_capacity];
      size_t dst_size = LZ4_compress_default(
        (char*)arr.data(), dst, src_size, dst_capacity);
      fs.write(dst, dst_size);
      delete[] dst;
      // end compress
      size_t end = fs.tellp();
      header_fs.write((char*)&start, sizeof(size_t));
      header_fs.write((char*)&end, sizeof(size_t));
      header_fs.write((char*)&src_size, sizeof(size_t));
    }
  }

  vector<int> read(size_t index) {
    assert(index < size);
    size_t metadata[3]; // same as metadata size
    header_fs.seekg(index*metadata_size);
    header_fs.read((char*)&metadata, metadata_size);

    size_t compressed_size = (metadata[1]-metadata[0]);
    char* src = new char[compressed_size/sizeof(char)];
    fs.seekg(metadata[0]);
    fs.read(src, compressed_size);
    vector<int> x(metadata[2]/sizeof(int));
    LZ4_decompress_safe(src, (char*)x.data(), compressed_size, metadata[2]);
    delete[] src;
    
    return x;
  }

  vector<vector<int>> read_batch(vector<int> &indices, int max_len) {
    vector<vector<int>> batch;
    for (const auto index : indices) {
      augment_read(index);
    }
  }

  void flush() {
    fs.flush();
    header_fs.flush();
  }
  
private:
  string filepath;
  string header_filepath;
  fstream fs;
  fstream header_fs;
  size_t size;
  bool write;
  size_t metadata_size;
};

/*
int main() {

  string s("test.arr");
  Jagged j(s);
  for (int i=1; i<10; i++) {
    vector<int> vec(i,i+1);
    j.append(vec);
  }
  j.flush();

  Jagged j2(s, false);
  for (const auto it : j2.read(20)) {
    std::cout << it << std::endl;
  }
}
*/
