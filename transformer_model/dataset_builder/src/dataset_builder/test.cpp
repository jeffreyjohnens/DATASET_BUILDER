#include <iostream>
#include <future>
#include <chrono>
#include <set>
#include <tuple>
#include <assert.h>
#include <math.h>
#include <vector>
#include <string>
#include <fstream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
namespace py = pybind11;
using namespace std;

#include "../../midifile/include/MidiFile.h"
#include "lz4.h"

// =============================================================
// tokens

static const int N_VELOCITY_BINS = 32;
static const int N_INST = 16;
static const int MIN_PITCH = 21;
static const int MAX_PITCH = 108;
static const int N_PITCH = MAX_PITCH - MIN_PITCH;
static const int MAX_SHIFT_STEPS = 100;
static const int MUSENET_VOCAB_SIZE = N_INST * N_VELOCITY_BINS * N_PITCH + MAX_SHIFT_STEPS; //44644

/*
static const int MUSENET_VELOCITY_MAP[128] = {0,1,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,22,22,22,22,23,23,23,23,24,24,24,24,24,25,25,25,25,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,29,30,30,30,30,31,31,31};
*/

static const int MUSENET_VELOCITY_MAP[128] = {0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

static const int PERFORMANCE_VELOCITY_MAP[128] = {0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,22,22,22,22,23,23,23,23,24,24,24,24,25,25,25,25,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,29,30,30,30,30,31,31,31};

static const double TIME_STRETCH_FACTORS[5] = {.95,.975,1.0,1.025,1.05};
static const int PITCH_SHIFTS[7] = {-3,-2,-1,0,1,2,3};

class TOKEN {
public:
  TOKEN(vector<int> domain_) {
    cp.push_back( 1 );
    for (const auto d : domain_) {
      domain.push_back( d );
      cp.push_back( cp.back() * d );
    }
  }
  int encode(vector<int> values) {
    assert(values.size() <= domain.size());
    int token = 0;
    for (int i=0; i<(int)values.size(); i++) {
      token += cp[i] * values[i];
    }
    return token;
  }
  int decode(int token, int index) {
    assert(index<(int)domain.size());
    return (token / cp[index]) % domain[index];
  }
  int shift(int token, int index, int shift) {
    assert(index<(int)domain.size());
    int shifted = token + (shift * cp[index]);
    int dec = decode(token, index) + shift;
    if ((dec<0)||(dec>=domain[index])||(shifted<0)||(shifted>=cp.back())) {
      throw 2;
    }
    return shifted;
  }
  int max_token() {
    return cp.back();
  }
  vector<int> domain;
  vector<int> cp;
};

class REPRESENTATION {
public:
  REPRESENTATION(vector<vector<int>> domains) {
    int count = 0;
    for (int i=0; i<(int)domains.size(); i++) {
      TOKEN tok(domains[i]);
      toks.push_back(tok);
      starts.push_back(count);
      count += tok.max_token();
      ends.push_back(count);
      //cout << starts.back() << " - " << ends.back() << endl;
    }
  }
  int decode(int token, int tindex, int index) {
    //cout << starts[tindex] << " " << ends[tindex] << endl;
    if ((token >= starts[tindex]) && (token < ends[tindex])) {
      return toks[tindex].decode(token - starts[tindex], index);
    }
    return -1;
  }
  int encode(vector<int> values, int tindex) {
    return toks[tindex].encode(values) + starts[tindex];
  }
  int shift(int token, int tindex, int index, int shift) {
    if ((token >= starts[tindex]) && (token < ends[tindex])) {
      return toks[tindex].shift(token - starts[tindex], index, shift) + starts[tindex];
    }
    return token;
  }
  vector<TOKEN> toks;
  vector<int> starts;
  vector<int> ends;
};


// =============================================================
static const int DRUM_CHANNEL = 9;

#define QUIET_CALL(noisy) { \
    cout.setstate(ios_base::failbit);\
    cerr.setstate(ios_base::failbit);\
    (noisy);\
    cout.clear();\
    cerr.clear();\
}

using Note = tuple<int,int,int,int,int>; // (ONSET,PITCH,DURATION,VELOCITY,INST)
using Event = tuple<int,int,int,int>; // (TIME,VELOCITY,PITCH,INSTRUMENT) 

int quantize_beat(double x, double TPQ, double SPQ, double cut=.5) {
  return (int)((x / TPQ * SPQ) + (1.-cut)) * (TPQ / SPQ);
}

int quantize_second(double x, double spq, double ticks, double steps_per_second, double cut=.5) {
  return (int)((x / ticks * spq * steps_per_second) + (1.-cut));
}

set<Event> parse(string filepath, int steps_per_unit, bool quantize_by_beat=true) {
  smf::MidiFile midifile;
  QUIET_CALL(midifile.read(filepath));

  set<Event> events;
  int pitch, time, velocity, channel;
  int track_count = midifile.getTrackCount();
  vector<int> instruments(16,0);

  for (int track=0; track<track_count; track++) {
    fill(instruments.begin(), instruments.end(), 0); // zero instruments
    for (int event=0; event<midifile[track].size(); event++) {

      if ((midifile[track][event].isNoteOn()) || (midifile[track][event].isNoteOff())) {

        pitch = (int)midifile[track][event][1];
        time = midifile.getTimeInSeconds(track,event) * steps_per_unit;
        velocity = (int)midifile[track][event][2];
        channel = midifile[track][event].getChannelNibble();

        // determine a way to figure out drums ...
        if (channel != DRUM_CHANNEL) {
          events.insert(make_tuple(time,velocity,pitch,instruments[channel]));
        }
      }
      if (midifile[track][event].isPatchChange()) {
        channel = midifile[track][event].getChannelNibble();
        instruments[channel] = (int)midifile[track][event][1];
      }      
    }
  }
  return events;
}

// new function to parse with measures
// split by track and channel
int get_bar_number(int tick, int ticks_per_quarter, map<int,tuple<int,int>> &timesigs, bool is_offset) {
  auto it = timesigs.lower_bound(tick);
  if (timesigs.size() > 0) {
    if (it != timesigs.begin()) {
      it = prev(it);
    }
    if ((get<0>(it->second) == 4) && (get<1>(it->second) == 4)) {
      //cout << tick << " <-- " << it->first << endl;
      int bar = (tick - it->first) / (ticks_per_quarter * 4);
      if ((is_offset) && (tick % (ticks_per_quarter * 4) == 0)) {
        return bar - 1;
      }
      return bar;
    }
  }
  return -1;
}

vector<vector<vector<Event>>> parse_new(string filepath, int steps_per_unit) {
  smf::MidiFile midifile;
  QUIET_CALL(midifile.read(filepath));
  midifile.makeAbsoluteTicks();
  midifile.linkNotePairs();

  set<Event> events;
  bool is_offset;
  int pitch, time, velocity, channel, current_tick, bar, voice;
  int start, end, start_bar, end_bar, rel_start, rel_end;
  int track_count = midifile.getTrackCount();
  int TPQ = midifile.getTPQ();
  vector<int> instruments(16,0);

  smf::MidiEvent *mevent;

  int resolution = 4;
  
  // get time signature and track/channel info
  int max_tick = 0;
  map<tuple<int,int>,int> track_map;
  map<int, tuple<int,int,int>> timesigs;

  for (int track=0; track<track_count; track++) {
    for (int event=0; event<midifile[track].size(); event++) { 
      mevent = &(midifile[track][event]);
      if (mevent->isTimeSignature()) {
        int barlength = (TPQ * 4 * (*mevent)[3]) / (1<<(*mevent)[4]);
        timesigs[mevent->tick] = make_tuple(
          (*mevent)[3], 1<<(*mevent)[4], barlength);
      }
      if ((mevent->isNoteOn()) || (mevent->isNoteOff())) {
        channel = mevent->getChannelNibble();
        if (track_map.find(make_pair(track,channel)) == track_map.end()) {
          track_map[make_pair(track,channel)] = track_map.size();
        }
        max_tick = max(max_tick, mevent->tick);
      }
    }
  }
  timesigs[max_tick] = make_tuple(0,0,0);

  map<int,int> barlines;

  if (timesigs.size() >= 2) {
    // must have atleast one time signature to compute bar lines
    int barlength;
    auto it = timesigs.begin();
    while (it != timesigs.rbegin().base()) {
      barlength = get<2>(it->second);
      for (int i=it->first; i<next(it)->first; i=i+barlength) {
        //cout << i << " (" << barlines.size() << ")" << endl;
        barlines[i] = barlines.size();
      }
      it++;
    }
    // add last barline
    barlines[timesigs.rbegin()->first + barlength] = barlines.size();
  }
  
  vector<vector<vector<Event>>> output(track_map.size(), vector<vector<Event>>(barlines.size()));

  for (int track=0; track<track_count; track++) {
    fill(instruments.begin(), instruments.end(), 0); // zero instruments
    for (int event=0; event<midifile[track].size(); event++) {
      mevent = &(midifile[track][event]);

      if ((mevent->isNoteOn()) && (mevent->isLinked())) {
        pitch = (*mevent)[1];
        velocity = (*mevent)[2];
        channel = mevent->getChannelNibble();
        voice = track_map.find(make_pair(track,channel))->second;

        start = quantize_beat(mevent->tick, TPQ, resolution);
        end = quantize_beat(mevent->getLinkedEvent()->tick, TPQ, resolution);

        auto start_bar_it = barlines.lower_bound(start);
        auto end_bar_it = barlines.lower_bound(end);
        if (start_bar_it->first > start) {
          start_bar_it--;
        }
        if (end_bar_it->first >= end) {
          end_bar_it--;
        }

        start_bar = start_bar_it->second;
        end_bar = end_bar_it->second;

        rel_start = ((start - start_bar_it->first) * resolution) / TPQ;
        rel_end = ((end - end_bar_it->first) * resolution) / TPQ;

        if ((channel != DRUM_CHANNEL) && (start_bar >= 0) && (end_bar >= 0)) {
          output[voice][start_bar].push_back(
            make_tuple(rel_start,velocity,pitch,start_bar_it->first));
          output[voice][end_bar].push_back(
            make_tuple(rel_end,0,pitch,end_bar_it->first));
        }
      }

      if (midifile[track][event].isPatchChange()) {
        channel = midifile[track][event].getChannelNibble();
        instruments[channel] = (int)midifile[track][event][1];
      }      
    }
  }

  // sort each bar
  for (int i=0; i<output.size(); i++) {
    for (int j=0; j<output[i].size(); j++) {
      sort(output[i][j].begin(), output[i][j].end());
    }
  }

  /*
  // print bar 0 1
  int i = 0;
  int j = 1;
  sort(output[i][j].begin(), output[i][j].end());
  for (int k=0; k<output[i][j].size(); k++) {
    cout << get<0>(output[i][j][k]) << " "
      << get<1>(output[i][j][k]) << " "
      << get<2>(output[i][j][k]) << " "
      << get<3>(output[i][j][k]) << endl;
  }
  */

  return output;
}

/*
vector<int> count_instruments(string filepath) {
  set<Note> notes = parse(filepath, 0, false);
  vector<int> counts(128,0);
  for (const auto note : notes) {
    counts[get<4>(note)/8]++;
  }
  return counts;
}
*/

class ENCODER {
public:
  virtual vector<int> convert(set<Event> &events, double time_stretch, int pitch_shift) {}
  virtual vector<Event> revert(vector<int> &tokens) {}

  REPRESENTATION *rep;
  int pitch_index;
  int pitch_tindex;
  
  int n_time_tokens;
  int min_pitch;
  int max_pitch;
};

class MUSENET : public ENCODER {
public:
  MUSENET(int min_pitch_, int max_pitch_, int n_time_tokens_, int use_velocity_) {
    n_time_tokens = n_time_tokens_;
    min_pitch = min_pitch_;
    max_pitch = max_pitch_;
    int n_velocity_tokens = 2; //max(32*use_velocity_,);
    rep = new REPRESENTATION(
      {{2},{16,n_velocity_tokens,max_pitch-min_pitch},{n_time_tokens}});
    pitch_tindex = 1;
    pitch_index = 2;
    use_velocity = use_velocity_;
  }
  vector<int> convert(set<Event> &events, double time_stretch=1., int pitch_shift=0) {

    int current_step = 0;
    int step, inst, velocity, pitch, velocity_bin;
    vector<int> tokens(2,0);
    vector<int> empty;

    // keep track of the min pitch and max pitch within a sequence
    // so that pitch shifting is easy
    tokens[0] = get<2>(*events.begin());
    tokens[1] = tokens[0];

    tokens.push_back( rep->encode({0},0) );

    for (const auto event : events) {
      step = (int)((double)get<0>(event) * time_stretch);
      velocity = get<1>(event);
      pitch = get<2>(event) + pitch_shift - min_pitch;
      inst = get<3>(event) / 8; // put into groups

      if ((pitch >= 0) && (pitch < (max_pitch - min_pitch))) {
        tokens[0] = min(tokens[0], pitch);
        tokens[1] = max(tokens[1], pitch);

        if (step > current_step) {
          while (step > current_step + n_time_tokens) {
            tokens.push_back( rep->encode({n_time_tokens-1},2) );
            current_step += n_time_tokens;
          }
          if (step > current_step) {
            tokens.push_back( rep->encode({step-current_step-1},2) );
          }
          current_step = step;
        }
        
        velocity_bin = MUSENET_VELOCITY_MAP[velocity] * use_velocity;
        tokens.push_back( rep->encode({inst,velocity_bin,pitch},1) ); 
      }
    }
    tokens.push_back( rep->encode({1},0) );
    return tokens;
  }
  // make a function that reverts to a sequence of events
  vector<Event> revert(vector<int> &tokens) {
    int current_step = 0;
    vector<Event> events;
    map<tuple<int,int>,tuple<int,int>> active_notes;
    for (const auto token : tokens) {
      if (rep->decode(token,2,0) >= 0) {
        current_step += (rep->decode(token,2,0) + 1);
      }
      else if (rep->decode(token,1,0) >= 0) {
        int inst = rep->decode(token,1,0) * 8; // lossy
        int vel = rep->decode(token,1,1) * 100; // lossy
        int pitch = rep->decode(token,1,2) + min_pitch;

        events.push_back(make_tuple(current_step,vel,pitch,inst));
      }
    }
    return events;
  }
  int use_velocity;
};

/*
vector<vector<int>> musenet_w_aug(string &filepath) {
  vector<vector<int>> seqs;
  set<Event> events = parse(filepath, MAX_SHIFT_STEPS, false);
  for (int i=0; i<5; i++) {
    seqs.push_back( musenet(events, TIME_STRETCH_FACTORS[i], 0) );
  }
  return seqs;
}
*/

enum ENCODER_MODES {
  MUSENET_DEFAULT,
  MUSENET_NO_VELOCITY,
};

MUSENET md(0,128,MAX_SHIFT_STEPS,1);
MUSENET mnv(0,128,MAX_SHIFT_STEPS,0);
map<ENCODER_MODES,ENCODER*> encoding_map = {
  {MUSENET_DEFAULT,&md},
  {MUSENET_NO_VELOCITY,&mnv}
};


map<ENCODER_MODES,int> get_encoding_sizes() {
  map<ENCODER_MODES,int> encoding_sizes;
  for (const auto kv : encoding_map) {
    encoding_sizes[kv.first] = kv.second->rep->ends.back();
  }
  return encoding_sizes;
}

vector<vector<int>> encode(string &midi_path, ENCODER_MODES encoder_mode, bool stretch) {
  vector<vector<int>> seqs;
  set<Event> events = parse(midi_path, MAX_SHIFT_STEPS, false);
  if (stretch) {
    for (int i=0; i<5; i++) {
      seqs.push_back( 
        encoding_map[encoder_mode]->convert(events, TIME_STRETCH_FACTORS[i], 0) );
    }
  }
  else {
    seqs.push_back( encoding_map[encoder_mode]->convert(events, 1, 0) );
  }
  return seqs;
}

void decode(vector<int> &tokens, ENCODER_MODES encoder_mode, string path) {
  vector<Event> events = encoding_map[encoder_mode]->revert(tokens);
  //set<Event> events = parse(midi_path, 100);

  smf::MidiFile outputfile;
  outputfile.absoluteTicks();
  outputfile.setTicksPerQuarterNote(100);
  outputfile.addTempo(0, 0, 60);

  // make channels correct instruments
  int current_track = 0;
  map<int,int> inst2chan;
  for (const auto event : events) {
    int inst = get<3>(event);
    auto it = inst2chan.find(inst);
    if (it == inst2chan.end()) {
      inst2chan[inst] = current_track % 16; // don't allow overflow
      if (current_track < 16) {
        outputfile.addPatchChange(current_track, 0, current_track, inst);
        outputfile.addTrack(1);
      }
      current_track++;
    }
    // skip the drum channel
    if (current_track == DRUM_CHANNEL) {
      current_track++;
      outputfile.addTrack(1);
    }
  }

  int current_step = 0;
  for (const auto event : events) {
    outputfile.addNoteOn(
      inst2chan[get<3>(event)], 
      get<0>(event), // time
      inst2chan[get<3>(event)], 
      get<2>(event), // pitch
      get<1>(event)); // velocity
  }
  outputfile.sortTracks();         // make sure data is in correct order
  outputfile.write(path.c_str()); // write Standard MIDI File twinkle.mid
}

// =============================================================
// storage



class Jagged {
public:
  Jagged(string &filepath_, ENCODER_MODES encoder_mode_, bool write_=true) {
    filepath = filepath_;
    write = write_;
    metadata_size = 4*sizeof(size_t); // (s,e,size_uncompressed,split_id)
    header_filepath = filepath + ".header";
    encoder_mode = encoder_mode_;
    enc = encoding_map[encoder_mode_];

    if (write) {
      enable_write();
    }
    else {
      enable_read();
    }
  }

  void enable_write() {
    // we don't want this to happen if the file has a size
    write = true;
    fs.flush();
    header_fs.flush();
    fs.close();
    header_fs.close();
    
    fs.open(filepath, ios::out | ios::binary);
    header_fs.open(header_filepath, ios::out | ios::binary);
    size = 0;
  }

  void enable_read() {
    write = false;
    fs.flush();
    header_fs.flush();
    fs.close();
    header_fs.close();
    
    fs.open(filepath, ios::in | ios::binary);
    header_fs.open(header_filepath, ios::in | ios::binary);
    header_fs.seekg(0, header_fs.end);
    size = header_fs.tellg()/metadata_size;
    header_fs.seekg(0, header_fs.beg);

    // load the metadata into memory ...
    // split by id (train=0, test=1, valid=2)
    size_t meta[4];
    ptr[0] = &train_meta;
    ptr[1] = &test_meta;
    ptr[2] = &valid_meta;
    
    for (size_t i=0; i<size; i++) {
      header_fs.read((char*)meta, metadata_size);
      ptr[meta[3]]->push_back(make_tuple(meta[0],meta[1],meta[2]));
    }
    header_fs.close(); // no longer needed

    //std::cout << ptr[0]->size() << " " << ptr[1]->size() << " " << ptr[2]->size() << std::endl;
  }

  void append(vector<int> &arr, size_t split_id) {
    if (!write) { return; }

    // encode right here ...
    // only add if successful parse with notes ...
    //set<Event> events = parse(midi_path, 100, false);
    //vector<int> arr = enc->convert(events, 1., 0);
    if (arr.size() >= 512) {
      size_t start = fs.tellp();
      // begin compress ===============================
      size_t src_size = sizeof(int)*arr.size();
      size_t dst_capacity = LZ4_compressBound(src_size);
      char* dst = new char[dst_capacity];
      size_t dst_size = LZ4_compress_default(
        (char*)arr.data(), dst, src_size, dst_capacity);
      fs.write(dst, dst_size);
      delete[] dst;
      // end compress =================================
      size_t end = fs.tellp();
      header_fs.write((char*)&start, sizeof(size_t));
      header_fs.write((char*)&end, sizeof(size_t));
      header_fs.write((char*)&src_size, sizeof(size_t));
      header_fs.write((char*)&split_id, sizeof(size_t));
      size++;
    }
  }

  vector<int> read(size_t index, size_t sid) {
    if (index >= ptr[sid]->size()) {
      std::cout << "ERROR : index exceeds dataset size" << std::endl;
      throw(1);
    }
    size_t csize = (get<1>((*ptr[sid])[index]) - get<0>((*ptr[sid])[index]));
    char* src = new char[csize/sizeof(char)];
    fs.seekg(get<0>((*ptr[sid])[index]));
    fs.read(src, csize);
    vector<int> x(get<2>((*ptr[sid])[index])/sizeof(int));
    LZ4_decompress_safe(src,(char*)x.data(),csize,get<2>((*ptr[sid])[index]));
    delete[] src;
    
    return x;
  }

  void crop(int *src, int n_tokens, vector<int> &dest, vector<int> &mask,  int pitch_shift) {
    // crop or pad sequences to max_len
    int max_len = dest.size();
    int pad_offset = 0;
    int token_offset = 0;
    if (n_tokens < max_len) {
      pad_offset = rand() % (max_len - n_tokens + 1);
    }
    else if (n_tokens > max_len) {
      token_offset = rand() % (n_tokens - max_len + 1);
    }

    for (int i=0; i<min(max_len,n_tokens); i++) {
      dest[pad_offset+i] = enc->rep->shift(
        src[token_offset+i], enc->pitch_tindex, enc->pitch_index, pitch_shift);
      mask[pad_offset+i] = 1;
    }
  }

  tuple<vector<vector<int>>,vector<vector<int>>> read_batch(int batch_size, int max_len, size_t sid) {
    srand(time(NULL));
    vector<vector<int>> batch(batch_size, vector<int>(max_len,0));
    vector<vector<int>> mask(batch_size, vector<int>(max_len,0));
    int max_pitch = enc->rep->toks[enc->pitch_tindex].domain[enc->pitch_index];
    for (int i=0; i<batch_size; i++) {
      int index = rand() % ptr[sid]->size();
      vector<int> tokens = read(index,sid); 
      // tokens[0] is min_pitch
      // tokens[1] is max_pitch

      vector<int> valid;
      for (int t=-6; t<6; t++) {
        if ((tokens[0] + t >= 0) && (tokens[1] + t < max_pitch)) {
          valid.push_back(t);
        }
      }
      
      // don't pass in the first two metadata in tokens
      int rshift = valid[rand() % valid.size()];
      crop(tokens.data()+2, (int)tokens.size()-2, batch[i], mask[i], rshift);
    }
    return make_pair(batch,mask);
  }

  void flush() {
    fs.flush();
    header_fs.flush();
  }

  int get_size() {
    return size;
  }

  size_t size;
  
private:
  string filepath;
  string header_filepath;
  fstream fs;
  fstream header_fs;
  bool write;
  size_t metadata_size;
  ENCODER_MODES encoder_mode;
  ENCODER *enc;

  vector<tuple<size_t,size_t,size_t>> train_meta;
  vector<tuple<size_t,size_t,size_t>> test_meta;
  vector<tuple<size_t,size_t,size_t>> valid_meta;
  vector<tuple<size_t,size_t,size_t>> *ptr[3];
};

// =============================================================
// build a dataset

/*
#include <thread>
#include "tqdm.h"

void worker(string *paths, int n, Jagged* writer, tqdm *bar, int total) {
  for (int i=0; i<n; i++) {
    vector<int> tokens = musenet(paths[i]);
    writer->append( tokens );
    bar->progress(writer->size, total);
  }
}

void builder(vector<string> &paths, string &dst_path, int n_threads=8) {
  int chunk = paths.size() / n_threads;
  int c = 0;
  Jagged writer(dst_path, true);
  tqdm bar;
  vector<thread> threads;
  for (int i=0; i<n_threads; i++) {
    c = chunk + ((int)(i==0) * (paths.size() % chunk));
    thread t(worker, paths.data()+chunk*i, c, &writer, &bar, paths.size());
    threads.push_back(std::move(t));
  }
  for (int i=0; i<n_threads; i++) {
    threads[i].join();
  }
}
*/




PYBIND11_MODULE(dataset_builder,m) {
  m.def("parse", &parse);
  m.def("encode", &encode);
  m.def("decode", &decode);
  //m.def("count_instruments", &count_instruments);
  //m.def("performance", &performance);
  //m.def("musenet", &musenet);
  //m.def("musenet_w_aug", &musenet_w_aug);

  m.def("parse_new", &parse_new);

  m.def("get_encoding_sizes", &get_encoding_sizes);

  py::enum_<ENCODER_MODES>(m, "ENCODER_MODES", py::arithmetic())
    .value("MUSENET_DEFAULT", ENCODER_MODES::MUSENET_DEFAULT)
    .value("MUSENET_NO_VELOCITY", ENCODER_MODES::MUSENET_NO_VELOCITY)
    .export_values();

  py::class_<Jagged>(m, "Jagged")
    .def(py::init<string &, ENCODER_MODES, bool>())
    .def("enable_write", &Jagged::enable_write)
    .def("enable_read", &Jagged::enable_read)
    .def("append", &Jagged::append)
    .def("read", &Jagged::read)
    .def("read_batch", &Jagged::read_batch)
    .def("flush", &Jagged::flush)
    .def("get_size", &Jagged::get_size);

}