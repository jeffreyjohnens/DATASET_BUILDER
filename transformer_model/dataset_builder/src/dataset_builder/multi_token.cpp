#include <iostream>
#include <array>
#include <vector>
#include <set>
#include <map>

using namespace std;

class TOKEN {
public:
  TOKEN(vector<int> domain_) {
    cp.push_back( 1 );
    for (const auto d : domain_) {
      domain.push_back( d );
      cp.push_back( cp.back() * d );
    }
  }
  int encode(vector<int> values) {
    assert(values.size() <= domain.size());
    int token = 0;
    for (int i=0; i<(int)values.size(); i++) {
      token += cp[i] * values[i];
    }
    return token;
  }
  int decode(int token, int index) {
    assert(index<6);
    return (token / cp[index]) % domain[index];
  }
  int shift(int token, int index, int shift) {
    assert(index<6);
    int shifted = token + (shift * cp[index]);
    int dec = decode(token, index) + shift;
    if ((dec<0)||(dec>=domain[index])||(shifted<0)||(shifted>=cp.back())) {
      throw 2;
    }
    return shifted;
  }
  int max_token() {
    return cp.back();
  }
  vector<int> domain;
  vector<int> cp;
};

class REPRESENTATION {
public:
  REPRESENTATION(vector<vector<int>> domains) {
    int count = 0;
    for (int i=0; i<(int)domains.size(); i++) {
      TOKEN tok(domains[i]);
      toks.push_back(tok);
      starts.push_back(count);
      count += tok.max_token();
      ends.push_back(count);
    }
  }
  int decode(int token, int tindex, int index) {
    return toks[tindex].decode(token - starts[tindex], index);
  }
  int encode(vector<int> values, int tindex) {
    return toks[tindex].encode(values) + starts[tindex];
  }
  int shift(int token, int tindex, int index, int shift) {
    if ((token >= starts[tindex]) && (token < ends[tindex])) {
      return toks[tindex].shift(token - starts[tindex], index, shift) + starts[tindex];
    }
    return token;
  }
  vector<TOKEN> toks;
  vector<int> starts;
  vector<int> ends;
};

static const int MUSENET_VELOCITY_MAP[128] = {0,1,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,22,22,22,22,23,23,23,23,24,24,24,24,24,25,25,25,25,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,29,30,30,30,30,31,31,31};

using Event = tuple<int,int,int,int>; // (TIME,VELOCITY,PITCH,INSTRUMENT) 

// we pass in a representation map ...
// that maps enum to token_index, index
class ENCODER {
public:
  virtual vector<int> convert(set<Event> &events, double time_stretch, int pitch_shift) {}
  
  REPRESENTATION *rep;
  int pitch_index;
  int pitch_tindex;
  
  int n_time_tokens;
  int min_pitch;
  int max_pitch;
};

class MUSENET : public ENCODER {
public:
  MUSENET(int min_pitch_, int max_pitch_, int n_time_tokens_, int use_velocity_) {
    n_time_tokens = n_time_tokens_;
    min_pitch = min_pitch_;
    max_pitch = max_pitch_;
    rep = new REPRESENTATION({{2},{16,32*use_velocity_,max_pitch-min_pitch},{n_time_tokens}});
    pitch_tindex = 1;
    pitch_index = 2;
    use_velocity = use_velocity_;
  }
  vector<int> convert(set<Event> &events, double time_stretch=1., int pitch_shift=0) {

    int current_step = 0;
    int step, inst, velocity, pitch, velocity_bin;
    vector<int> tokens(2,0);
    vector<int> empty;

    // keep track of the min pitch and max pitch within a sequence
    // so that pitch shifting is easy
    tokens[0] = get<2>(*events.begin());
    tokens[1] = tokens[0];

    tokens.push_back( rep->encode({0},0) );

    for (const auto event : events) {
      step = (int)((double)get<0>(event) * time_stretch);
      velocity = get<1>(event);
      pitch = get<2>(event) + pitch_shift - min_pitch;
      inst = get<3>(event)/8; // put into groups

      if ((pitch >= 0) && (pitch < (max_pitch - min_pitch))) {
        tokens[0] = min(tokens[0], pitch);
        tokens[1] = max(tokens[1], pitch);

        if (step > current_step) {
          while (step > current_step + n_time_tokens) {
            tokens.push_back( rep->encode({n_time_tokens-1},2) );
            current_step += n_time_tokens;
          }
          if (step > current_step) {
            tokens.push_back( rep->encode({step-current_step-1},2) );
          }
          current_step = step;
        }
        
        velocity_bin = MUSENET_VELOCITY_MAP[velocity] * use_velocity;
        tokens.push_back( rep->encode({inst,velocity_bin,pitch},1) ); 
      }
    }
    tokens.push_back( rep->encode({1},0) );
    return tokens;
  }
  int use_velocity;
};


enum ENCODING {
  MUSENET_DEFAULT,
};

MUSENET x(0,128,100);
map<ENCODING,ENCODER*> m = {{MUSENET_DEFAULT,&x}};

int main() {

  int min_pitch = 32;
  int max_pitch = 96;
  int n_time_tokens = 100;
  vector<vector<int>> doms = {{16,32,max_pitch-min_pitch},{n_time_tokens}};
  REPRESENTATION rep(doms);


  for (int n=0; n<100000; n++) {

    int d = (rand() % 5) + 2;
    vector<int> dom(d);
    vector<int> pa(d);
    vector<int> pb(d);
    for (int i=0; i<d; i++) {
      dom[i] = (rand() % 25) + 5; 
      pa[i] = rand() % dom[i];
      pb[i] = rand() % dom[i];
    }
    TOKEN m(dom);
    int token = m.encode(pa);
    int index = rand() % d;
    assert(pa[index] == m.decode(token, index));
    int shift = pb[index] - pa[index];
    assert(pb[index] == m.decode(m.shift(token,index,shift),index));
  
    /*
    vector<vector<int>> rdom;
    vector<vector<int>> rpa;
    vector<vector<int>> rpb;
    for (int nn=0; nn<3; nn++) {
      int d = (rand() % 5) + 2;
      vector<int> dom(d);
      vector<int> pa(d);
      vector<int> pb(d);
      for (int i=0; i<d; i++) {
        dom[i] = (rand() % 25) + 5; 
        pa[i] = rand() % dom[i];
        pb[i] = rand() % dom[i];
      }
      rdom.push_back(dom);
      rpa.push_back(pa);
      rpb.push_back(pb);
    }
    */
  
  }
}