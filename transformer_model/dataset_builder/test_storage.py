import dataset_builder
import numpy as np
from subprocess import call
from tqdm import tqdm

def test_storage(n):
	jag = dataset_builder.Jagged("test.arr", True, 0)
	sizes = np.random.randint(1000,size=(n,)) + 100
	data = [np.random.randint(1000,size=(size,)) for size in sizes]
	for i,d in enumerate(data):
		jag.append(d,i%3)
	jag.flush()

	jagr = dataset_builder.Jagged("test.arr", False, 0)
	for i in tqdm(range(n)):
		assert(np.array_equal(data[i], jagr.read(i//3,i%3)))

if __name__ == "__main__":

  test_storage(10000)
  call("rm test.arr", shell=True)
  call("rm test.arr.header", shell=True)