import glob
import numpy as np

import dataset_builder
from interface_utils import param

#from collections import namedtuple
#param = namedtuple('PARAM', ('max', 'min', 'type', 'default', 'desc'))

ckpt_path = "model_190000.pkl"

def download_model():
  import os
  from subprocess import call
  if not os.path.exists(ckpt_path):
    call("wget -O model_190000.pkl http://vault.sfu.ca/index.php/s/i6GPEWIfRt1Mifn/download -q --show-progress", shell=True)

def train(
  data_directory: param(min=None,max=None,type=str,default="'../Sorted_Corpus/The_Beatles'",desc="folder containing MIDI files for training."),
  model_instance_name: param(min=None,max=None,type=str,default="'tmp_model'",desc="unique filename for trained model"),
  midiChannelsFormat : param(min=0, max=1, type=int, default=0, desc="is the midi formatted correctly.")
):
  download_model()
  mode = dataset_builder.ENCODER_MODES.MUSENET_DEFAULT
  jag = dataset_builder.Jagged(model_instance_name, mode, True)
  for path in glob.glob(data_directory + "/**/*.mid", recursive=True):
    seqs = dataset_builder.encode(path, mode, True)
    jag.append(seqs[0],0) # add each to train set

def generate(
  result_directory : param(min=None,max=None,type=str,default=None,desc="folder to contain generated files."),
  batch_size: param(min=0,max=128,type=int,default=2,desc="number of midis to generate"),
  temperature : param(min=0.5,max=5.,type=float,default=1.,desc="temperature for sampling"),
  model_instance_name: param(min=None,max=None,type=str,default="'tmp_model'",desc="unique filename for trained model"),
  prompt_length: param(min=1,max=1024,type=int,default=256,desc="the number of tokens provided as a prompt to the model."),
  length: param(min=1,max=1024,type=int,default=512,desc="the number of tokens to generate for each sequence.")
):

  import gin
  import os
  import jax
  import trax
  from trax.supervised import inputs
  from trax.models.beam_search import Search
  from configs import config_map
  from tqdm import tqdm
  from scipy.special import softmax
  from trax.shapes import ShapeDtype

  vocab_size = 4198
  model_cls = trax.models.TransformerLM
  #model_cls = trax.models.RNNLM

  gin.parse_config(
    ["vocab_size={}".format(vocab_size)] + config_map["tran_defaults"])

  model = model_cls(mode='predict')
  model.init_from_file(ckpt_path, weights_only=True)

  mode = dataset_builder.ENCODER_MODES.MUSENET_DEFAULT
  jag = dataset_builder.Jagged(model_instance_name, mode, False)
  prompt = []
  idx = np.random.randint(jag.get_size(), size=(batch_size,))
  for i in idx:
    prompt.append(jag.read(i,0)[:prompt_length])
  prompt = np.array(prompt)

  signature = ShapeDtype((batch_size, 1), np.int32)
  _,state = model_cls(mode='predict').init(signature)

  seqs = []
  for i in tqdm(range(length)):
    if i < prompt.shape[1]:
      inputs = prompt[:,i][:,None]
    else:
      inputs = [np.random.choice(np.arange(4198), p=p) for p in softmax(logits[:,-1,:],axis=-1)]
      inputs = np.array(inputs)[:,None]
      
    logits, state = model.pure_fn(
      inputs,
      model.weights,
      state,
      jax.random.PRNGKey(12346))
  
    seqs.append(inputs)
  
  seqs = np.concatenate(seqs,axis=1)

  if len(result_directory):
    os.makedirs(result_directory, exist_ok=True)
  for i,seq in enumerate(seqs):
    midi_path = os.path.join(result_directory, "{}.mid".format(i))
    dataset_builder.decode(seq, mode, midi_path)

"""
if __name__ == "__main__":

  train(data_directory="../dataset_builder/test_midi", model_instance_name="test",midiChannelsFormat=0)
  generate("output", 2, 1., "test", 32, 32)
"""
