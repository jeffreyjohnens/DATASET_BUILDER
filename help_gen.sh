#!/bin/bash
#SBATCH --gres=gpu:v100l:1
#SBATCH --cpus-per-task=32
#SBATCH --exclusive
#SBATCH --mem=0
#SBATCH --time=0-01:00
#SBATCH --account=def-pasquier
#SBATCH --mail-user jeffreyjohnens@gmail.com
#SBATCH --mail-type ALL

module load gcc/7.3.0 cuda/10.0.130
export XLA_FLAGS=--xla_gpu_cuda_data_dir=$EBROOTCUDA
source ~/TRAX/bin/activate
cd /home/jeffe/project/jeffe/DATASET_BUILDER

python train_reformer2.py --train 0 --name musenet_trans --checkpoint_step 190000 --temp .95 --midi_start 0 --batch_size 16

python train_reformer2.py --train 0 --name musenet_trans --checkpoint_step 190000 --temp 1. --midi_start 0 --batch_size 16

python train_reformer2.py --train 0 --name musenet_trans --checkpoint_step 190000 --temp .95 --midi_start 0 --batch_size 32

python train_reformer2.py --train 0 --name musenet_trans --checkpoint_step 190000 --temp 1. --midi_start 0 --batch_size 32

#TEMP=1.
#for ((i=0; i<=320; i=i+16)); do
#  python train_reformer2.py --train 0 --name musenet_trans --checkpoint_step 195000 --temp $TEMP --midi_start $i
#done



