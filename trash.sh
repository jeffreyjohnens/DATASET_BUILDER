#salloc --time=1:0:0 --ntasks=8 --account=def-pasquier --mem-per-cpu 8000 --gres gpu:1 --nodes 1


# salloc --time=1:0:0 --ntasks-per-node=48 --mem=0 --account=def-pasquier

#salloc --time=1:0:0 --ntasks=1 --cpus-per-task=16 --account=def-pasquier --mem=0 --gres gpu:1

#salloc --time=1:00 --cpus-per-task=24 --ntasks=1 --gres=gpu:p100l:4 --mem=0 --account=def-pasquier

# salloc --time=1:00 --cpus-per-task=24 --ntasks=1 --gres=gpu:p100l --mem=0 --account=def-pasquier

# salloc --time=1:0:0 --ntasks=1 --cpus-per-task=24 --account=def-pasquier --mem=0 --gres gpu:4

# SCRIPT FOR RUNNING MODEL
module load gcc/7.3.0 cuda/10.0.130
export XLA_FLAGS=--xla_gpu_cuda_data_dir=$EBROOTCUDA
source ~/TRAX/bin/activate
cd ~/project/jeffe/DATASET_BUILDER
python train_reformer2.py


# killall GPU mem
fuser -v /dev/nvidia*
kill -9 23689


# installing jaxlib
module load nixpkgs/16.09 gcc/7.3.0 cuda/9.2.148 cudnn
module load nixpkgs/16.09 intel/2018.3
module load cuda/9.2.148 cudnn

PYTHON_VERSION=cp36  # alternatives: cp36, cp37, cp38
CUDA_VERSION=cuda92  # alternatives: cuda92, cuda100, cuda101, cuda102
PLATFORM=linux_x86_64  # alternatives: linux_x86_64
BASE_URL='https://storage.googleapis.com/jax-releases'
pip install --upgrade $BASE_URL/$CUDA_VERSION/jaxlib-0.1.43-$PYTHON_VERSION-none-$PLATFORM.whl

pip install --upgrade https://storage.googleapis.com/jax-releases/cuda100/jaxlib-0.1.43-cp36-none-linux_x86_64.whl

pip install --upgrade jax  # install jax

# installing trax
pip install --user trax



module load python/3.6 nixpkgs/16.09  gcc/5.4.0  cuda/10.0.130 openmpi/2.1.1
source ~/TRAX_ENV/bin/activate

XLA_FLAGS=--xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda

XLA_FLAGS=--xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.130/

export XLA_FLAGS="--xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.130/"

XLA_FLAGS="--xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.130/" python -c "import trax"

# create  simlink
ln -s /usr/local/cuda /cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.13

TF_XLA_FLAGS="--tf_xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.13"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.130/bin


export PATH=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.13/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.13/lib64:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}