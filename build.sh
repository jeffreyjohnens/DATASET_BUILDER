# build environment
cd ~
module load python/3.6 nixpkgs/16.09  gcc/5.4.0  cuda/10.0.130 openmpi/2.1.1
virtualenv --no-download ~/TRAX_ENV
source ~/TRAX_ENV/bin/activate
pip install --no-index --upgrade pip
pip install --no-index tensorflow-gpu
pip install --upgrade https://storage.googleapis.com/jax-releases/cuda100/jaxlib-0.1.43-cp36-none-linux_x86_64.whl
pip install jaxlib
pip install trax

python -c "import trax"

Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/trax/__init__.py", line 18, in <module>
    from trax import layers
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/trax/layers/__init__.py", line 23, in <module>
    from trax.layers.attention import *
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/trax/layers/attention.py", line 19, in <module>
    import jax
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/jax/__init__.py", line 19, in <module>
    from jax.api import *
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/jax/api.py", line 46, in <module>
    from .lib.xla_bridge import canonicalize_dtype, device_count
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/jax/lib/xla_bridge.py", line 55, in <module>
    from jaxlib import xla_client
  File "/home/jeffe/TRAX_ENV/lib/python3.6/site-packages/jaxlib/xla_client.py", line 37, in <module>
    from . import xla_extension as _xla
ImportError: libcusolver.so.10.0: cannot open shared object file: No such file or directory

I have tried 

XLA_FLAGS="--xla_gpu_cuda_data_dir=/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc5.4/cuda/10.0.130/" python -c "import trax"