rm -rf output
rm -rf runs

python3 train.py \
    --output_dir=output \
    --model_type=gpt2 \
    --model_name_or_path=gpt2 \
    --do_train \
    --train_data_file=invalid.raw \
    --do_eval \
    --eval_data_file=invalid.raw \
    --overwrite_output_dir