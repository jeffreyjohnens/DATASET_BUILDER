import requests
import os
import gin
import os
import jax
import trax
from trax.supervised import inputs
from trax.models.beam_search import Search

import numpy as np
import jax.numpy as jnp
from scipy.special import softmax

from dataset_builder import Jagged, ENCODER_MODES, get_encoding_sizes, decode
from subprocess import call

# rsync jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/checkpoints/musenet_default/model_2000.pkl /Users/jeff/CODE/DATASET_BUILDER/checkpoints/musenet_default/model_2000.pkl

# rsync jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/checkpoints/musenet_trans/model_120000.pkl /Users/jeff/CODE/DATASET_BUILDER/checkpoints/musenet_trans/model_120000.pkl

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--train", type=int, default=1)
  parser.add_argument("--checkpoint_step", type=int, default=0)
  parser.add_argument("--overwrite", type=int, default=0)
  parser.add_argument("--name", type=str, default="musenet_trans")
  parser.add_argument("--temp", type=float, default=.5)
  parser.add_argument("--midi_start", type=int, default=0)
  parser.add_argument("--dry", type=int, default=0)
  parser.add_argument("--batch_size", type=int, default=16)
  args = parser.parse_args()

  if args.dry:
    print(vars(args))
    exit()

  model_cls = trax.models.TransformerLM
  train_steps = 200000
  train_chunk_steps = 10
  model_name = args.name
  #dataset_path = "dataset_builder/data_small.arr"
  dataset_path = "/home/jeffe/scratch/data_small.arr"
  model_folder = os.path.join("checkpoints", model_name)
  sample_folder = os.path.join("examples", model_name)
  batch_size = 32

  axial = [4,1024]
  max_seq_len = axial[0] * axial[1]

  encoder_mode = ENCODER_MODES.MUSENET_DEFAULT
  vocab_size = get_encoding_sizes()[encoder_mode]
  max_seq_len = 2048

  if args.overwrite:
    print("removing old model in " + model_folder)
    call("rm -rf " + model_folder, shell=True)

  if args.train == 0:
    tmp_folder = "tmpfGEN"
    call("rm -rf " + tmp_folder, shell=True)
    os.makedirs(tmp_folder, exist_ok=True)
    src = os.path.join(model_folder,"model_{}.pkl".format(args.checkpoint_step))
    dest = os.path.join(tmp_folder,"model.pkl")
    call("cp " + src + " " + dest, shell=True)
    model_folder = tmp_folder
    os.makedirs(sample_folder, exist_ok=True)

  # Set up the data pipeline.
  """
  def my_inputs(n_devices):
    while True:
      mask = np.ones((batch_size,max_seq_len),dtype=np.int32)
      inputs = np.random.randint(100, size=(batch_size,max_seq_len))
      yield (inputs, inputs, mask)
  """

  def train_inputs(*args):
    jag = Jagged(dataset_path, encoder_mode, False)
    while True:
      batch, mask = jag.read_batch(batch_size, max_seq_len, 0)
      yield (np.array(batch), np.array(batch), np.array(mask))

  def train_eval_inputs(*args):
    jag = Jagged(dataset_path, encoder_mode, False)
    while True:
      batch, mask = jag.read_batch(batch_size, max_seq_len, 1)
      yield (np.array(batch), np.array(batch), np.array(mask))

  #for batch,_,_ in train_inputs():
  #  print(batch[0][:100])
  #exit()

  params = [
    "axialx = {}".format(axial[0]), 
    "axialy = {}".format(axial[1]), 
    "n_tokens = {}".format(max_seq_len),
    "vocab_size = {}".format(vocab_size)
  ]

  defaults = ["""
  import trax.layers
  import trax.models
  import trax.optimizers
  import trax.supervised.inputs
  import trax.supervised.trainer_lib

  # Parameters that will vary between experiments:
  # ==============================================================================
  train.model = @trax.models.ReformerLM
  # Our model will have 6 layers, alternating between the LSH attention proposed
  # in the Reformer paper and local attention within a certain context window.
  n_layers = 6
  attn_type = [
    @SelfAttention,
    @LSHSelfAttention,  
    @SelfAttention,
    @LSHSelfAttention,
    @SelfAttention,
    @LSHSelfAttention,
    ]
  share_qk = False  # LSH attention ignores this flag and always shares q & k
  n_heads = 6
  attn_kv = 64
  dropout = 0.05

  # Parameters for MultifactorSchedule:
  # ==============================================================================
  MultifactorSchedule.constant = 0.0001
  MultifactorSchedule.factors = 'constant'

  # Parameters for Adam:
  # ==============================================================================
  Adam.weight_decay_rate=0.0
  Adam.b1 = 0.9
  Adam.b2 = 0.9999
  Adam.eps = 1e-7

  # Parameters for SelfAttention:
  # ==============================================================================
  SelfAttention.attention_dropout = 0.05
  SelfAttention.chunk_len = 64
  SelfAttention.n_chunks_before = 1
  SelfAttention.n_parallel_heads = 1

  # Parameters for LSHSelfAttention:
  # ==============================================================================
  LSHSelfAttention.attention_dropout = 0.0
  LSHSelfAttention.chunk_len = 64
  LSHSelfAttention.n_buckets = [64, 128]
  LSHSelfAttention.n_chunks_after = 0
  LSHSelfAttention.n_chunks_before = 1
  LSHSelfAttention.n_hashes = 1
  LSHSelfAttention.n_parallel_heads = 1
  LSHSelfAttention.predict_drop_len = 128
  LSHSelfAttention.predict_mem_len = 1024

  # Parameters for ReformerLM:
  # ==============================================================================
  ReformerLM.attention_type = %attn_type
  ReformerLM.d_attention_key = %attn_kv
  ReformerLM.d_attention_value = %attn_kv
  ReformerLM.d_model = 256
  ReformerLM.d_ff = 512
  ReformerLM.dropout = %dropout
  ReformerLM.ff_activation = @trax.layers.Relu
  ReformerLM.max_len = %n_tokens
  ReformerLM.mode = 'train'
  ReformerLM.n_heads = %n_heads
  ReformerLM.n_layers = %n_layers
  ReformerLM.vocab_size = %vocab_size
  ReformerLM.share_qk = %share_qk
  ReformerLM.axial_pos_shape = (%axialx, %axialy)
  ReformerLM.d_axial_pos_embs= (64, 192)
  """]

  gru_defaults = ["""
    import trax.layers
    import trax.models
    import trax.optimizers
    import trax.supervised.inputs
    import trax.supervised.trainer_lib

    train.model = @trax.models.RNNLM
    RNNLM.vocab_size = %vocab_size
    RNNLM.d_model=512
    RNNLM.n_layers=2

    # Parameters for MultifactorSchedule:
    # ==============================================================================
    MultifactorSchedule.constant = 0.00001
    MultifactorSchedule.factors = 'constant'

    # Parameters for Adam:
    # ==============================================================================
    Adam.weight_decay_rate=0.0
    Adam.b1 = 0.9
    Adam.b2 = 0.9999
    Adam.eps = 1e-5
  """]

  tran_defaults = ["""
    import trax.layers
    import trax.models
    import trax.optimizers
    import trax.supervised.inputs
    import trax.supervised.trainer_lib

    train.model = @trax.models.TransformerLM
    TransformerLM.vocab_size = %vocab_size
    TransformerLM.d_model = 512
    TransformerLM.d_ff = 2048
    TransformerLM.n_layers = 6
    TransformerLM.n_heads = 8
    TransformerLM.dropout = 0.1
    TransformerLM.max_len = 2048

    # Parameters for MultifactorSchedule:
    # ==============================================================================
    MultifactorSchedule.constant = 0.0001
    MultifactorSchedule.factors = 'constant'

    # Parameters for Adam:
    # ==============================================================================
    Adam.weight_decay_rate=0.0
    Adam.b1 = 0.9
    Adam.b2 = 0.9999
    Adam.eps = 1e-5
  """]

  # Configure hyperparameters.
  gin.parse_config(params + tran_defaults)

  # arguments for trainer ..
  inputs = trax.supervised.inputs.Inputs(
    train_inputs, eval_stream=train_eval_inputs)
  output_dir = os.path.expanduser(model_folder)
  checkpoints_at = list(range(0,train_steps,1000))

  trainer = trax.supervised.Trainer(
      model=model_cls,
      loss_fn=trax.layers.CrossEntropyLoss,
      optimizer=trax.optimizers.Adam,
      lr_schedule=trax.lr.MultifactorSchedule,
      inputs=inputs,
      output_dir=output_dir,
      checkpoints_at=checkpoints_at,
      has_weights=True)

  if args.train:
    for _ in range(trainer._step,train_steps,train_chunk_steps):
      trainer.train_epoch(n_steps=train_chunk_steps, n_eval_steps=1)

  else:
    # only for reformer
    #gin.parse_config("""LSHCausalAttention.n_hashes = 4""")

    sampling_decoder = Search(
        model_cls,
        trainer.model_weights,
        max_decode_len=1024,
        temperature=args.temp
        )

    # need to change line 607 in beam search file ...
    # change line 289 so that sampling is random each time ...
    # Call fast-decoder model on current tokens to get next-position logits.
    # --> [batch * beam, vocab]
    """
import time
onp.random.seed(int(time.time() * 10e6) % (2**31 - 1))
flat_logits, new_flat_cache = tokens_to_logits(
  flat_ids, flat_cache, jax.random.PRNGKey(onp.random.randint(2**31)))
    """

    seqs, scores = sampling_decoder.decode(batch_size=args.batch_size)
    for i,seq in enumerate(seqs):
      midi_path = "example_{}_temp={}_{}.mid".format(
        trainer._step, args.temp, args.midi_start + i)
      midi_path = os.path.join(sample_folder, midi_path)
      decode(seq, encoder_mode, midi_path)