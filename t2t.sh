DATA_DIR=trash_data
HPARAMS_SET=fuckery
MODEL=transformer
PROBLEM=my_problem
TRAIN_DIR=training

rm -rf $DATA_DIR
rm -rf $TRAIN_DIR
mkdir $DATA_DIR
mkdir $TRAIN_DIR

touch trash_data/my_problem-train.tfrecords

HPARAMS=\
"label_smoothing=0.0,"\
"max_length=0,"\
"max_target_seq_length=2048"

python3 t2t.py \
  --data_dir="${DATA_DIR}" \
  --hparams=${HPARAMS} \
  --hparams_set=${HPARAMS_SET} \
  --model=${MODEL} \
  --output_dir=${TRAIN_DIR} \
  --problem=${PROBLEM} \
  --train_steps=1000000