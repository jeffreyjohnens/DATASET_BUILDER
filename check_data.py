from dataset_builder import Jagged
import numpy as np

j = Jagged("dataset_builder/data.arr", False, 0)
sizes = [len(j.read(i,0)) for i in range(31808)]

print(np.median(sizes))