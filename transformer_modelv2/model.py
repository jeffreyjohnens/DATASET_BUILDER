import os
import glob
import json
import copy
import time
import numpy as np
import torch
from subprocess import call

import dataset_builder_2 as db

# make README
# fix the bug with list input ...

#from interface_utils import param
from collections import namedtuple
param = namedtuple('PARAM', ('max', 'min', 'type', 'default', 'desc'))

from transformers import *

model_dict = {
  "gpt2" : GPT2LMHeadModel
}

def load_model_from_web(model_name,name,step,ckpt_path=""):
  if len(ckpt_path) == 0:
    file_list = ["config.json", "pytorch_model.bin", "scheduler.pt", "training_args.bin"] # don't need optimizer
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    current_folder = os.path.dirname(os.path.abspath(__file__))
    dst_folder = os.path.join(current_folder, ckpt_path)
    os.makedirs(dst_folder, exist_ok=True)
    for file in file_list:
      path = os.path.join(dst_folder,file)
      if not os.path.exists(path):
        call("wget http://www.sfu.ca/~jeffe/{}/{} -O {}".format(ckpt_path,file,path), shell=True)
  return model_dict[model_name].from_pretrained(ckpt_path, from_tf=False)

def load_model_direct(model_name,ckpt_path):
  return model_dict[model_name].from_pretrained(ckpt_path, from_tf=False)

#=================================================================

GENERAL_MIDI_MAP = {
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [128],
  "any" : list(range(129))
}

def make_inf_mask(mask):
  mask[mask==0] = -float("Inf")
  mask[mask==1] = 0
  return mask

def generate_w_injections(model, input_ids=None, injects=None, temperature=.9):
  import torch.nn.functional as F
  assert injects is not None
  batch_size = len(injects)

  gen_start_time = time.time()

  if input_ids is None:
    input_ids = torch.zeros((batch_size,1), dtype=torch.long)
  else:
    input_ids = torch.from_numpy(input_ids).to(torch.long)
  # must be 2d

  past = None
  finished = np.zeros((batch_size,), dtype=np.bool)
  use_cache = model.config.use_cache
  vocab_size = model.config.vocab_size
  
  while np.any(finished==False):

    raw_input = input_ids
    if past:
      raw_input = input_ids[:,-1].unsqueeze(-1)

    outputs = model(input_ids=raw_input, past=past, use_cache=use_cache)
    if model._use_cache(outputs, use_cache):
      past = outputs[1]
    
    logits = outputs[0][:,-1,:]
    if temperature != 1.0:
      logits = logits / temperature
    
    last_tokens = raw_input[:,-1].detach().numpy()
    masks = torch.zeros((batch_size, vocab_size), dtype=torch.float)
    for i,(last_token,inject) in enumerate(zip(last_tokens,injects)):
      trigger, mask = inject[0]
      if last_token == trigger:
        if mask is not None:
          assert mask.dtype == np.bool
          masks[i][~mask] = -float("Inf") # set avoided tokens to smallest value
        if len(inject) == 1:
          finished[i] = True
        else:
          inject.pop(0)
        
        #print(np.where(masks==0))
    
    if np.any(finished==False):
      logits = masks + logits
      
      probs = F.softmax(logits, dim=-1)
      next_token = torch.multinomial(probs, num_samples=1).squeeze(1)
      next_token[finished] = 0 # if the sequence has ended
      #print(db.TrackEncoder().rep.pretty([next_token]), db.TrackEncoder().rep.pretty([injects[0][0][0]]))
      input_ids = torch.cat([input_ids, next_token.unsqueeze(-1)], dim=-1)
    
    if input_ids.shape[1] >= 2048:
      finished[:] = True

  print(time.time() - gen_start_time)
  input_ids = input_ids.detach().numpy()

  # remove the padding at end of sequences
  output = []
  for seq in input_ids:
    idx = np.where(seq==0)[0]
    if len(idx) > 1:
      output.append( seq[:idx[1]] )
    else:
      output.append( seq )
  return output 

def run_generation(model, input_ids, tracks, temperature):
  
  # make the injects
  vocab_size = model.config.vocab_size
  enc = db.TrackEncoder()

  PIECE_START = enc.rep.encode({db.TOKEN_TYPE.PIECE_START : 0})
  NORMAL_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 0})
  DRUM_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 1})
  TRACK_END = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})
  ANY_TOKEN_MASK = None
  
  # impose various controls on the generated sequence
  control = []
  for track_num, track in enumerate(tracks):
    if isinstance(track,str):
      if track in GENERAL_MIDI_MAP:
        track = GENERAL_MIDI_MAP[track]
      else:
        print("WARNING : UNKNOWN GENERAL MIDI CATEGORY")
        track = GENERAL_MIDI_MAP["any"]
    assert isinstance(track,list)
    
    mask = np.zeros((vocab_size,), dtype=np.bool)
    if 128 in track:
      mask[DRUM_TRACK] = True
    if any([t < 128 for t in track]):
      mask[NORMAL_TRACK] = True

    print(np.where(mask))

    if track_num == 0 and input_ids is None:
      control.append((PIECE_START, mask))
    else:
      control.append((TRACK_END, mask))

    mask = np.zeros((vocab_size,), dtype=np.bool)
    if 128 not in track:
      for allowed_inst in track:
        tok = enc.rep.encode({db.TOKEN_TYPE.INSTRUMENT : allowed_inst})
        mask[tok] = True
      #for _ in range(4):
      #  control.append((BAR_START, mask))
      control.append((NORMAL_TRACK, mask)) # for force instrument version

  # only append final TRACK_END once ..
  control.append((TRACK_END, ANY_TOKEN_MASK))

  batch_size = len(input_ids)
  controls = [copy.deepcopy(control) for _ in range(batch_size)]
  tokens = generate_w_injections(
    model, input_ids=input_ids, injects=controls, temperature=temperature)

  return tokens

#=================================================================

def train(
  data_directory: param(min=None,max=None,type=str,default="'../Sorted_Corpus/The_Beatles'",desc="folder containing MIDI files for training."),
  model_instance_name: param(min=None,max=None,type=str,default="'tmp_model'",desc="unique filename for trained model"),
  midiChannelsFormat : param(min=0, max=1, type=int, default=0, desc="is the midi formatted correctly.")
):
  encoder = db.TrackEncoder()
  e = db.EncoderConfig()
  
  mid_paths = glob.glob(data_directory + "/**/*.mid", recursive=True)
  midi_paths = glob.glob(data_directory + "/**/*.midi", recursive=True)
  paths = list(mid_paths) + list(midi_paths)

  if len(paths) == 0:
    print("ERROR : NO MIDI FILES FOUND IN DIRECTORY")
    return
  if len(paths) > 1:
    print("WARNING : ONLY USING ONE OF THE PROVIDED MIDI FILES.")
  with open(model_instance_name, "w") as f:
    json.dump(json.loads(encoder.midi_to_json(paths[0],e)), f)

def generate(
  result_directory : param(min=None,max=None,type=str,default=None,desc="folder to contain generated files."),
  batch_size: param(min=0,max=128,type=int,default=2,desc="number of midis to generate"),
  temperature : param(min=0.5,max=5.,type=float,default=.9,desc="temperature for sampling"),
  model_instance_name: param(min=None,max=None,type=str,default="'tmp_model'",desc="unique filename for trained model"),
  tracks_to_generate: param(min=None,max=None,type=str,default="''",desc="a list describing restrictions on the instruments for each generated track"),
  ckpt_path: param(min=None,max=None,type=str,default="''",desc="a path to a checkpoint")
):

  # use this actually
  #model = load_model_from_web("gpt2", "TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False", 130000)
  model = load_model_from_web("gpt2", "TRACK_ENCODER_gpt2_version1_Jul_01_22_28_False", 310000, ckpt_path=ckpt_path)

  #model = load_model_direct("gpt2", "../train_hugging/checkpoints/TRACK_ONE_BAR_FILL_ENCODER_gpt2_version1_Jun_24_09_57_False/checkpoint-130000")
  
  enc = db.TrackEncoder()
  e = db.EncoderConfig()

  with open(model_instance_name, "r") as f:
    piece = json.load(f)
    json_string = json.dumps(piece)

  valid_segments = piece["validSegments"]
  if len(valid_segments) == 0:
    print("ERROR : NO VALID SEGMENTS IN PROVIDED MIDI FILE")
    return
  
  segment_idx = np.random.choice(np.arange(len(valid_segments)))
  vint = piece["validTracks"][segment_idx]
  valid_tracks = [i for i in range(32) if vint & (1<<i)]
  start_bar = valid_segments[segment_idx]

  json_string = db.partial_copy_json(json_string, valid_tracks[:2], start_bar, 4)
  tokens = enc.json_to_tokens(json_string, e)
  tokens = np.tile(np.array(tokens)[None,:], (batch_size,1))

  try:
    tracks_to_generate = json.loads(tracks_to_generate)
  except:
    print("ERROR : {} IS NOT A VALID JSON STRING.")
    return

  seqs = run_generation(model, tokens, tracks_to_generate, temperature)

  if len(result_directory):
    os.makedirs(result_directory, exist_ok=True)
  for i,seq in enumerate(seqs):
    midi_path = os.path.join(result_directory, "{}.mid".format(i))
    enc.tokens_to_midi(seq, midi_path, e)
  



  
