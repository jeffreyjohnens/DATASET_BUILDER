import os
from model import train, generate

os.makedirs("demo", exist_ok=True)

train("test_input", "demo/test_model.json", 0)
generate("demo", 2, .9, "demo/test_model.json", '["drums", "drums"]', 'checkpoints/TRACK_ENCODER_gpt2_version1_Jul_01_22_28_False/checkpoint-310000')