# want to make an embedding space by encoding segments that have labels
# if we can get genre tags via spotify or via the matched dataset component
# then we can create some embedding space that the user can move around in

import csv
import os.path as osp
import glob
from tqdm import tqdm
import tables

def get_metadata(h5filename):
	h5 = tables.open_file(h5filename, mode='r')
	artist = h5.root.metadata.songs.cols.artist_name[0]
	title = h5.root.metadata.songs.cols.title[0]
	h5.close()
	return artist, title

def write_csv(data, path):
	with open(path, 'w')  as f:
		dict_writer = csv.DictWriter(f, data[0].keys())
		dict_writer.writeheader()
		dict_writer.writerows(data)

root_data_dir = "/users/jeff/DATA"
full_folder = osp.join(root_data_dir, "lmd_full")
match_folder = osp.join(root_data_dir, "lmd_matched")
meta_folder = osp.join(root_data_dir, "lmd_matched_h5")
full_paths = list(glob.glob(full_folder+"/**/*.mid",recursive=True))
match_paths = list(glob.glob(match_folder+"/**/*.mid",recursive=True))

meta = {}
for path in match_paths:
	h5path = osp.join(meta_folder,osp.dirname(osp.relpath(path,match_folder)))
	meta[osp.basename(path)] = h5path + ".h5"

data = []
for path in tqdm(full_paths,leave=False):
	bpath = osp.basename(path)
	meta_path = meta.get(bpath, None)
	if meta_path is not None:
		artist, title = get_metadata(meta_path)
		# find genre using the spotify web-api
		data.append( {
			"artist" : artist.decode('utf-8'),
			"title" : title.decode('utf-8'),
			"LID" : osp.splitext(path)[0]
		})

# write to a csv
write_csv(data, "metadata.csv")