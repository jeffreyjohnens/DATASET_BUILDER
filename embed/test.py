# test a simple request
import requests
import base64
import json
import csv
import time
import re
from tqdm import tqdm

client_id = "2f31656c9886461193926daab745060a"
client_secret = "e3465b7e980c48909cb542437753125d"
client_string = client_id + ":" + client_secret
cred = base64.b64encode(client_string.encode('ascii')).decode('ascii')

def refresh_token():
  auth_header = {"Authorization" : "Basic " + cred}
  response = requests.post("https://accounts.spotify.com/api/token", data={"grant_type" : "client_credentials"}, headers=auth_header)
  return response.json()["access_token"]

def make_url(title, artist):
  artist = "+".join(artist.split())
  title = "+".join(title.split())
  return "https://api.spotify.com/v1/search?q={title}&artist={artist}&type=track".format(artist=artist, title=title)

def make_artist_url(artist):
  artist = "+".join(artist.split())
  return "https://api.spotify.com/v1/search?q={}&type=artist".format(artist)

def get_artist_genre(artist, token):
  auth_header = {"Authorization" : "Bearer " + token}
  response = requests.get(make_artist_url(artist), headers=auth_header)
  if response.ok:
    return response.json()["artists"]["items"][0]["genres"]

def write_csv(data, path):
	with open(path, 'w')  as f:
		dict_writer = csv.DictWriter(f, data[0].keys())
		dict_writer.writeheader()
		dict_writer.writerows(data)

def write_json(data, path):
  with open(path, "w") as f:
    json.dump(data,f,indent=4)

# get all the artist names from the metadata.csv
artists = {}
with open("metadata.csv", "r") as f:
  for row in csv.DictReader(f):
    for artist in re.split(';|/|_', row["artist"]):
      artist_clean = re.sub("[\(\[].*?[\)\]]", "", artist).strip().lower()
      artists[artist_clean] = artists.get(artist_clean,0) + 1

artists = sorted(list(artists.keys()))

token = refresh_token()

data = {}
for i,artist in tqdm(enumerate(artists)):
  try:
    genres = get_artist_genre(artist, token)
    if len(genres):
      data[artist] = genres
    time.sleep(.1)
  except:
    print(artist + " FAILED")
  
  if i % 100 == 0:
    write_json(data, "genre_{}.json".format(i))

write_json(data, "genre.json")