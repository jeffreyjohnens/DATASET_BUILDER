# get the vectors
import csv
import json
import torch
import numpy as np
from tqdm import tqdm
from transformers import *
import dataset_builder_2 as db

def get_tokens(path):
  encoder = db.TrackDensityEncoder()
  ec = db.EncoderConfig()
  ec.num_bars = 8
  return torch.from_numpy(np.array(encoder.midi_to_tokens(path,ec)))

ckpt_path = "../examples/local_notebook/checkpoints/TRACK_DENSITY_ENCODER_gpt2_version3_Aug_13_13_31_False_num_bars_8_6/checkpoint-240000"
model = GPT2LMHeadModel.from_pretrained(ckpt_path, from_tf=False)

data = {}
with open("metadata.csv", "r") as f:
  for ro in tqdm(list(csv.DictReader(f))[:10]):
    path = "/Users/Jeff/DATA/lmd_full/{}/{}.mid".format(ro["LID"][0],ro["LID"])
    try:
      tokens = get_tokens(path)
      state = model.transformer(input_ids=tokens)[0]
      print(state.shape)
      data[ro["LID"]] = {
        "state" : [float(_) for _ in state.detach().numpy().flatten()],
        "artist" : ro["artist"],
        "title" : ro["title"]
      }
    except Exception as e:
      print(e)

np.savez_compressed("metadata_w_state.npz", data=data)
#with open("metadata_w_state.json", "w") as f:
#  json.dump(data, f)