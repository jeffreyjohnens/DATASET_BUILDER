import json
import numpy as np

with open("genre.json", "r") as f:
  data = json.load(f)

# map from genre to integer
genres = {}
genres_rev = {}
for k,v in data.items():
  for genre in v:
    if not genre in genres:
      length = len(genres)
      genres[genre] = length
      genres_rev[length] = genre

# map each artist to a vector
z = len(genres)
vecs = []
for k,v in data.items():
  vec = np.zeros((z,), dtype=np.float32)
  for genre in v:
    vec[genres[genre]] = 1
  vecs.append( vec )
vecs = np.array(vecs)

# remove things with less than 1 support
low_rows = (np.sum(vecs,axis=0)<=50)
vecs[:,low_rows] = 0
print(np.sum(low_rows==0))

# do dimensionality reduction
from sklearn.decomposition import PCA, KernelPCA
from sklearn.manifold import TSNE

alg = TSNE

model = alg(n_components=2) #, kernel="linear")
Y = model.fit_transform(vecs)

# pick ten most popular genres and plot them
from matplotlib import pyplot as plt

top_genres = np.argsort(np.sum(vecs,axis=0))[-10:]

for genre in top_genres:
  index = vecs[:,genre]>0
  plt.scatter(Y[index,0], Y[index,1], label=genres_rev[genre], alpha=.5)

plt.legend()
plt.show()