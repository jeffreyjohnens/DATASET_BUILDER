import os
import gin
import jax
import trax
import requests
from trax.supervised import inputs
from trax.models.beam_search import Search

import numpy as np
import jax.numpy as jnp
from scipy.special import softmax
from configs import config_map

from dataset_builder_2 import Jagged, ENCODER_TYPE, getEncoderSize
from subprocess import call

# rsync jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/checkpoints/musenet_default/model_2000.pkl /Users/jeff/CODE/DATASET_BUILDER/checkpoints/musenet_default/model_2000.pkl

# rsync jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/checkpoints/musenet_trans/model_120000.pkl /Users/jeff/CODE/DATASET_BUILDER/checkpoints/musenet_trans/model_120000.pkl

# salloc --time=1:0:0 --ntasks-per-node=32 --mem=0 --gres=gpu:v100l:4 --account=def-pasquier

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--train", type=int, default=1)
  parser.add_argument("--checkpoint_step", type=int, default=0)
  parser.add_argument("--overwrite", type=int, default=0)
  parser.add_argument("--name", type=str, default="track_trans")
  parser.add_argument("--dry", type=int, default=0)
  args = parser.parse_args()

  if args.dry:
    print(vars(args))
    exit()

  model_cls = trax.models.TransformerLM
  train_steps = 200000
  train_chunk_steps = 10
  model_name = args.name
  #dataset_path = "../dataset_builder_2/test.arr"
  dataset_path = "/home/jeffe/scratch/data_track.arr"
  model_folder = os.path.join("../checkpoints", model_name)
  sample_folder = os.path.join("../examples", model_name)
  batch_size = 32

  encoder_mode = ENCODER_TYPE.TRACK_ENCODER
  vocab_size = getEncoderSize(encoder_mode)
  max_seq_len = 2048

  if args.overwrite:
    print("removing old model in " + model_folder)
    call("rm -rf " + model_folder, shell=True)

  if args.train == 0:
    tmp_folder = "tmpfGEN"
    call("rm -rf " + tmp_folder, shell=True)
    os.makedirs(tmp_folder, exist_ok=True)
    src = os.path.join(model_folder,"model_{}.pkl".format(args.checkpoint_step))
    dest = os.path.join(tmp_folder,"model.pkl")
    call("cp " + src + " " + dest, shell=True)
    model_folder = tmp_folder
    os.makedirs(sample_folder, exist_ok=True)

  # Set up the data pipeline.
  def train_inputs(*args):
    jag = Jagged(dataset_path)
    while True:
      batch, mask = jag.read_batch(batch_size, 0, encoder_mode)
      batch = np.array(batch)[:,:max_seq_len]
      mask = np.array(mask)[:,:max_seq_len]
      yield (batch, batch, mask)

  def train_eval_inputs(*args):
    jag = Jagged(dataset_path)
    while True:
      batch, mask = jag.read_batch(batch_size, 1, encoder_mode)
      batch = np.array(batch)[:,:max_seq_len]
      mask = np.array(mask)[:,:max_seq_len]
      yield (batch, batch, mask)

  params = [
    "n_tokens = {}".format(max_seq_len),
    "vocab_size = {}".format(vocab_size)
  ]

  # Configure hyperparameters.
  gin.parse_config(params + config_map["tran_defaults"])

  # arguments for trainer ..
  inputs = trax.supervised.inputs.Inputs(
    train_inputs, eval_stream=train_eval_inputs)
  output_dir = os.path.expanduser(model_folder)
  checkpoints_at = list(range(0,train_steps,1000))

  trainer = trax.supervised.Trainer(
      model=model_cls,
      loss_fn=trax.layers.CrossEntropyLoss,
      optimizer=trax.optimizers.Adam,
      lr_schedule=trax.lr.MultifactorSchedule,
      inputs=inputs,
      output_dir=output_dir,
      checkpoints_at=checkpoints_at,
      has_weights=True)

  for _ in range(trainer._step,train_steps,train_chunk_steps):
    trainer.train_epoch(n_steps=train_chunk_steps, n_eval_steps=1)