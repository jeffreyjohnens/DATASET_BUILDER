import gin
import os
import jax
import trax
import numpy as np
import jax.numpy as jp
from trax.supervised import inputs
from trax.models.beam_search import Search
from configs import config_map
from tqdm import tqdm
from scipy.special import softmax
from trax.shapes import ShapeDtype
from subprocess import call

import dataset_builder_2 as db
from dataset_builder_2 import Jagged, getEncoderSize, ENCODER_TYPE, decode

def download_model(name,step):
  # download a model from compute canada (cedar)
  ckpt_path = "../checkpoints/{}/model_{}.pkl".format(name,step)
  if not os.path.exists(ckpt_path):
    call("rsync -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/checkpoints/{}/model_{}.pkl /Users/jeff/CODE/DATASET_BUILDER/checkpoints/{}/model_{}.pkl --progress".format(name,step,name,step), shell=True)
  return ckpt_path

if __name__ == "__main__":

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("--step", type=int, required=True)
  parser.add_argument("--temperature", type=float, required=True)
  args = parser.parse_args()

  result_directory = "examples/{}/{}".format(args.step, args.temperature)
  os.makedirs(result_directory, exist_ok=True)
  ckpt_path = download_model("track_trans_fix", args.step)

  mode = ENCODER_TYPE.TRACK_ENCODER
  vocab_size = getEncoderSize(mode)
  model_cls = trax.models.TransformerLM

  gin.parse_config(
    ["vocab_size={}".format(vocab_size)] + config_map["tran_defaults"])

  model = model_cls(mode='predict')
  model.init_from_file(ckpt_path, weights_only=True)

  # get the prompts
  batch_size = 10
  jag = Jagged("../dataset_builder_2/test.arr")
  batch, mask = jag.read_batch(512, 2, mode) # use unseen samples
  batch = np.array(batch)

  enc = db.TrackEncoder()
  track_token = enc.rep.encode({db.TOKEN_TYPE.TRACK : 0})
  end_token = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})

  n_prompt = 1
  length = 1024

  full_prompts = []
  prompts = []
  for seq in batch:
    track_starts = np.where((seq==track_token)|(seq==(track_token+1)))[0]
    if len(prompts) < batch_size and len(track_starts) > n_prompt:
      prompts.append( seq[:track_starts[n_prompt]+1] )
      full_prompts.append( seq )

  for i,seq in enumerate(full_prompts):
    midi_path = os.path.join(result_directory, "{}_orig.mid".format(i))
    decode(midi_path, seq, mode)

  signature = ShapeDtype((batch_size, 1), np.int32)
  _,state = model_cls(mode='predict').init(signature)

  #model.init(signature) # need to init if we don't load from weights
  #np.random.seed(5812)

  import time
  np.random.seed(int(time.time()))

  prompts = np.zeros((batch_size,1), dtype=np.int32)
  temperature = args.temperature
  
  seqs = []
  for i in tqdm(np.arange(length)):
    inputs = np.zeros((batch_size,1), dtype=np.int32)
    for k,prompt in enumerate(prompts):
      if i < len(prompt):
        inputs[k] = prompt[i]
      else:
        probs = softmax(logits[k].flatten() / temperature)
        inputs[k] = np.random.choice(np.arange(vocab_size), p=probs)
    
    logits, state = model.pure_fn(
      inputs,
      model.weights,
      state,
      jax.random.PRNGKey(0))

    seqs.append(inputs)

  seqs = np.concatenate(seqs,axis=1)

  if len(result_directory):
    os.makedirs(result_directory, exist_ok=True)
  for i,seq in enumerate(seqs):
    midi_path = os.path.join(result_directory, "{}.mid".format(i))
    try:
      seq = seq[:np.where(seq==end_token)[0][4]]
      decode(midi_path, seq, mode)
    except:
      print("not enough tracks", i)
      pass
    