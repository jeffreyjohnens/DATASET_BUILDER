import jax
import numpy as np

@jax.jit
def sample_or_feed(k, step, length, prompt, logits):
  return jax.lax.cond(step < length, lambda _: prompt[step], None, lambda _: 0, None)

v = 128
b = 10
l = 100
prompt = np.random.randint(v, size=(b,l)) # np.eye(v)
logits = np.random.rand(b,l,v)
batch_index = np.arange(b)
step_index = np.full(b,0)
length = np.full(b,10)

fn = jax.vmap(sample_or_feed, in_axes=(0,0,0,0,0))

print(fn(batch_index,step_index,length,prompt,logits))

"""
def step(state, logits, i, prompts, lengths):
  inputs = jax.numpy.zeros((batch_size,1), dtype=np.int32)
  for k,(prompt,length) in enumerate(zip(prompts,lengths)):
    if i < length:
      #jax.ops.index_update(inputs, k, prompt[i])
      inputs[k] = prompt[i]
    else:
      #jax.ops.index_update(inputs, k, np.random.choice(np.arange(vocab_size), p=probs))
      probs = softmax(logits[k].flatten())
      inputs[k] = np.random.choice(np.arange(vocab_size), p=probs)
  
  logits, state = model.pure_fn(
    inputs,
    model.weights,
    state,
    jax.random.PRNGKey(0))
  
  return state, logits, inputs
"""

"""
seqs = []
for i in tqdm(np.arange(length)):
  #state,logits,inputs = step(state, logits, i, prompts, lengths)
  #seqs.append(inputs)

  inputs = np.zeros((batch_size,1), dtype=np.int32)
  for k,prompt in enumerate(prompts):
    if i < len(prompt):
      inputs[k] = prompt[i]
    else:
      probs = softmax(logits[k].flatten())
      inputs[k] = np.random.choice(np.arange(vocab_size), p=probs)
  
  logits, state = model.pure_fn(
    inputs,
    model.weights,
    state,
    jax.random.PRNGKey(0))

  seqs.append(inputs)

seqs = np.concatenate(seqs,axis=1)
"""