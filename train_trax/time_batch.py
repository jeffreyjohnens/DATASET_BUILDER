import time
import numpy as np
from dataset_builder_2 import Jagged, ENCODER_TYPE, getEncoderSize

if __name__ == "__main__":

  dataset_path = "/home/jeffe/scratch/data_track.arr"
  dataset_path = "../dataset_builder_2/test.arr"
  batch_size = 32
  encoder_mode = ENCODER_TYPE.TRACK_ENCODER
  vocab_size = getEncoderSize(encoder_mode)
  max_seq_len = 2048

  # Set up the data pipeline.
  def train_inputs(*args):
    jag = Jagged(dataset_path)
    while True:
      batch, mask = jag.read_batch(batch_size, 0, encoder_mode)
      batch = np.array(batch)[:,:max_seq_len]
      mask = np.array(mask)[:,:max_seq_len]
      yield (batch, batch, mask)

  def train_eval_inputs(*args):
    jag = Jagged(dataset_path)
    while True:
      batch, mask = jag.read_batch(batch_size, 1, encoder_mode)
      batch = np.array(batch)[:,:max_seq_len]
      mask = np.array(mask)[:,:max_seq_len]
      yield (batch, batch, mask)

  times = []
  current = time.time()
  for x,y,m in train_inputs():
    print(x.shape, m.shape)
    time_elapsed = time.time() - current
    times.append(time_elapsed)
    print(time_elapsed)
    current = time.time()
    if len(times) >= 100:
      break
  
  print("AVERAGE : ", np.mean(time_elapsed))