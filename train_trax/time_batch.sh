#!/bin/bash
#SBATCH --gres=gpu:v100l:4
#SBATCH --cpus-per-task=32
#SBATCH --exclusive
#SBATCH --mem=0
#SBATCH --time=0-0:10
#SBATCH --account=def-pasquier
#SBATCH --mail-user jeffreyjohnens@gmail.com
#SBATCH --mail-type ALL

module load gcc/7.3.0 cuda/10.0.130
export XLA_FLAGS=--xla_gpu_cuda_data_dir=$EBROOTCUDA
source ~/TRAX/bin/activate
cd /home/jeffe/project/jeffe/DATASET_BUILDER/train_trax
python time_batch.py