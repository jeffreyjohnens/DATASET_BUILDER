import os
import copy
import time
import glob
import json
import torch
import numpy as np
from subprocess import call
from transformers import *
import dataset_builder_2 as db

GENERAL_MIDI_MAP = {
  "piano" : list(range(8)),
  "chromatic_perc" : list(range(9,16)),
  "organ" : list(range(16,24)),
  "guitar" : list(range(24,32)),
  "bass" : list(range(32,40)),
  "strings" : list(range(40,48)),
  "ensemble" : list(range(48,56)),
  "brass" : list(range(56,64)),
  "reed" : list(range(64,72)),
  "pipe" : list(range(72,80)),
  "synth_lead" : list(range(80,88)),
  "synth_pad" : list(range(88,96)),
  "synth_effects" : list(range(96,104)),
  "ethnic" : list(range(104,112)),
  "percussive" : list(range(112,120)),
  "sound_fx" : list(range(120,128)),
  "no_drums" : list(range(128)),
  "drums" : [128],
  "any" : list(range(129))
}


class sampler:

  def __init__(self, ckpt_path=None, name=None, step=None, web=True):
    if ckpt_path is None:
      if web:
        ckpt_path = self.download_model_from_web(name, step)
      else:
        ckpt_path = self.download_model(name, step)
    self.model = GPT2LMHeadModel.from_pretrained(ckpt_path, from_tf=False)

  def download_model_from_web(self,name,step):
    file_list = ["config.json", "pytorch_model.bin", "scheduler.pt", "training_args.bin"] # don't need optimizer
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    os.makedirs(ckpt_path, exist_ok=True)
    for file in file_list:
      path = os.path.join(ckpt_path,file)
      if not os.path.exists(path):
        call("wget http://www.sfu.ca/~jeffe/{}/{} -O {}".format(ckpt_path,file,path), shell=True)
    return ckpt_path

  def download_model(self, name, step):
    # download a model from compute canada (cedar)
    ckpt_path = "checkpoints/{}/checkpoint-{}".format(name,step)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    model_dir = os.path.dirname(ckpt_path)
    dest_dir = os.path.join(current_dir, model_dir)
    os.makedirs(dest_dir, exist_ok=True)
    if not os.path.exists(ckpt_path):
      call("rsync -a -e 'ssh -i /Users/Jeff/.ssh/cedar_rsa' jeffe@cedar.computecanada.ca:project/jeffe/DATASET_BUILDER/train_hugging/checkpoints/{}/checkpoint-{} {} --progress".format(name,step,dest_dir), shell=True)
    return ckpt_path

  def generate(self, input_ids=None, injects=None, temperature=.9, batch_size=2):
    import torch.nn.functional as F

    injects = [copy.deepcopy(injects) for _ in range(batch_size)]
    
    gen_start_time = time.time()

    if input_ids is None:
      input_ids = torch.zeros((batch_size,1), dtype=torch.long)
    else:
      input_ids = torch.from_numpy(input_ids).to(torch.long)
    # must be 2d

    past = None
    finished = np.zeros((batch_size,), dtype=np.bool)
    use_cache = self.model.config.use_cache
    vocab_size = self.model.config.vocab_size
    
    while np.any(finished==False):

      raw_input = input_ids
      if past:
        raw_input = input_ids[:,-1].unsqueeze(-1)

      outputs = self.model(input_ids=raw_input, past=past, use_cache=use_cache)
      if self.model._use_cache(outputs, use_cache):
        past = outputs[1]
      
      logits = outputs[0][:,-1,:]
      if temperature != 1.0:
        logits = logits / temperature
      
      last_tokens = raw_input[:,-1].detach().numpy()
      masks = torch.zeros((batch_size, vocab_size), dtype=torch.float)
      if injects is not None:
        for i,(last_token,inject) in enumerate(zip(last_tokens,injects)):
          trigger, mask = inject[0]
          if last_token == trigger:
            if mask is not None:
              assert mask.dtype == np.bool
              masks[i][~mask] = -float("Inf") # set avoided tokens to smallest value
            if len(inject) == 1:
              finished[i] = True
            else:
              inject.pop(0)
      
      if np.any(finished==False):
        logits = masks + logits
        
        probs = F.softmax(logits, dim=-1)
        next_token = torch.multinomial(probs, num_samples=1).squeeze(1)
        next_token[finished] = 0 # if the sequence has ended
        #print(db.TrackEncoder().rep.pretty([next_token]), db.TrackEncoder().rep.pretty([injects[0][0][0]]))
        input_ids = torch.cat([input_ids, next_token.unsqueeze(-1)], dim=-1)
      
      #if input_ids.shape[1] >= 512:
      #  finished[:] = True

    print(time.time() - gen_start_time)
    input_ids = input_ids.detach().numpy()

    # remove the padding at end of sequences
    output = []
    for seq in input_ids:
      idx = np.where(seq==0)[0]
      if len(idx) > 1:
        output.append( seq[:idx[1]] )
      else:
        output.append( seq )
    return output 

  def generate_tracks(self, tracks, input_ids=None, batch_size=2, temperature=.9):
    vocab_size = self.model.config.vocab_size
    enc = db.TrackEncoder()

    PIECE_START = enc.rep.encode({db.TOKEN_TYPE.PIECE_START : 0})
    NORMAL_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 0})
    DRUM_TRACK = enc.rep.encode({db.TOKEN_TYPE.TRACK : 1})
    TRACK_END = enc.rep.encode({db.TOKEN_TYPE.TRACK_END : 0})
    
    # impose various controls on the generated sequence
    control = []
    for track_num, track in enumerate(tracks):
      if isinstance(track,str):
        if track in GENERAL_MIDI_MAP:
          track = GENERAL_MIDI_MAP[track]
        else:
          print("WARNING : UNKNOWN GENERAL MIDI CATEGORY")
          track = GENERAL_MIDI_MAP["any"]
      assert isinstance(track,list)
      
      mask = np.zeros((vocab_size,), dtype=np.bool)
      if 128 in track:
        mask[DRUM_TRACK] = True
      if any([t < 128 for t in track]):
        mask[NORMAL_TRACK] = True

      print(np.where(mask))

      if track_num == 0 and input_ids is None:
        control.append((PIECE_START, mask))
      else:
        control.append((TRACK_END, mask))

      mask = np.zeros((vocab_size,), dtype=np.bool)
      if 128 not in track:
        for allowed_inst in track:
          tok = enc.rep.encode({db.TOKEN_TYPE.INSTRUMENT : allowed_inst})
          mask[tok] = True
        control.append((NORMAL_TRACK, mask)) # for force instrument version
      
    control.append((TRACK_END, None))
    
    return self.generate(input_ids=input_ids, temperature=temperature, injects=control, batch_size=batch_size)
  
  def resample_track(self, jstr, track_num, e, temperature=1):
    data = json.loads(jstr)
    inst = data["tracks"][track_num]["instrument"]
    tracks = [[inst]]
    if data["tracks"][track_num]["isDrum"]:
      tracks = ["drums"]

    jstr = db.remove_track(jstr,track_num) # remove it
    input_ids = db.TrackEncoder().json_to_tokens(jstr, e)
    input_ids = np.array(input_ids)[None,:]
    print(input_ids)
    return self.generate_tracks(tracks, input_ids=input_ids, batch_size=1, temperature=temperature)

  def resample_bar(self, jstr, track_num, bar_num, temperature=1):
    
    e = db.EncoderConfig()
    e.fill_track = track_num
    e.fill_bar = bar_num
    e.do_track_shuffle = False # don't reshuffle
    enc = db.TrackOneBarFillEncoder()
    FILL_START = enc.rep.encode({db.TOKEN_TYPE.FILL_IN:1})
    input_ids = enc.json_to_tokens(jstr, e)
    input_ids = np.array(input_ids)
    input_ids = input_ids[:np.where(input_ids==FILL_START)[0][0]+1][None,:]
    print(input_ids)
    control = [(enc.rep.encode({db.TOKEN_TYPE.FILL_IN:2}),None)]
    return self.generate(input_ids, temperature=temperature, batch_size=1, injects=control)

name = "TRACK_ENCODER_gpt2_version3_Jul_06_19_58_False"
#name = "TRACK_ONE_BAR_FILL_ENCODER_gpt2_TRASH_Jul_06_20_30_True"
step = 260000
s = sampler(name=name, step=step)

midi_paths = glob.glob("/users/jeff/data/lmd_full/**/*.mid", recursive=True)
path = np.random.choice(midi_paths)

# generate tracks
#tracks = ["drums", "bass"]
#tokens = s.generate_tracks(tracks, temperature=1., batch_size=1)[0]

# resample tracks
# want to somehow override the tempo
e = db.EncoderConfig()
e.do_track_shuffle = False # don't reshuffle

js = db.TrackEncoder().midi_to_json(path,e)
js = db.select_segment(js, 3) # select a random section with 3 tracks

e.default_tempo = json.loads(js)["tempo"]
print(e.default_tempo)

assert len(json.loads(js)["tracks"]) == 3
# check that it has three tracks

db.TrackEncoder().json_to_midi(js, "test_orig.mid", e)

"""
# resample bars ...
for i in range(20):
  tr = np.random.randint(3)
  br = np.random.randint(4)
  tokens = s.resample_bar(js, tr, br, temperature=.9)[0]
  js = db.TrackOneBarFillEncoder().tokens_to_json(tokens, e)
  db.TrackOneBarFillEncoder().tokens_to_midi(tokens, "test_bar_{}.mid".format(i), e)
"""

# resample tracks ...
for i in range(3):
  tokens = s.resample_track(js, 0, e, temperature=1.)[0]
  js = db.TrackEncoder().tokens_to_json(tokens, e)
  db.TrackEncoder().tokens_to_midi(tokens, "test_{}.mid".format(i), e)

