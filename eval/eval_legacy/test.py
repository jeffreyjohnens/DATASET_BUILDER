import numpy as np
import dataset_builder_2 as db

from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics.pairwise import cosine_distances
from sklearn.preprocessing import OneHotEncoder

# we can compare rhythm (i.e. number of onsets at each beat)
# using style rank?

# encode a bunch of midi and train several markov models
# take random skip grams and encode to a vector using a hash
enc = db.TrackEncoder()
vocab_size = db.getEncoderSize(db.ENCODER_TYPE.TRACK_ENCODER)

class skip_gram_vector:
	def __init__(self, n, m, w=6):
		self.w = w
		self.m = m # the number of skip grams to randomly sample
		self.n = n # the size of the vector
		self.vector = np.zeros((n,), dtype=np.int32)

	def __call__(self, x):
		for _ in range(self.m):
			self.encode(self.sample_skipgram(x))
		return np.minimum(self.vector, 1) # clip at 1

	def sample_skipgram(self, x):
		offset = np.random.randint(len(x)-self.w+1)
		skip_mask = np.random.randint(2, size=(self.w,))
		while skip_mask.sum() < 2:
			skip_mask = np.random.randint(2, size=(self.w,))

		segment = np.copy(x[offset:][:self.w]) # don't destroy
		segment[skip_mask==0] = -1
		if np.any(skip_mask==0):
			start = np.where(segment>=0)[0][0]
			end = np.where(segment>=0)[0][-1] + 1
			segment = segment[start:end]
		# normalize relative to first pitch
		first_pitch = None
		tokens = []
		for token in segment:
			if enc.rep.is_token_type(token, db.TOKEN_TYPE.PITCH):
				value = enc.rep.decode(token, db.TOKEN_TYPE.PITCH)
				if first_pitch is None:
					first_pitch = value
				rel_pitch = value - first_pitch
				tokens.append( vocab_size + 128 + rel_pitch )
			else:
				tokens.append( token )
		return tuple(list(tokens))
		
	def encode(self, x):
		self.vector[hash(x) % self.n] += 1

class style_rank_lite:
	# use random forest to rank a set of vectors
	# against corpus
	def __init__(self, n_estimators=500, max_depth=2):
		self.clf = RandomForestClassifier(
			n_estimators=n_estimators, max_depth=max_depth, class_weight="balanced")
	
	def __call__(self, x, y):
		labels = np.array([0] * len(x) + [1] * len(y))
		feature = np.concatenate([x,y], axis=0)
		self.clf.fit(feature, labels)
		print( self.clf.score(feature, labels) )
		leaves = self.clf.apply(feature)
		embedded = np.array(
			OneHotEncoder(categories='auto').fit_transform(leaves).todense())
		return 1. - cosine_distances(embedded)

"""
# get the dataset of tokens
e = db.EncoderConfig()
enc = db.TrackEncoder()
jag = db.Jagged("../dataset_builder_2/test.arr")
data = []
for i in tqdm(range(jag.get_split_size(0))):
	jstr = jag.read_json(i,0)
	data.append( enc.json_to_tokens(jstr, e) )
np.savez_compressed("corpus.npz", data=data)
exit()
"""

# PYTHONHASHSEED=8712346 python3 test.py

# model the tokens using different markov models
# or generate from model with different temperatures
n = 5000
m = 10000000
w = 4
seed = np.random.randint(2**20)

ng = 200
nc = 1000
data = np.load("data.npz", allow_pickle=True)["data"][()]
corpus = np.load("corpus.npz", allow_pickle=True)["data"]

D = {}

# dictionary based approach
x = []
for i in tqdm(np.random.randint(len(corpus), size=(nc,)),leave=False):
	bag = []
	seq = np.copy(corpus[i])
	for _ in range(n):
		ngram = tuple(list(seq[np.random.randint(len(seq)-w+1):][:w]))
		if not ngram in D:
			D[ngram] = len(D)
		bag.append(D[ngram])
	x.append(bag)

y = []
for i,(temp,seqs) in enumerate(tqdm(data.items(),leave=False)):
	av_score = []
	for seq in tqdm(seqs, leave=False):
		#bag = []
		score = 0
		for _ in range(n):
			ngram = tuple(list(seq[np.random.randint(len(seq)-w+1):][:w]))
			#if not ngram in D:
			#	D[ngram] = len(D)
			#bag.append(D[ngram])
			score += int(ngram in D)
		#y.append(bag)
		av_score.append(score)
	print(np.mean(av_score))

vocab_size = db.getEncoderSize(db.ENCODER_TYPE.TRACK_ENCODER)
av_score = []
for i in range(ng):
	seq = np.random.randint(vocab_size, size=(512,))
	#bag = []
	score = 0
	for _ in range(n):
		ngram = tuple(list(seq[np.random.randint(len(seq)-w+1):][:w]))
		#if not ngram in D:
		#	D[ngram] = len(D)
		#bag.append(D[ngram])
		score += int(ngram in D)
	#y.append(bag)
	av_score.append(score)

print(np.mean(av_score))
exit()

# bags to vectors
xv = np.zeros((len(x),len(D)), dtype=np.int32)
yv = np.zeros((len(y),len(D)), dtype=np.int32)

for i,xbag in enumerate(x):
	for val in xbag:
		xv[i,val] = 1

for i,ybag in enumerate(y):
	for val in ybag:
		yv[i,val] = 1

# remove indices only found in corpus
idx = yv.sum(0)>0
yv = yv[:,idx]
xv = xv[:,idx]

# show average intersection
for i in range(4):
	av_av = []
	for j in range(20):
		av_sim = np.mean([np.sum(xv[k] * yv[i*ng + j]) for k in range(nc)])
		#print(i, j, av_sim)
		av_av.append(av_sim)
	print("FINAL ", np.mean(av_av))

sr = style_rank_lite()
d = sr(xv,yv)

for i in range(4):
	print( np.median(np.mean(d[-nc:][:,:-nc][:,i*ng:(i+1)*ng],axis=0)) )

exit()

#sv = skip_gram_vector(1000, 10000)

# store hashes
x = []
for i in tqdm(np.random.randint(len(corpus), size=(nc,)),leave=False):
	x.append( db.sgbf(corpus[i][:512], n, m, w, seed) )

#np.savez_compressed("x.npz", data=x)
#exit()
#x = np.load("x.npz", allow_pickle=True)["data"]

ylabel = []
y = []
for i,(temp,seqs) in enumerate(tqdm(data.items(),leave=False)):
	#if (i < 2):
	for seq in tqdm(seqs, leave=False):
		y.append( db.sgbf(seq, n, m, w, seed) )
		ylabel.append( i )

vocab_size = db.getEncoderSize(db.ENCODER_TYPE.TRACK_ENCODER)
for i in range(ng):
	y.append( db.sgbf(np.random.randint(vocab_size, size=(512,)), n, m, w, seed) )

sr = style_rank_lite()
d = sr(x,y)

for i in range(4):
	print( np.median(np.median(d[-nc:][:,:-nc][:,i*ng:(i+1)*ng],axis=0)) )
