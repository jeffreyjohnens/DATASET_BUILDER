import dataset_builder_2 as db
import json
import time
import numpy as np
from tqdm import tqdm

# how acurate is density control

temp = .975

np.random.seed(int(time.time()))

ckpt_path = "/Users/Jeff/CODE/TE_MMM/code/track.pt"
encoder_type = db.ENCODER_TYPE.TRACK_DENSITY_ENCODER
sampler = db.Sampler(ckpt_path, encoder_type)

errors = []
for _ in tqdm(range(100)):

  density = np.random.randint(10)
  tokens = sampler.generate_tracks([(0,"bass")], "", 4, density, temp)

  ec = db.EncoderConfig()
  encoder = db.getEncoder(encoder_type)
  midi_json = json.loads(encoder.tokens_to_json(tokens[0],ec))

  actual_density = midi_json["tracks"][0]["noteDensity"]

  errors.append( abs(density - actual_density) )

print(np.mean(errors))