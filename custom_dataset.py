import numpy as np
import torch
from torch.utils.data import DataLoader, Dataset, RandomSampler, SequentialSampler

vocab_size = 60000
max_len = 512

class CustomDataset(Dataset):
  def __init__(self):
    pass

  def __len__(self):
    return 100

  def __getitem__(self, item):
    x = np.random.randint(vocab_size, size=(max_len,))
    return torch.tensor(x, dtype=torch.long)

# what should the pad value be ...
class CustomTokenizer:
  def __init__(self):
    self._pad_token = 0
    self.pad_token_id = 0
    self.max_len = max_len
  
  def __len__(self):
    return vocab_size
  
  def save_pretrained(self, *args, **kwargs):
    pass


#from transformers import GPT2Config
#configuration = GPT2Config()