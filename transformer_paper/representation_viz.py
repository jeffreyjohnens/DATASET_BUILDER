import dataset_builder_2 as db

path = "../dataset_builder_2/stems/stem_50.mid"

encoder = db.TrackEncoder()
e = db.EncoderConfig()
e.force_four_four = True
e.do_track_shuffle = False

tokens = encoder.midi_to_tokens(path,e)
tokens = encoder.rep.pretty(tokens)

def draw_text(dwg, text, x, y):
  dwg.add(dwg.text(text, insert=(x,y), text_anchor="middle", alignment_baseline="middle", style="font-size:12;font-family:Arial"))

def draw_left_text(dwg, text, x, y, fontsize=12):
  dwg.add(dwg.text(text, insert=(x,y), alignment_baseline="middle", style="font-size:{};font-family:Arial".format(fontsize)))

def create_token(dwg, x, y, text, fill="none"):
  dwg.add(dwg.rect(insert=(x,y), size=(token_width,token_height), stroke="#000000", stroke_width=2, fill=fill, opacity=.7, stroke_opacity=1, rx=5))
  draw_text(dwg, text, x+token_width//2, y+token_height*.75)
  #dwg.add(dwg.text(text, insert=(x+token_width//2,y+token_height//2), text_anchor="middle", dominant_baseline="middle", style="font-size:12;font-family:Arial")) 
  #writing_mode="tb", glyph_orientation_vertical=90))
  #transform="rotate(90 {},{})".format(x,y)))

# make a sequence of tokens in svg or something ...
bar_view = [
  "NOTE_ON=60",
  "TIME_DELTA=2",
  "NOTE_OFF=60",
  "NOTE_ON=64",
  "NOTE_ON=67",
  "TIME_DELTA=4",
  "NOTE_OFF=64",
  "TIME_DELTA=4",
  "NOTE_OFF=67",
]

voice_view = [
  "INST=30",
  "DENSITY=5",
  "BAR_START",
  "BLANK_BAR",
  "BAR_END",
  "BAR_START",
  "BLANK_BAR",
  "BAR_END",
  "BAR_START",
  "BLANK_BAR",
  "BAR_END",
  "BAR_START",
  "BLANK_BAR",
  "BAR_END",
]

piece_view = [
  "PIECE_START",
  #"GENRE=ROCK",
  #"GENRE=POP",
  "TRACK_START",
  "BLANK_TRACK",
  "TRACK_END",
  "TRACK_START",
  "BLANK_TRACK",
  "TRACK_END",
  "TRACK_START",
  "BLANK_TRACK",
  "TRACK_END",
]

fill_view = [
  "PIECE_START",
  "TRACK_START",
  "INST=30",
  "DENSITY=5",
  "BAR_START",
  "FILL_IN",
  "BAR_END",
  "BLANK_WHITE",
  "TRACK_END",
  "FILL_START",
  "BLANK_BAR",
  "FILL_END",
  "FILL_START",
  "BLANK_BAR",
  "FILL_END"
]

def draw_view(dwg, view, x, y):
  for i,token in enumerate(view):
    yy = y + (token_height+5)*i
    if token == "BLANK_BAR":
      create_token(dwg, x, yy, "<BAR>", fill="#ACACAC")
    elif token == "BLANK_TRACK":
      create_token(dwg, x, yy, "<TRACK>", fill="#ACACAC")
    elif token == "BLANK_WHITE":
      create_token(dwg, x, yy, "...", fill="none")
    else:
      create_token(dwg, x, yy, token, fill="none")

import svgwrite

token_height = 20
token_width = 100
width = 650
height = 500

dwg = svgwrite.Drawing('diagram.svg', (width,height))

dwg.add(dwg.rect(insert=(0,0), size=(width,height), fill="white"))

# headers
draw_left_text(dwg, "BAR", 50, 50, fontsize=16)
draw_left_text(dwg, "TRACK ", 200, 50, fontsize=16)
draw_left_text(dwg, "MULTI-TRACK", 350, 50, fontsize=16)
draw_left_text(dwg, "BAR-FILL", 500, 50, fontsize=16)

ys = 80
draw_view(dwg, bar_view, 50, ys)
draw_view(dwg, voice_view, 200, ys)
draw_view(dwg, piece_view, 350, ys)
draw_view(dwg, fill_view, 500, ys)

# make selectors for each view and pointers to the place they sit
# a selector is a bracket basically
xs = 50
gw = 50
g = 5

bar_target = 3
dwg.add(dwg.line(
  start=(xs + token_width + g, ys), 
  end=(xs + token_width + gw - g, ys + (token_height+5)*bar_target), 
  stroke="black", 
  stroke_width=2).dasharray([4,4]))

dwg.add(dwg.line(
  start=(xs + token_width + g, ys + (token_height+5)*(len(bar_view)-1) + token_height), 
  end=(xs + token_width + gw - g, ys + (token_height+5)*bar_target + token_height), 
  stroke="black", 
  stroke_width=2).dasharray([4,4]))

voice_target = 2
dwg.add(dwg.line(
  start=(xs + token_width*2 + gw + g, ys), 
  end=(xs + token_width*2 + gw*2 - g, ys + (token_height+5)*voice_target), 
  stroke="black", 
  stroke_width=2).dasharray([4,4]))

dwg.add(dwg.line(
  start=(xs + token_width*2 + gw + g, ys + (token_height+5)*(len(voice_view)-1) + token_height), 
  end=(xs + token_width*2 + gw*2 - g, ys + (token_height+5)*voice_target + token_height), 
  stroke="black", 
  stroke_width=2).dasharray([4,4]))

dwg.save()

#from cairosvg import svg2png
#from subprocess import call
#call("cairosvg test.svg -o diagram.png", shell=True)

#import svgutils
#svg = svgutils.transform.fromfile('test.svg')
#svg.transform.rotate(90)
