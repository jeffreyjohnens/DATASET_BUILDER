from dataset_builder import encode, decode, ENCODER_MODES, get_encoding_sizes
#from magenta.music import midi_file_to_sequence_proto
from subprocess import call
import numpy as np
import dataset_builder

print(get_encoding_sizes())

mode = ENCODER_MODES.MUSENET_DEFAULT
sigma = get_encoding_sizes()[mode]
j = dataset_builder.Jagged("../../data_small.arr", mode, False)

for _ in range(100):
  batch,_ = j.read_batch(512, 1024, 0)
  print(batch[0][:100])
  assert np.max(batch) <= sigma
  assert np.min(batch) >= 0
  decode(batch[10], mode, "err_test.mid")
  exit()
exit()

print()
exit()

path = "../../MIDI_DATA/CLASSICAL_ARCHIVES/JACOTIN/a_paris_.mid"
path = "test_midi/0a0c8865e48111d4359a3337fc574eb8.mid"
mode = ENCODER_MODES.MUSENET_DEFAULT

tokens = encode(path, mode, False)[0]
tokens = np.random.randint(65638, size=(512,))
decode(tokens, mode, "test.mid")


#call("open " + path, shell=True)
#call("open test.mid", shell=True)

