import dataset_builder
import os
import numpy as np
from tqdm import tqdm
from multiprocessing import Pool

def recurse_directory(rootdir):
	for root, _, filenames in os.walk(rootdir):
		for filename in filenames:
			if filename.endswith(".mid"):	
				yield os.path.join(root,filename)

def worker(args):
	sid,path,mode = args
	try:
		return sid, dataset_builder.encode(path,mode,True)
	except:
		return sid,[]

if __name__ == "__main__":

	# python test.py --data_dir /home/jeffe/project/jeffe/lmd_full --output /home/jeffe/project/jeffe/data_small.arr 
	# salloc --time=1:0:0 --ntasks-per-node=48 --mem=0 --account=def-pasquier
	# cp /home/jeffe/project/jeffe/data_small.arr /home/jeffe/scratch/data_small.arr
	# cp /home/jeffe/project/jeffe/data_small.arr.header /home/jeffe/scratch/data_small.arr.header
	# j = dataset_builder.Jagged("data.arr",dataset_builder.ENCODER_MODES.MUSENET_DEFAULT,False)

	# python3 test.py --data_dir /users/jeff/data/lmd_full --output data_small.arr

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("--data_dir", type=str, required=True)
	parser.add_argument("--output", type=str, required=True)
	parser.add_argument("--n_threads", type=int, default=8)
	args = parser.parse_args()

	import os
	os.system("taskset -p 0xffff %d" % os.getpid())

	# multi thread approach takes about 2 minutes
	pool = Pool(args.n_threads)
	mode = dataset_builder.ENCODER_MODES.MUSENET_DEFAULT
	jag = dataset_builder.Jagged(args.output, mode, True)
	inputs = list(recurse_directory(args.data_dir))
	
	# split dataset 
	N = len(inputs)
	np.random.seed(71236409)
	idx = np.argsort(np.random.rand(N))
	sids = np.zeros((N,), np.int32)
	sids[idx[:int(N*.8)]] = 0
	sids[idx[int(N*.8):int(N*.9)]] = 1
	sids[idx[int(N*.9):]] = 2
	
	print(np.bincount(sids).astype(np.float32) / N)

	#for sid,path in tqdm(zip(sids,inputs),total=len(inputs)):
	#	jag.append(path,sid)
	#	jag.flush()

	inputs = list(zip(sids, inputs, [mode] * len(inputs)))
	for sid,seqs in tqdm(pool.imap_unordered(worker, inputs), total=N):
		for seq in seqs:
			jag.append(seq,sid)
		jag.flush()
		if jag.get_size() > 10000:
			break
	
	jag.enable_read()
	print( jag.get_size() )
	batch, mask = jag.read_batch(512,16384,0)
	print(np.array(mask).sum(1))