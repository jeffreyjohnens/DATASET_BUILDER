#include <iostream>
#include <future>
#include <chrono>
#include <set>
#include <tuple>
#include <assert.h>
#include <math.h>
#include <vector>
#include <string>
#include <fstream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
namespace py = pybind11;
using namespace std;

#include "../../midifile/include/MidiFile.h"
#include "lz4.h"

// =============================================================
// tokens

static const int N_VELOCITY_BINS = 32;
static const int N_INST = 16;
static const int MIN_PITCH = 21;
static const int MAX_PITCH = 108;
static const int N_PITCH = MAX_PITCH - MIN_PITCH;
static const int MAX_SHIFT_STEPS = 100;
static const int MUSENET_VOCAB_SIZE = N_INST * N_VELOCITY_BINS * N_PITCH + MAX_SHIFT_STEPS; //44644

static const int MUSENET_VELOCITY_MAP[128] = {0,1,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,22,22,22,22,23,23,23,23,24,24,24,24,24,25,25,25,25,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,29,30,30,30,30,31,31,31};

static const int PERFORMANCE_VELOCITY_MAP[128] = {0,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,11,11,11,11,12,12,12,12,13,13,13,13,14,14,14,14,15,15,15,15,16,16,16,16,17,17,17,17,18,18,18,18,19,19,19,19,20,20,20,20,21,21,21,21,22,22,22,22,23,23,23,23,24,24,24,24,25,25,25,25,26,26,26,26,27,27,27,27,28,28,28,28,29,29,29,29,30,30,30,30,31,31,31};

static const double TIME_STRETCH_FACTORS[5] = {.95,.975,1.0,1.025,1.05};
static const int PITCH_SHIFTS[7] = {-3,-2,-1,0,1,2,3};

struct mtoken{
  int domain[10];
  int cumprod[10];
};

static const struct mtoken IVP = {
  {N_INST, N_VELOCITY_BINS, N_PITCH}, 
  {1, N_INST, N_INST*N_VELOCITY_BINS, N_INST*N_VELOCITY_BINS*N_PITCH}
};

int mtoken_encode(int a, int b, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  if ((a<0) || (a>=x->domain[0])) {
    throw 20;
  }
  if ((b<0) || (b>=x->domain[1])) {
    throw 21;
  }
  return (x->cumprod[1]*b) + a;
}

int mtoken_encode(int a, int b, int c, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  assert((c>=0) & (c<x->domain[2]));
  if ((a<0) || (a>=x->domain[0])) {
    throw 20;
  }
  if ((b<0) || (b>=x->domain[1])) {
    throw 21;
  }
  if ((c<0) || (c>=x->domain[2])) {
    throw 22;
  }
  return (x->cumprod[2]*c) + (x->cumprod[1]*b) + a;
}

int mtoken_encode(int a, int b, int c, int d, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  assert((c>=0) & (c<x->domain[2]));
  assert((d>=0) & (d<x->domain[3]));
  return (x->cumprod[3]*d) + (x->cumprod[2]*c) + (x->cumprod[1]*b) + a; 
}

int mtoken_decode(int token, int index, const mtoken *x) {
  return (token / x->cumprod[index]) % x->domain[index];
}

int mtoken_shift(int token, int index, int shift, const mtoken *x) {
  // throw exception if out of bounds
  int shifted = token + (shift * x->cumprod[index]);
  int dec = mtoken_decode(token, index, x);
  if ((dec < 0) || (dec >= x->domain[index]) || (shifted < 0) || (shifted >= x->cumprod[3])) {
    throw 2;
  }
  return shifted;
}



// =============================================================
static const int DRUM_CHANNEL = 9;

#define QUIET_CALL(noisy) { \
    cout.setstate(ios_base::failbit);\
    cerr.setstate(ios_base::failbit);\
    (noisy);\
    cout.clear();\
    cerr.clear();\
}

using Note = tuple<int,int,int,int,int>;
using Event = tuple<int,int,int,int>; // (TIME,VELOCITY,PITCH,INSTRUMENT) 

int quantize_beat(double x, double ticks, double steps_per_quarter, double cut=.5) {
  return (int)((x / ticks * steps_per_quarter) + (1.-cut));
}

int quantize_second(double x, double spq, double ticks, double steps_per_second, double cut=.5) {
  return (int)((x / ticks * spq * steps_per_second) + (1.-cut));
}

set<Event> parse(string filepath, int steps_per_unit, bool quantize_by_beat=true) {
  smf::MidiFile midifile;
  QUIET_CALL(midifile.read(filepath));
  midifile.linkNotePairs();

  set<Note> notes;
  set<Event> events;

  int pitch, duration, velocity, onset, channel;
  int track_count = midifile.getTrackCount();
  int ticks = midifile.getTicksPerQuarterNote();
  double spq = 0; // seconds per quarter
  vector<int> instruments(16,0);

  for (int track=0; track<track_count; track++) {
    for (int event=0; event<midifile[track].size(); event++) {
      if (midifile[track][event].isTempo()) {
        spq = (double)midifile[track][event].getTempoMicroseconds() / 1e6;
      }
    }
  }

  for (int track=0; track<track_count; track++) {
    fill(instruments.begin(), instruments.end(), 0); // zero instruments
    for (int event=0; event<midifile[track].size(); event++) {
      if (midifile[track][event].isNoteOn()) {

        pitch = (int)midifile[track][event][1] - MIN_PITCH;
        onset = midifile[track][event].tick;
        duration = midifile[track][event].getTickDuration();
        velocity = (int)midifile[track][event][2];
        channel = midifile[track][event].getChannelNibble();

        if (steps_per_unit > 0) {
          if (quantize_by_beat) {
            duration = quantize_beat(duration, ticks, steps_per_unit);
            onset = quantize_beat(onset, ticks, steps_per_unit);
          }
          else {
            duration = quantize_second(duration, spq, ticks, steps_per_unit);
            onset = quantize_second(onset, spq, ticks, steps_per_unit);
          }
        }

        if ((onset > 0) && (duration > 0) && (channel != DRUM_CHANNEL) && (pitch >= 0) && (pitch < N_PITCH)) {
          events.insert(
            make_tuple(onset,velocity,pitch,instruments[channel]));
          events.insert(
            make_tuple(onset+duration,0,pitch,instruments[channel]));
          notes.insert(make_tuple(
            onset,pitch,duration,velocity,instruments[channel]));
        }
      }
      if (midifile[track][event].isPatchChange()) {
        channel = midifile[track][event].getChannelNibble();
        instruments[channel] = (int)midifile[track][event][1];
      }      
    }
  }

  return events;
}

/*
vector<int> count_instruments(string filepath) {
  set<Note> notes = parse(filepath, 0, false);
  vector<int> counts(128,0);
  for (const auto note : notes) {
    counts[get<4>(note)/8]++;
  }
  return counts;
}
*/

// performance representation (MAGENTA)
// 128 NOTE_ON 128 NOTE_OFF 100 TIME_SHIFT 32 VELOCITY
vector<int> performance(string filepath, bool use_velocity=false) {
  int velocity_start = N_PITCH*2 + MAX_SHIFT_STEPS;
  int time_shift_start = N_PITCH*2;
  int current_step = 0;
  int current_velocity_bin = 0;
  int step, inst, velocity, pitch; 
  vector<int> tokens;
  
  set<Event> events = parse(filepath, MAX_SHIFT_STEPS, false);

  for (const auto event : events) {
    step = get<0>(event);
    velocity = get<1>(event);
    pitch = get<2>(event);
    inst = get<3>(event);

    if (step > current_step) {
      while (step > current_step + MAX_SHIFT_STEPS) {
        tokens.push_back(time_shift_start + MAX_SHIFT_STEPS - 1);
        current_step += MAX_SHIFT_STEPS;
      }
      if (step > current_step) {
        tokens.push_back(time_shift_start + step - current_step - 1);
      }
      current_step = step;
    }
    
    if (use_velocity) {
      int velocity_bin = PERFORMANCE_VELOCITY_MAP[velocity];
      if ((velocity > 0) && (velocity_bin != current_velocity_bin)) {
        current_velocity_bin = velocity_bin;
        tokens.push_back(velocity_start + velocity_bin);
      }
    }
    tokens.push_back( N_PITCH * (int)(velocity==0) + pitch ); 
  }
  return tokens;
}

// make a representation class so that code is variable
static const struct mtoken IVP = {
  {N_INST, N_VELOCITY_BINS, N_PITCH}, 
  {1, N_INST, N_INST*N_VELOCITY_BINS, N_INST*N_VELOCITY_BINS*N_PITCH}
};

class musenet_rep {
public:
  musenet_rep(int min_pitch_, int max_pitch_, int n_time_tokens_) {
    min_pitch = min_pitch_;
    max_pitch = max_pitch_;
    n_time_tokens = n_time_tokens_;
  }
  vector<int> convert(set<Event> &events, double time_stretch=1., int pitch_shift=0) {
    int TIME_SHIFT_START = IVP.cumprod[3];
    int current_step = 0;
    int step, inst, velocity, pitch, velocity_bin;
    vector<int> tokens(2,0);
    vector<int> empty;

    // keep track of the min pitch and max pitch within a sequence
    // so that pitch shifting is easy
    tokens[0] = get<2>(*events.begin());
    tokens[1] = tokens[0];

    for (const auto event : events) {
      step = (int)((double)get<0>(event) * time_stretch);
      velocity = get<1>(event);
      pitch = get<2>(event) + pitch_shift;
      inst = get<3>(event)/8; // put into groups

      tokens[0] = min(tokens[0], pitch);
      tokens[1] = max(tokens[1], pitch);

      if ((pitch < 0) || (pitch >= N_PITCH)) {
        return empty; // pitch was shifted out of range
      }

      if (step > current_step) {
        while (step > current_step + MAX_SHIFT_STEPS) {
          tokens.push_back(TIME_SHIFT_START + MAX_SHIFT_STEPS - 1);
          current_step += MAX_SHIFT_STEPS;
        }
        if (step > current_step) {
          tokens.push_back(TIME_SHIFT_START + step - current_step - 1);
        }
        current_step = step;
      }
      
      velocity_bin = MUSENET_VELOCITY_MAP[velocity];
      tokens.push_back( mtoken_encode(inst, velocity_bin, pitch, &IVP) ); 
    }
    return tokens;
  }


  int min_pitch;
  int max_pitch;
  int n_time_tokens;
};

// musenet representation
// 16*32*128 INST*VELOCITY*PITCH 100 TIME_SHIFT
vector<int> musenet(set<Event> &events, double time_stretch=1., int pitch_shift=0) {
  int TIME_SHIFT_START = IVP.cumprod[3];
  int current_step = 0;
  int step, inst, velocity, pitch, velocity_bin;
  vector<int> tokens(2,0);
  vector<int> empty;

  // keep track of the min pitch and max pitch within a sequence
  // so that pitch shifting is easy
  tokens[0] = get<2>(*events.begin());
  tokens[1] = tokens[0];

  for (const auto event : events) {
    step = (int)((double)get<0>(event) * time_stretch);
    velocity = get<1>(event);
    pitch = get<2>(event) + pitch_shift;
    inst = get<3>(event)/8; // put into groups

    tokens[0] = min(tokens[0], pitch);
    tokens[1] = max(tokens[1], pitch);

    if ((pitch < 0) || (pitch >= N_PITCH)) {
      return empty; // pitch was shifted out of range
    }

    if (step > current_step) {
      while (step > current_step + MAX_SHIFT_STEPS) {
        tokens.push_back(TIME_SHIFT_START + MAX_SHIFT_STEPS - 1);
        current_step += MAX_SHIFT_STEPS;
      }
      if (step > current_step) {
        tokens.push_back(TIME_SHIFT_START + step - current_step - 1);
      }
      current_step = step;
    }
    
    velocity_bin = MUSENET_VELOCITY_MAP[velocity];
    tokens.push_back( mtoken_encode(inst, velocity_bin, pitch, &IVP) ); 
  }
  return tokens;
}

vector<vector<int>> musenet_w_aug(string &filepath) {
  vector<vector<int>> seqs;
  set<Event> events = parse(filepath, MAX_SHIFT_STEPS, false);
  for (int i=0; i<5; i++) {
    seqs.push_back( musenet(events, TIME_STRETCH_FACTORS[i], 0) );
  }
  return seqs;
}

/*
vector<int> augment_musenet(vector<int> &tokens, int velocity_shift, int pitch_shift, int time_shift) {
  vector<int> augmented(tokens.size());
  int shifted;
  for (size_t i=0; i<tokens.size(); i++) {
    if (tokens[i] < IVP.cumprod[3]) {
      augmented[i] = mtoken_shift(tokens[i], 1, velocity_shift, &IVP);
      augmented[i] = mtoken_shift(augmented[i], 2, pitch_shift, &IVP);
    }
    else {
      // this is kind of long ...
      shifted = tokens[i] + time_shift;
      if ((shifted >= IVP.cumprod[3]) && (shifted < IVP.cumprod[3] + 100)) {
        augmented[i] = shifted;
      }
      else {
        augmented[i] = tokens[i];
      }
    }
  }
  return augmented;
}
*/

// =============================================================
// storage

class Jagged {
public:
  Jagged(string &filepath_, bool write_=true) {
    filepath = filepath_;
    write = write_;
    metadata_size = 4*sizeof(size_t); // (s,e,size_uncompressed,split_id)
    header_filepath = filepath + ".header";

    if (write) {
      fs.open(filepath, ios::out | ios::binary);
      header_fs.open(header_filepath, ios::out | ios::binary);
      size = 0;
    }
    else {
      fs.open(filepath, ios::in | ios::binary);
      header_fs.open(header_filepath, ios::in | ios::binary);
      header_fs.seekg(0, header_fs.end);
      size = header_fs.tellg()/metadata_size;
      header_fs.seekg(0, header_fs.beg);

      // load the metadata into memory ...
      // split by id (train=0, test=1, valid=2)
      size_t meta[4];
      ptr[0] = &train_meta;
      ptr[1] = &test_meta;
      ptr[2] = &valid_meta;
      
      for (size_t i=0; i<size; i++) {
        header_fs.read((char*)meta, metadata_size);
        ptr[meta[3]]->push_back(make_tuple(meta[0],meta[1],meta[2]));
      }
      header_fs.close(); // no longer needed

      std::cout << ptr[0]->size() << " " << ptr[1]->size() << " " << ptr[2]->size() << std::endl;
    }
  }

  void append(vector<int> &arr, size_t split_id) {
    if (!write) { return; }

    size_t start = fs.tellp();
    // begin compress ===============================
    size_t src_size = sizeof(int)*arr.size();
    size_t dst_capacity = LZ4_compressBound(src_size);
    char* dst = new char[dst_capacity];
    size_t dst_size = LZ4_compress_default(
      (char*)arr.data(), dst, src_size, dst_capacity);
    fs.write(dst, dst_size);
    delete[] dst;
    // end compress =================================
    size_t end = fs.tellp();
    header_fs.write((char*)&start, sizeof(size_t));
    header_fs.write((char*)&end, sizeof(size_t));
    header_fs.write((char*)&src_size, sizeof(size_t));
    header_fs.write((char*)&split_id, sizeof(size_t));
    size++;
  }

  vector<int> read(size_t index, size_t sid) {
    if (index >= ptr[sid]->size()) {
      std::cout << "ERROR : index exceeds dataset size" << std::endl;
      throw(1);
    }
    size_t csize = (get<1>((*ptr[sid])[index]) - get<0>((*ptr[sid])[index]));
    char* src = new char[csize/sizeof(char)];
    fs.seekg(get<0>((*ptr[sid])[index]));
    fs.read(src, csize);
    vector<int> x(get<2>((*ptr[sid])[index])/sizeof(int));
    LZ4_decompress_safe(src,(char*)x.data(),csize,get<2>((*ptr[sid])[index]));
    delete[] src;
    
    return x;
  }

  void crop(int *src, int n_tokens, vector<int> &dest, vector<int> &mask,  int pitch_shift) {
    // crop or pad sequences to max_len
    int max_len = dest.size();
    int pad_offset = 0;
    int token_offset = 0;
    if (n_tokens < max_len) {
      pad_offset = rand() % (max_len - n_tokens + 1);
    }
    else {
      token_offset = rand() % (n_tokens - max_len + 1);
    }
    
    //int LIMIT = IVP.cumprod[3]; // pitches must be less than this value
    int LIMIT = 0;
    for (int i=0; i<min(max_len,n_tokens); i++) {
      if (src[token_offset+i]<LIMIT) {
        dest[pad_offset+i] = mtoken_shift(src[token_offset+i], 2, pitch_shift, &IVP);
      }
      else {
        dest[pad_offset+i] = src[token_offset+i];
      }
      mask[pad_offset+i] = 1;
    }
  }

  tuple<vector<vector<int>>,vector<vector<int>>> read_batch(int batch_size, int max_len, size_t sid) {
    vector<vector<int>> batch(batch_size, vector<int>(max_len,0));
    vector<vector<int>> mask(batch_size, vector<int>(max_len,0));
    int index;
    for (int i=0; i<batch_size; i++) {
      index = rand() % ptr[sid]->size();
      vector<int> tokens = read(index,sid); 
      // tokens[0] is min_pitch
      // tokens[1] is max_pitch

      /*
      vector<int> valid;
      for (int t=-3; t<4; t++) {
        if ((tokens[0] + t >= 0) && (tokens[1] + t < N_PITCH)) {
          valid.push_back(t);
        }
      }
      */
      
      // don't pass in the first two metadata in tokens
      //crop(tokens.data() + 2, (int)tokens.size() - 2, batch[i], mask[i], valid[rand() % valid.size()]);
      crop(tokens.data() + 2, (int)tokens.size() - 2, batch[i], mask[i], 0);
    }
    return make_pair(batch,mask);
  }

  void flush() {
    fs.flush();
    header_fs.flush();
  }

  size_t size;
  
private:
  string filepath;
  string header_filepath;
  fstream fs;
  fstream header_fs;
  bool write;
  size_t metadata_size;

  vector<tuple<size_t,size_t,size_t>> train_meta;
  vector<tuple<size_t,size_t,size_t>> test_meta;
  vector<tuple<size_t,size_t,size_t>> valid_meta;
  vector<tuple<size_t,size_t,size_t>> *ptr[3];
};

// =============================================================
// build a dataset

/*
#include <thread>
#include "tqdm.h"

void worker(string *paths, int n, Jagged* writer, tqdm *bar, int total) {
  for (int i=0; i<n; i++) {
    vector<int> tokens = musenet(paths[i]);
    writer->append( tokens );
    bar->progress(writer->size, total);
  }
}

void builder(vector<string> &paths, string &dst_path, int n_threads=8) {
  int chunk = paths.size() / n_threads;
  int c = 0;
  Jagged writer(dst_path, true);
  tqdm bar;
  vector<thread> threads;
  for (int i=0; i<n_threads; i++) {
    c = chunk + ((int)(i==0) * (paths.size() % chunk));
    thread t(worker, paths.data()+chunk*i, c, &writer, &bar, paths.size());
    threads.push_back(std::move(t));
  }
  for (int i=0; i<n_threads; i++) {
    threads[i].join();
  }
}
*/


PYBIND11_MODULE(dataset_builder,m) {
  m.def("parse", &parse);
  //m.def("count_instruments", &count_instruments);
  m.def("performance", &performance);
  m.def("musenet", &musenet);
  m.def("musenet_w_aug", &musenet_w_aug);

  py::class_<Jagged>(m, "Jagged")
    .def(py::init<string &, bool, int>())
    .def("append", &Jagged::append)
    .def("read", &Jagged::read)
    .def("read_batch", &Jagged::read_batch)
    .def("flush", &Jagged::flush);

}