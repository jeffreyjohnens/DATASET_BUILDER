#include <set>
#include <iostream>
#include <tuple>

using namespace std;

int main() {

  set<tuple<int,int>> s;

  for (int i=0; i<100; i++) {
    s.insert(make_tuple(rand() % 100, rand() % 100));
  }

  for (auto it : s) {
    std::cout << get<0>(it) << " " << get<1>(it) << std::endl;
  }

}