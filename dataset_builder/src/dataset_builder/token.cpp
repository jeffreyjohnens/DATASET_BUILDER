#include <iostream>
#include <assert.h>

// supports up to 10 member multi token
static const int NUM_VELOCITY_BINS = 32;
static const int NUM_INST = 10;

struct mtoken{
  int domain[10];
  int cumprod[10];
};

static const struct mtoken IVP = {
  {NUM_INST, NUM_VELOCITY_BINS, 128}, 
  {1, NUM_INST, NUM_INST*NUM_VELOCITY_BINS, NUM_INST*NUM_VELOCITY_BINS*128}};

int mtoken_encode(int a, int b, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  return (x->cumprod[1]*b) + a;
}

int mtoken_encode(int a, int b, int c, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  assert((c>=0) & (c<x->domain[2]));
  return (x->cumprod[2]*c) + (x->cumprod[1]*b) + a;
}

int mtoken_encode(int a, int b, int c, int d, const mtoken *x) {
  assert((a>=0) & (a<x->domain[0]));
  assert((b>=0) & (b<x->domain[1]));
  assert((c>=0) & (c<x->domain[2]));
  assert((d>=0) & (d<x->domain[3]));
  return (x->cumprod[3]*d) + (x->cumprod[2]*c) + (x->cumprod[1]*b) + a; 
}

int mtoken_decode(int token, int index, const mtoken *x) {
  return (token / x->cumprod[index]) % x->domain[index];
}

int mtoken_shift(int token, int index, int shift, const mtoken *x) {
  // return original token if shift is out of bounds
  int shifted = token + (shift * x->cumprod[index]);
  int dec = mtoken_decode(token, index, x);
  if ((dec < 0) || (dec >= x->domain[index])) {
    return token;
  }
  return shifted;
}

// testing ...
int main() {

  for (int i=0; i<100000; i++) {
    int a = rand() % IVP.domain[0];
    int b = rand() % IVP.domain[1];
    int c = rand() % IVP.domain[2];

    int t = mtoken_encode(a, b, c, &IVP);
    assert(mtoken_decode(t, 0, &IVP)==a);
    assert(mtoken_decode(t, 1, &IVP)==b);
    assert(mtoken_decode(t, 2, &IVP)==c);

    int sa = rand() % IVP.domain[0];
    int sb = rand() % IVP.domain[1];
    int sc = rand() % IVP.domain[2];

    int s = mtoken_shift(t, 0, sa-a, &IVP);
    s = mtoken_shift(s, 1, sb-b, &IVP);
    s = mtoken_shift(s, 2, sc-c, &IVP);

    assert(mtoken_decode(s, 0, &IVP)==sa);
    assert(mtoken_decode(s, 1, &IVP)==sb);
    assert(mtoken_decode(s, 2, &IVP)==sc);

    assert(t<IVP.cumprod[3]); // don't be out of range
    assert(s<IVP.cumprod[3]);

  }
}