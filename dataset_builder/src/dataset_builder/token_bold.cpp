#include <iostream>
#include <vector>
#include <tuple>
#include <map>
#include <set>

#include "../../midifile/include/MidiFile.h"
#include "lz4.h"
#include "midi.pb.h"

// installing protobuf
// brew install protobuf ...
// export PKG_CONFIG_PATH=/usr/local/Cellar/protobuf/3.12.2/lib/pkgconfig
// pkg-config --cflags --libs protobuf
// protoc --cpp_out . midi.proto

//g++ token_bold.cpp ../../midifile/src/Binasc.cpp ../../midifile/src/MidiEvent.cpp ../../midifile/src/MidiEventList.cpp ../../midifile/src/MidiFile.cpp ../../midifile/src/MidiMessage.cpp midi.pb.cc lz4.c -I../../midifile/include -pthread -I/usr/local/Cellar/protobuf/3.12.2/include -L/usr/local/Cellar/protobuf/3.12.2/lib -lprotobuf --std=c++11; ./a.out

using namespace std;


enum TOKEN_TYPE {
  NOTE_ONSET,
  NOTE_OFFSET,
  PITCH,
  VELOCITY,
  TIME_DELTA,
  INSTRUMENT,
  BAR,
  BAR_END,
  TRACK,
  TRACK_END,
};

inline const char* toString(TOKEN_TYPE tt) {
  switch (tt) {
    case NOTE_ONSET: return "NOTE_ONSET";
    case NOTE_OFFSET: return "NOTE_OFFSET";
    case PITCH: return "PITCH";
    case VELOCITY: return "VELOCITY";
    case TIME_DELTA: return "TIME_DELTA";
    case INSTRUMENT: return "INSTRUMENT";
    case BAR: return "BAR";
    case BAR_END: return "BAR_END";
    case TRACK: return "TRACK";
    case TRACK_END: return "TRACK_END";
  }
}

class TOKEN {
public:
  TOKEN(vector<tuple<TOKEN_TYPE,int>> spec) {
    cprod.push_back( 1 );
    for (size_t i=0; i<spec.size(); i++) {
      tt_2_index[get<0>(spec[i])] = i;
      domain.push_back( get<1>(spec[i]) );
      cprod.push_back( cprod.back() * get<1>(spec[i]) );
    }
  }
  int encode(map<TOKEN_TYPE,int> values) {
    // notifies if you try to add an unknown token
    int value;
    TOKEN_TYPE tt;
    int token = 0;
    for (const auto kv : values) {
      tt = kv.first;
      value = kv.second;
      auto it = tt_2_index.find(tt);
      if (it == tt_2_index.end()) {
        cout << "WARNING : TRYING TO ADD UNKNOWN TOKEN TYPE" << endl;
      }
      else {
        if ((value < 0) || (value >= domain[it->second])) {
          cout << "ERROR : TRYING TO ENCODE VALUE (" << value << ") OUTSIDE OF DOMAIN (" << toString(tt) << ")" << endl;
          throw(2);
        }
        token += cprod[it->second] * value;
      }
    }
    return token;
  }
  int decode(int token, TOKEN_TYPE tt) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    return (token / cprod[it->second]) % domain[it->second];
  }
  int shift(int token, TOKEN_TYPE tt, int shift) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    int shifted = token + (shift * cprod[it->second]);
    int dec = decode(token, tt) + shift;
    if ((dec<0)||(dec>=domain[it->second])||(shifted<0)||(shifted>=cprod.back())) {
      throw 2;
    }
    return shifted;
  }
  int max_token() {
    return cprod.back();
  }
  int get_domain(TOKEN_TYPE tt) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    return domain[it->second];
  }
  vector<int> domain;
  vector<int> cprod;
  map<TOKEN_TYPE,int> tt_2_index;
};

class REPRESENTATION {
public:
  REPRESENTATION(vector<vector<tuple<TOKEN_TYPE,int>>> spec) {
    int count = 0;
    for (size_t i=0; i<spec.size(); i++) {
      for (size_t j=0; j<spec[i].size(); j++) {
        tt_2_index[get<0>(spec[i][j])] = i;
      }
      TOKEN tok(spec[i]);
      toks.push_back(tok);
      starts.push_back(count);
      count += tok.max_token();
      ends.push_back(count);
    }
  }
  int decode(int token, TOKEN_TYPE tt) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    if ((token >= starts[it->second]) && (token < ends[it->second])) {
      return toks[it->second].decode(token - starts[it->second], tt);
    }
    return -1;
  }
  bool is_token_type(int token, TOKEN_TYPE tt) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    if ((token >= starts[it->second]) && (token < ends[it->second])) {
      return true;
    }
    return false;
  }
  int encode(map<TOKEN_TYPE,int> values) {
    // make sure that tts all belong to same token
    auto it = tt_2_index.find(values.begin()->first);
    assert(it != tt_2_index.end());
    return toks[it->second].encode(values) + starts[it->second];
  }
  int shift(int token, TOKEN_TYPE tt, int shift) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    if ((token >= starts[it->second]) && (token < ends[it->second])) {
      int shifted = toks[it->second].shift(token-starts[it->second], tt, shift);
      if (shifted == -1) { return -1; } // if it fails make sure to return -1
      return shifted + starts[it->second];
    }
    return token; // don't modify
  }
  int get_domain(TOKEN_TYPE tt) {
    auto it = tt_2_index.find(tt);
    assert(it != tt_2_index.end());
    return toks[it->second].get_domain(tt);
  }
  void show(vector<int> tokens) {
    int value;
    for (const auto token : tokens) {
      for (const auto kv : tt_2_index) {
        value = decode(token, kv.first);
        if (value >= 0) {
          cout << toString(kv.first) << "=" << value << " ";
        }
      }
      cout << endl;
    }
  }
  int max_token() {
    return ends.back();
  }
  vector<TOKEN> toks;
  vector<int> starts;
  vector<int> ends;
  map<TOKEN_TYPE,int> tt_2_index;
};


using Event = tuple<int,int,int,int>; // (TIME,VELOCITY,PITCH,INSTRUMENT) 
using EventList = google::protobuf::RepeatedPtrField<midi::Piece_Track_Bar_Event>;

vector<int> to_performance(midi::Piece_Track_Bar *bar, REPRESENTATION *rep) {
  vector<int> tokens;
  int current_step = 0;
  int N_TIME_TOKENS = rep->get_domain(TIME_DELTA);
  for (const auto event : bar->events()) {

    if ((event.pitch() >= 0) && (event.pitch() < 128)) {
      if (event.time() > current_step) {
        while (event.time() > current_step + N_TIME_TOKENS) {
          tokens.push_back( rep->encode({{TIME_DELTA,N_TIME_TOKENS-1}}) );
          current_step += N_TIME_TOKENS;
        }
        if (event.time() > current_step) {
          tokens.push_back( rep->encode(
            {{TIME_DELTA,event.time()-current_step-1}}) );
        }
        current_step = event.time();
      }
      else if (event.time() < current_step) {
        cout << "ERROR : events are not sorted." << endl;
        throw(2);
      }
      tokens.push_back( rep->encode(
        {{PITCH,event.pitch()},{VELOCITY,event.velocity()}}) ); 
    }
  }
  return tokens;
}

vector<int> to_performance_w_tracks(midi::Piece *p, REPRESENTATION *rep, int start) {

  vector<int> tokens;
  for (int i=0; i<p->tracks_size(); i++) {
    tokens.push_back( rep->encode({{TRACK,i}}) );
    for (int j=start; j<min((int)p->tracks(i).bars_size(),start+4); j++) {
      tokens.push_back( rep->encode({{BAR,j - start}}) );
      midi::Piece_Track_Bar b = p->tracks(i).bars(j);
      vector<int> bar = to_performance(&b, rep);
      tokens.insert(tokens.end(), bar.begin(), bar.end());
      tokens.push_back( rep->encode({{BAR_END,0}}) );
    }
    tokens.push_back( rep->encode({{TRACK_END,0}}) );
  }
  return tokens;
}

class ENCODER {
public:
  virtual vector<int> encode(midi::Piece *p) {}
  virtual midi::Piece* decode(vector<int> &tokens) {}
  REPRESENTATION *rep;
};

class TrackEncoder : public ENCODER  {
public:
  TrackEncoder() {
    rep = new REPRESENTATION({
      {{BAR,4}}, 
      {{BAR_END,1}}, 
      {{TRACK,16}},
      {{TRACK_END,1}}, 
      {{PITCH,128},{VELOCITY,2}},
      {{TIME_DELTA,48}}
    });
  }
  ~TrackEncoder() {
    delete rep;
  }
  vector<int> encode(midi::Piece *p) {
    return to_performance_w_tracks(p, rep, 8);
  }
  midi::Piece* decode(vector<int> &tokens) {
    midi::Piece *p = new midi::Piece;
    midi::Piece_Track *t = NULL;
    midi::Piece_Track_Bar *b = NULL;
    midi::Piece_Track_Bar_Event *e = NULL;
    int current_time;
    for (const auto token : tokens) {
      if (rep->is_token_type(token, TRACK)) {
        t = p->add_tracks();
      }
      else if (rep->is_token_type(token, TRACK_END)) {
        t = NULL;
      }
      else if (rep->is_token_type(token, BAR)) {
        b = t->add_bars();
        current_time = 0;
      }
      else if (rep->is_token_type(token, BAR_END)) {
        b = NULL;
      }
      else if (rep->is_token_type(token, TIME_DELTA)) {
        current_time += rep->decode(token, TIME_DELTA);
      }
      else if (rep->is_token_type(token, PITCH)) {
        e = b->add_events();
        e->set_pitch( rep->decode(token, PITCH) );
        e->set_velocity( rep->decode(token, VELOCITY) );
        e->set_time( current_time );
        e->set_instrument( 0 );
      }
    }
    return p;
  }
};

enum ENCODER_TYPE {
  TRACK_ENCODER
};

ENCODER* getEncoder(ENCODER_TYPE et) {
  switch (et) {
    case TRACK_ENCODER: return new TrackEncoder();
  }
}


// =============================================================
static const int DRUM_CHANNEL = 9;

#define QUIET_CALL(noisy) { \
    cout.setstate(ios_base::failbit);\
    cerr.setstate(ios_base::failbit);\
    (noisy);\
    cout.clear();\
    cerr.clear();\
}

using Note = tuple<int,int,int,int,int>; // (ONSET,PITCH,DURATION,VELOCITY,INST)
using Event = tuple<int,int,int,int>; // (TIME,VELOCITY,PITCH,INSTRUMENT) 

int quantize_beat(double x, double TPQ, double SPQ, double cut=.5) {
  return (int)((x / TPQ * SPQ) + (1.-cut)) * (TPQ / SPQ);
}

int quantize_second(double x, double spq, double ticks, double steps_per_second, double cut=.5) {
  return (int)((x / ticks * spq * steps_per_second) + (1.-cut));
}

void parse_new(string filepath, int resolution, midi::Piece *p) {
  smf::MidiFile midifile;
  QUIET_CALL(midifile.read(filepath));
  midifile.makeAbsoluteTicks();
  midifile.linkNotePairs();

  set<Event> events;
  bool is_offset;
  int pitch, time, velocity, channel, current_tick, bar, voice;
  int start, end, start_bar, end_bar, rel_start, rel_end;
  int track_count = midifile.getTrackCount();
  int TPQ = midifile.getTPQ();
  vector<int> instruments(16,0);

  smf::MidiEvent *mevent;
  
  // get time signature and track/channel info
  int max_tick = 0;
  map<tuple<int,int>,int> track_map;
  map<int, tuple<int,int,int>> timesigs;

  for (int track=0; track<track_count; track++) {
    for (int event=0; event<midifile[track].size(); event++) { 
      mevent = &(midifile[track][event]);
      if (mevent->isTimeSignature()) {
        int barlength = (TPQ * 4 * (*mevent)[3]) / (1<<(*mevent)[4]);
        timesigs[mevent->tick] = make_tuple(
          (*mevent)[3], 1<<(*mevent)[4], barlength);
      }
      if ((mevent->isNoteOn()) || (mevent->isNoteOff())) {
        channel = mevent->getChannelNibble();
        if (track_map.find(make_pair(track,channel)) == track_map.end()) {
          track_map[make_pair(track,channel)] = track_map.size();
        }
        max_tick = max(max_tick, mevent->tick);
      }
    }
  }
  timesigs[max_tick] = make_tuple(0,0,0);

  map<int,int> barlines;

  if (timesigs.size() >= 2) {
    // must have atleast one time signature to compute bar lines
    int barlength;
    auto it = timesigs.begin();
    while (it != timesigs.rbegin().base()) {
      barlength = get<2>(it->second);
      for (int i=it->first; i<next(it)->first; i=i+barlength) {
        barlines[i] = barlines.size();
      }
      it++;
    }
    // add last barline
    barlines[timesigs.rbegin()->first + barlength] = barlines.size();
  }
  
  vector<vector<vector<Event>>> output(track_map.size(), vector<vector<Event>>(barlines.size()));

  for (int track=0; track<track_count; track++) {
    fill(instruments.begin(), instruments.end(), 0); // zero instruments
    for (int event=0; event<midifile[track].size(); event++) {
      mevent = &(midifile[track][event]);

      if ((mevent->isNoteOn()) && (mevent->isLinked())) {
        pitch = (*mevent)[1];
        velocity = min((int)(*mevent)[2], 1); // rep->get_domain(VELOCITY)-1);
        channel = mevent->getChannelNibble();
        voice = track_map.find(make_pair(track,channel))->second;

        start = quantize_beat(mevent->tick, TPQ, resolution);
        end = quantize_beat(mevent->getLinkedEvent()->tick, TPQ, resolution);

        auto start_bar_it = barlines.lower_bound(start);
        auto end_bar_it = barlines.lower_bound(end);
        if (start_bar_it->first > start) {
          start_bar_it--;
        }
        if (end_bar_it->first >= end) {
          end_bar_it--;
        }

        start_bar = start_bar_it->second;
        end_bar = end_bar_it->second;

        rel_start = ((start - start_bar_it->first) * resolution) / TPQ;
        rel_end = ((end - end_bar_it->first) * resolution) / TPQ;

        if ((channel != DRUM_CHANNEL) && (start_bar >= 0) && (end_bar >= 0)) {
          output[voice][start_bar].push_back(
            make_tuple(rel_start,velocity,pitch,instruments[channel]));
          output[voice][end_bar].push_back(
            make_tuple(rel_end,0,pitch,instruments[channel]));
        }
      }

      if (midifile[track][event].isPatchChange()) {
        channel = midifile[track][event].getChannelNibble();
        instruments[channel] = (int)midifile[track][event][1];
      }      
    }
  }

  // sort each bar
  for (int i=0; i<output.size(); i++) {
    for (int j=0; j<output[i].size(); j++) {
      sort(output[i][j].begin(), output[i][j].end());
    }
  }
  
  for (int i=0; i<output.size(); i++) {
    midi::Piece_Track *t = p->add_tracks();
    for (int j=0; j<output[i].size(); j++) {
      midi::Piece_Track_Bar *b = t->add_bars();
      for (int k=0; k<output[i][j].size(); k++) {
        midi::Piece_Track_Bar_Event *e = b->add_events();
        e->set_time(get<0>(output[i][j][k]));
        e->set_velocity(get<1>(output[i][j][k]));
        e->set_pitch(get<2>(output[i][j][k]));
        e->set_instrument(get<3>(output[i][j][k]));
      }
    }
  }
}


// how to handle saving large datasets ...

class Jagged {
public:
  Jagged(string filepath_) {
    filepath = filepath_;
    header_filepath = filepath_ + ".header";
    can_write = false;
    can_read = false;
  }

  void enable_write() {
    if (can_write) { return; }
    fs.open(filepath, ios::out | ios::binary);
    can_write = true;
  }
  
  void enable_read() {
    if (can_read) { return; }
    fs.open(filepath, ios::in | ios::binary);
    header_fs.open(header_filepath, ios::in | ios::binary);
    header.ParseFromIstream(&header_fs);
    can_read = true;
  }

  void append(string &s, size_t split_id) {
    enable_write();

    size_t start = fs.tellp();
    // begin compress ===============================
    size_t src_size = sizeof(char)*s.size();
    size_t dst_capacity = LZ4_compressBound(src_size);
    char* dst = new char[dst_capacity];
    size_t dst_size = LZ4_compress_default(
      (char*)s.c_str(), dst, src_size, dst_capacity);
    fs.write(dst, dst_size);
    delete[] dst;
    // end compress =================================
    size_t end = fs.tellp();
    midi::Dataset::Item *item;
    switch (split_id) {
      case 0: item = header.add_train();
      case 1: item = header.add_valid();
      case 2: item = header.add_test();
    }
    item->set_start(start);
    item->set_end(end);
    item->set_src_size(src_size);
  }

  string read(size_t index, size_t split_id) {
    enable_read();

    midi::Dataset::Item item;
    switch (split_id) {
      case 0: item = header.train(index);
      case 1: item = header.valid(index);
      case 2: item = header.test(index);
    }
    size_t csize = item.end() - item.start();
    char* src = new char[csize/sizeof(char)];
    fs.seekg(item.start());
    fs.read(src, csize);
    string x(item.src_size(), ' ');
    LZ4_decompress_safe(src,(char*)x.c_str(),csize,item.src_size());
    delete[] src;
    return x;
  }

  vector<vector<int>> read_batch(int batch_size, size_t split_id, ENCODER_TYPE et) {
    int nitems = 0;
    switch (split_id) {
      case 0 : nitems = header.train_size();
      case 1 : nitems = header.valid_size();
      case 2 : nitems = header.test_size();
    }
    int index;
    midi::Piece p;
    vector<vector<int>> batch;
    ENCODER* enc = getEncoder(et);
    for (int i=0; i<batch_size; i++) {
      index = rand() % nitems;
      string serialized_data = read(index, split_id);
      p.ParseFromString(serialized_data);
      batch.push_back( enc->encode(&p) );
    }
    return batch;
  }

  void close() {
    fs.flush();

    header_fs.open(header_filepath, ios::out | ios::binary);
    if (!header.SerializeToOstream(&header_fs)) {
      cerr << "ERROR : Failed to write header file" << endl;
    }
    fs.close();
    header_fs.close();
  }
  
private:
  string filepath;
  string header_filepath;
  fstream fs;
  fstream header_fs;
  bool can_write;
  bool can_read;
  midi::Dataset header;
};

// encode midi for dataset
string encode(string &filepath, int resolution) {
  string x;
  midi::Piece p;
  parse_new(filepath, resolution, &p);
  p.SerializeToString(&x);
  return x;
}

// make into a python library ...

int main() {

  REPRESENTATION r({
    {{BAR,4}}, 
    {{BAR_END,1}}, 
    {{TRACK,16}},
    {{TRACK_END,1}}, 
    {{PITCH,128},{VELOCITY,2}},
    {{TIME_DELTA,48}}
  });

  Jagged h("test_bold.arr");
  string xx = h.read(0,0);
  
  midi::Piece o;
  o.ParseFromString(xx);

  for (int i=0; i<100; i++) {
    to_performance_w_tracks(&o, &r, 8);
    cout << i << endl;
  }

  //vector<int> tokens = to_performance_w_tracks(output, &r, 8);
  //r.show(tokens);

}