cmake_minimum_required(VERSION 2.8.12)
project(dataset_builder)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(pybind11)
#add_subdirectory(abseil)
#add_subdirectory(midifile)

include_directories(midifile/include)
set(SRCS
    midifile/src/Options.cpp
    midifile/src/Binasc.cpp
    midifile/src/MidiEvent.cpp
    midifile/src/MidiEventList.cpp
    midifile/src/MidiFile.cpp
    midifile/src/MidiMessage.cpp
)

set(HDRS
    midifile/include/Binasc.h
    midifile/include/MidiEvent.h
    midifile/include/MidiEventList.h
    midifile/include/MidiFile.h
    midifile/include/MidiMessage.h
    midifile/include/Options.h
)

add_library(midifile STATIC ${SRCS} ${HDRS})

pybind11_add_module(dataset_builder src/dataset_builder/test.cpp src/dataset_builder/lz4.c)
#target_link_libraries(dataset_builder PRIVATE absl::flat_hash_map)
target_link_libraries(dataset_builder PRIVATE midifile)