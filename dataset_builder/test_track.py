import dataset_builder
import glob
import json

paths = glob.glob("test_midi/*.mid")
for path in paths:
  res = dataset_builder.parse_new(path,0)

  for i,track in enumerate(res):
    for j,bar in enumerate(track):
      cur = 0
      for s,v,p,_ in bar:
        try:
          assert s >= cur
        except:
          print(i,j,bar, sorted(bar))
          exit()
        cur = s
  
  exit()

  with open("track.json", "w") as f:
    json.dump(res,f,indent=4)
  break